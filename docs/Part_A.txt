﻿Part A .- Disseny, construcció, configuració i administració d’una Intranet de serveis

Consistirà en el disseny, construcció, configuració i administració d’una Intranet vàlida per a una empresa o institució.

Donada la diversitat de tipologies d’empreses i institucions, es podrà agafar com a referència una tipologia genèrica o es podrà concretar la proposta en una tipologia concreta.

El disseny del maquinari i programari de la Intranet ha de ser amb la màxima seguretat i redundància dins d'un pressupost raonable.

Les característiques mínimes que haurà de contemplar la Intranet són:

   1. FET Servidors: Servidor Linux i Windows Server amb un sistema de fitxers RAID per a emmagatzemar les dades. Seran màquines virtuals.
   2. FET Clients: Ordinadors Windows o Linux. Seran màquines virtuals.
   3. FALTA En acabar la instal·lació de les maquines virtuals, caldrà fer una còpia d’una imatge emprant les eines adequades, per a possibles restauracions.
   4. FET Usuaris: Els usuaris es validaran de forma centralitzada en un dels dos servidors, de manera que podran accedir als recursos de l'altre servidor sense tornar a introduir usuari i contrasenya. La gestió dels grups també serà centralitzada.
   5. FET Comunicacions: Caldrà afegir un nou servei de xifrat no explicat al cicle.
   6. FET Disseny i construcció de la xarxa, amb el cablejat necessari, que ha d’acompanyar els elements bàsics pel funcionament de la xarxa (hubs, switchs, routers, etc).
   7. FET Pressupost del maquinari i programari.
   8. El servidor Linux ha de tenir, almenys, els següents serveis:
         1. FET Una carpeta documents_generals, accessible mitjançant el protocol FTP i Samba, que pot ser llegida per tots els usuaris i sols pot ser modificada pels usuaris del grup gestors.
         2. FET La validació d’usuaris per a FTP i Samba, s’ha de fer de forma centralitzada.
         3. FET Servidor de DNS.
         4. FET Servidor LAMP.
         5. FET El servei Mysql tindrà, almenys, un usuari admin amb drets de root i un usuari user amb drets de lectura. Cal crear un script per donar d’alta en el servidor Mysql els nous usuaris, creant una BD pròpia amb drets totals.
         6. FET Còpia de seguretat incremental automatitzada.
         7. FET Firewall que sols permet accedir per protocol HTTP, HTTPS i FTP a Internet.
