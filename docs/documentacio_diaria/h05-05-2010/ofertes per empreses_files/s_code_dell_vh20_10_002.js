// THIS FILE DELIVERED 03/12/2010

/* SiteCatalyst code version: H.20.3
Copyright 1997-2009 Omniture, Inc. More info available at
http://www.omniture.com */
/************************ ADDITIONAL FEATURES ************************
	Plugins
	removed default currencyCode variable - defaults happen in VISTA
	added T&T plugin v1.0
	includes Universal Tag support
	includes fix for Google referrer changes
	added enhanced file download tracking
	added dynamicObjectIDs plugin
	added navigation method merchandising
	added next-gen pagename
	added survey module
*/

var s_account="dellglobalonline"
var s_dell=s_gi(s_account)
/************************** CONFIG SECTION **************************/
/* You may add or alter any code config here. */
/* Conversion Config */
s_dell.charSet="UTF-8"

/* Link Tracking Config */
s_dell.trackDownloadLinks=true
s_dell.trackExternalLinks=true
s_dell.trackInlineStats=true
s_dell.linkDownloadFileTypes="exe,zip,wav,mp3,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx"
s_dell.linkInternalFilters="javascript:,dellpartnerdirect.com,dell.,allpowerful.com,ideastorm.com"
s_dell.linkLeaveQueryString=false
s_dell.linkTrackVars="None"
s_dell.linkTrackEvents="None"

/** added in 10/24/08 release **/
s_dell.isPageLoad=true;

/** added in 8/09 release **/
function s_dell_getObjectID(o){
	var ID=o.href;
	return ID;
}
s_dell.getObjectID=s_dell_getObjectID

/* Plugin Config */
s_dell.usePlugins=true
function s_dell_doPlugins(s_dell) {
	/* Add calls to plugins here */
	s_dell.events=s_dell.events?s_dell.events:'';

/** added in 8/09 release **/
	/*** DynamicObjectIDs config (for ClickMap) ***/
	s_dell.setupDynamicObjectIDs();
	
/***** BEGIN pageName logic *****/	
// - lowercase META names
// - "storm" in generator tag
// - ability to override by manually setting pageName prior to this function
// - special handling for ecomm manually set page names - override pageName value, pull and process from META
	var metas=document.getElementsByTagName?document.getElementsByTagName('META'):'';
	var pn='',pnf=false,n='',gen='',nextgen=false;
	for (i=0; i<metas.length; i++){
		n=metas[i].getAttribute('NAME');
		n=n?n.toLowerCase():n;
		if(n == "generator"){
			gen=metas[i].getAttribute('CONTENT');
			if(gen.toLowerCase().indexOf('storm')>-1)pnf=true;
			if(gen.toLowerCase().indexOf('ng content publishing engine')>-1)nextgen=true;
		}
		if(n == "metricspath"){
			pn = metas[i].getAttribute('CONTENT');
			pn=pn.toLowerCase();
			if(pn.indexOf('&eiwatch=')>-1)pn=s_dell.repl(pn,'&eiwatch=','');
		}
		if(pnf&&pn)break;
	}
		
	if(!s_dell.pageName || s_dell.pageName.indexOf("dellstore^")>-1){
		s_dell.pageName=''
		
// handle dell.com homepages (static and dynamic should have same name)
		if(document.location=="http://www.dell.com/" || pn=="www1.us.dell.com/us/en/gen//content^default/")s_dell.pageName="dell.com homepage"
		var pna=s_dell.split(pn,'/');		// split on / delimiter	
// handle page cloaking
		if(pna.length>0 && pna.length<6){
			if(!s_dell.pageName){
				if(pn.indexOf('http://')>-1)pn=s_dell.repl(pn,'http://','');
				if(pn.indexOf('https://')>-1)pn=s_dell.repl(pn,'https://','');
				if(pn.indexOf('?')>-1)s_dell.pageName=pn.substring(0,pn.indexOf('?'))
				else s_dell.pageName=pn;
			}
		}
		if(pnf && pn && !s_dell.pageName){
			s_dell.prop14=pn;								// set original page key
// default storm format:
// domain/country/lauguage/segment/customerset/uristem^info/   /[optionalparameters]
//    0      1       2        3        4             5       6	        7	
			var a7=pna[7],a6=pna[6];									// querystring value (optional parameters)	
			var ovf=false,af=false;					// other value flag, appended flag (for else case)
			pn=dpn='';													// clear page name
			for(i=1;i<8;i++){
				if(i==4 && pna[0].indexOf('premier')>-1){pn=s_dell.apl(pn,'',':',0);af=true;}
/** next line added in 10/24/08 release **/
				if(i==4 && pna[4].indexOf('rc')==0 && !af){pn=s_dell.apl(pn,'',':',0);af=true;}
				if(i==6 && a6 && a7){
					if(pna[6].indexOf('[')>-1){
						pn=s_dell.apl(pn,'',':',0);
						af=true
					}
				}
				else if(i==6 && a6){
					if(pna[6].indexOf('[')>-1)af=true
				}
				else if(i==6){
					pn=s_dell.apl(pn,pna[i],':',0);
					af=true
				}
				if(i==7 && a7){
	/** values to include in page name AND details page name **/
					if(a7.indexOf('category_id=')>-1){
						n=a7.substring(a7.indexOf('category_id=')+12);
						n=n.substring(0, n.indexOf(']'));
						if(ovf){pn=s_dell.apl(pn,n,'',0);af=true;}else{pn=s_dell.apl(pn,n,':',0);ovf=true;af=true;}
					}
					if(a7.indexOf('categoryid=')>-1){
						n=a7.substring(a7.indexOf('categoryid=')+11);
						n=n.substring(0, n.indexOf(']'));
						if(ovf){pn=s_dell.apl(pn,n,'',0);af=true;}else{pn=s_dell.apl(pn,n,':',0);ovf=true;af=true;}
					}
					if(a7.indexOf('sku=')>-1 && pn.indexOf('addedtocart')==-1){
						n=a7.substring(a7.indexOf('sku='));
						n='['+n.substring(0, n.indexOf(']')+1);
						if(ovf){pn=s_dell.apl(pn,n,'',0);af=true;}else{pn=s_dell.apl(pn,n,':',0);ovf=true;af=true;}
					}
					if(a7.indexOf('oc=')>-1 && pna[0].indexOf('premier')==-1 && pn.indexOf('dellstore^config')>-1){
						n=a7.substring(a7.indexOf('oc='));
						n='['+n.substring(0, n.indexOf(']')+1);
						if(ovf){pn=s_dell.apl(pn,n,'',0);af=true;}else{pn=s_dell.apl(pn,n,':',0);ovf=true;af=true;}
					}
					if(a7.indexOf('product_id=')>-1){
						n=a7.substring(a7.indexOf('product_id='));
						n='['+n.substring(0, n.indexOf(']')+1);
						if(ovf){pn=s_dell.apl(pn,n,'',0);af=true;}else{pn=s_dell.apl(pn,n,':',0);ovf=true;af=true;}
					}
					if(a7.indexOf('productid=')>-1){
						n=a7.substring(a7.indexOf('productid='));
						n='['+n.substring(0, n.indexOf(']')+1);
						if(ovf){pn=s_dell.apl(pn,n,'',0);af=true;}else{pn=s_dell.apl(pn,n,':',0);ovf=true;af=true;}
					}
					if(a7.indexOf('[~id=')>-1 && pn.indexOf('imagedirect')==-1){
						n=a7.substring(a7.indexOf('id='));
						n='['+n.substring(0, n.indexOf(']')+1);
						if(ovf){pn=s_dell.apl(pn,n,'',0);af=true;}else{pn=s_dell.apl(pn,n,':',0);ovf=true;af=true;}
					}
					if(a7.indexOf('[id=')>-1 && pn.indexOf('imagedirect')==-1){
						n=a7.substring(a7.indexOf('id='));
						n='['+n.substring(0, n.indexOf(']')+1);
						if(ovf){pn=s_dell.apl(pn,n,'',0);af=true;}else{pn=s_dell.apl(pn,n,':',0);ovf=true;af=true;}
					}
					if(a7.indexOf('topic=')>-1){
						n=a7.substring(a7.indexOf('topic='));
						n='['+n.substring(0, n.indexOf(']')+1)
						if(ovf){pn=s_dell.apl(pn,n,'',0);af=true;}else{pn=s_dell.apl(pn,n,':',0);ovf=true;af=true;}
					}
					dpn=pn
	/** values to include in details page name **/
					if(a7.indexOf('section=')>-1){
						n=a7.substring(a7.indexOf('section='));
						n='['+n.substring(0, n.indexOf(']')+1);
						if(ovf){dpn=s_dell.apl(dpn,n,'',0);af=true;}else{dpn=s_dell.apl(dpn,n,':',0);ovf=true;af=true;}
					}				
					if(a7.indexOf('tab=')>-1){
						n=a7.substring(a7.indexOf('tab='));
						n='['+n.substring(0, n.indexOf(']')+1);
						if(ovf){dpn=s_dell.apl(dpn,n,'',0);af=true;}else{dpn=s_dell.apl(dpn,n,':',0);ovf=true;af=true;}
					}
					if(a7.indexOf('page=')>-1){
						n=a7.substring(a7.indexOf('page='));
						n='['+n.substring(0, n.indexOf(']')+1);
						if(ovf){dpn=s_dell.apl(dpn,n,'',0);af=true;}else{dpn=s_dell.apl(dpn,n,':',0);ovf=true;af=true;}
					}
					if(a7.indexOf('brandid=')>-1){
						n=a7.substring(a7.indexOf('brandid='));
						n='['+n.substring(0, n.indexOf(']')+1);
						if(ovf){dpn=s_dell.apl(dpn,n,'',0);af=true;}else{dpn=s_dell.apl(dpn,n,':',0);ovf=true;af=true;}
					}
	/** values to variable only **/
					if(a7.indexOf('cat=')>-1){
						n=a7.substring(a7.indexOf('cat=')+4);
						n=n.substring(0, n.indexOf(']'));
						if(pna[0].indexOf('search')>-1)s_dell.eVar9=n;
						af=true;
					}
				}
				if(!af&&i!=7)pn=s_dell.apl(pn,pna[i],':',0);
				af=false;
			}
			// cleanup - remove trailing colon and undefined
			if(pn.length-1 == pn.lastIndexOf(':'))pn=pn.substring(0,pn.length-1);
			if(pn.indexOf(':undefined')>-1)pn=pn.substring(0,pn.indexOf(':undefined'));
			if(dpn.length-1 == dpn.lastIndexOf(':'))dpn=dpn.substring(0,dpn.length-1);
			
			// cleanup - remove dellstore: from the beginning fo the string for ecomm
			if(pn.indexOf("dellstore:")==0)pn=pn.substring(10,pn.length);
			if(dpn.indexOf("dellstore:")==0)dpn=dpn.substring(10,dpn.length);
			
			s_dell.pageName=pn
			dpn=dpn?s_dell.prop13=dpn:s_dell.prop13=pn;
		}
	}		
	if(!s_dell.pageName)s_dell.pageName=s_dell.prop13=s_dell.prop14=document.location;
	if(!s_dell.prop13)s_dell.prop13=s_dell.pageName;
	if(!s_dell.prop14)s_dell.prop14=s_dell.pageName;
	s_dell.prop29=(nextgen==false)?s_dell.pageName:'ng:'+s_dell.pageName;

/** updated 10/29/08 release **/
	/** handling of AJAX pages **/	
	if(s_dell.pageName.indexOf('ajax')>-1){
		s_dell.prop14=s_dell.pageName;
		if(s_dell.prop13.indexOf(':ajax')>-1){
			s_dell.pageName=s_dell.prop13.substring(0,s_dell.prop13.indexOf(':ajax'));
		}
		else {
			s_dell.pageName=s_dell.prop13;
			s_dell.prop13=s_dell.prop13+':ajax'
		}
		if(s_dell.prop14.indexOf('&')>-1){
			s_dell.prop14=s_dell.prop14.substring(0,s_dell.prop14.indexOf('&'));
		}
	}
/***** END pageName logic *****/

/** added in 10/24/08 release **/
	/*** getPreviousValue: pageName ***/
	s_dell.gpv_pn=''
	if(s_dell.isPageLoad){
		s_dell.gpv_pn=s_dell.getPreviousValue(s_dell.pageName,'gpv_pn','');
	}
	
	
/** revised in 10/24/08 release **/
	/*** Utility script - check inputs for Dell values ***/
	s_dell.ss_kw='';
	s_dell.ss_rff='';
	s_dell.es_on='';
	var inputs = document.getElementsByTagName?document.getElementsByTagName('INPUT'):'';
	for (i=0; i<inputs.length; i++){
		if(inputs[i].getAttribute('ID')=="kw")s_dell.ss_kw=inputs[i].getAttribute('VALUE')
		if(inputs[i].getAttribute('ID')=="rff")s_dell.ss_rff=inputs[i].getAttribute('VALUE')
		if(inputs[i].getAttribute('NAME')=="order_number")s_dell.es_on=inputs[i].getAttribute('VALUE')
	}

/** added in 10/24/08 release **/
	/*** Order Number (uses value from HTML input) ***/
	if(s_dell.es_on)s_dell.prop22=s_dell.es_on;

/** revised in 4/27/09 release **/	
	/*** Site Search (uses values from HTML inputs) ***/
	if(s_dell.ss_kw && s_dell.ss_rff){
		if(s_dell.ss_rff=='1'){
			s_dell.prop7=s_dell.ss_kw;
		}else if(s_dell.ss_rff=='2'){
			s_dell.prop7='reclink:'+s_dell.ss_kw;
		}else if(s_dell.ss_rff=='3'){
			s_dell.prop7='othercat:'+s_dell.ss_kw;
		}else if(s_dell.ss_rff=='0'){
			s_dell.prop7='null:'+s_dell.ss_kw;
		}
	}
	
	if(s_dell.prop7){
		s_dell.prop7=s_dell.prop7.toLowerCase();
		s_dell.eVar36=s_dell.prop7;
		var t_search=s_dell.getValOnce(s_dell.eVar36,'v36',0);
		if(t_search){
			s_dell.events=s_dell.apl(s_dell.events,"event6",",",2);
			s_dell.prop42=s_dell.gpv_pn;
		}
	}

	/*** ServiceTag presented on page ***/
	if(!s_dell.prop17){
		for (i=0; i<inputs.length; i++){
			if(inputs[i].getAttribute('ID')=="ServiceTagMetricsId")s_dell.prop17=inputs[i].getAttribute('VALUE')
		}
	}
	
	/*** Set events based on pageName ***/
	if(s_dell.pageName.indexOf("order^recentorders")>-1 || s_dell.pageName.indexOf("order^details")>-1 || s_dell.pageName.indexOf("order^singlestatus")>-1 || s_dell.pageName.indexOf("order^multiplestatus")>-1){
		s_dell.events=s_dell.apl(s_dell.events,'event11',',',2);
	}
	if(s_dell.pageName.indexOf("dellstore^basket")>-1){
		s_dell.events=s_dell.apl(s_dell.events,'scView',',',2); // set cart visit (config in SC once per visit)
	}
	if(s_dell.pageName.indexOf("chkout")>-1){
		s_dell.events=s_dell.apl(s_dell.events,'scCheckout',',',2); 	// set checkout start (config in SC once per visit)
	}
	
/** revised in 10/30/08 release **/
	/*** Products/Events Handling ***/
	if(s_dell.pageName.indexOf("sna^productdetail")>-1 || s_dell.pageName.indexOf("content^products^productdetails")>-1){
		var prod=s_dell.getQueryParam('sku,oc')
		s_dell.events=s_dell.events?s_dell.events:'';
		if(s_dell.events.indexOf('prodView,event2')>-1)s_dell.events=s_dell.repl(s_dell.events,'prodView,event2','');
		if(prod && s_dell.events.indexOf('event3')>-1){
			if(prod.indexOf(','))prod=s_dell.repl(prod,',',',;');
			s_dell.products=s_dell.apl(s_dell.products,';'+prod,',',2);
		}
		else if(prod){
			prod=s_dell.dedupVal('sku_oc',prod);
			if(prod){
				if(prod.indexOf(','))prod=s_dell.repl(prod,',',',;');
				s_dell.products=s_dell.apl(s_dell.products,';'+prod,',',2);
				s_dell.events=s_dell.apl(s_dell.events,'prodView',',',2);
				s_dell.events=s_dell.apl(s_dell.events,'event2',',',2);
			}
		}
	}
	// manage config starts by unique order code - most recent unique value per session will set the event
	if(s_dell.pageName.indexOf("dellstore^config")>-1){
		var oc=s_dell.getQueryParam('oc');
		if(oc)oc=s_dell.dedupVal('ocstart',oc);
		if(oc){
			s_dell.products=s_dell.apl(s_dell.products,';'+oc,',',2);
			s_dell.events=s_dell.apl(s_dell.events,'event10',',',2);
			s_dell.events=s_dell.apl(s_dell.events,'prodView',',',2);
			s_dell.events=s_dell.apl(s_dell.events,'event2',',',2);
		}
	}
/* updated 11/3/08 */
	// final check to see if we have products with no events, clear products if we do
	if(s_dell.products){
		s_dell.products=s_dell.events?s_dell.products:'';
		s_dell.events=s_dell.events?s_dell.events:'';
		//check for semicolon in products
		if(s_dell.products && s_dell.products.indexOf(';')!=0 && s_dell.events.indexOf('scAdd')>-1){
			var p=s_dell.products;
			if(p.indexOf(';')>-1 && p.indexOf(',;')>-1){
				s_dell.products=';'+p
			}
			else if(p.indexOf(';')>-1){
				var pa=s_dell.split(p,';');
				p=';'+pa[0];
				for(var i=1;i<pa.length;i++)p+=',;'+pa[i];
				s_dell.products=p;
			}
			else{
				s_dell.products=';'+p
			}
		}
	}


/** NOT TESTED - TEST AT DELL **/	
	/*** Set events based on page URL ***/
	var loc=''+document.location
	// set application start
	if(loc.indexOf("/financing/app.aspx")>-1 || loc.indexOf("/financing/us_ca/app.aspx")>-1 || loc.indexOf("/financing/process.aspx")>-1){
		s_dell.events=s_dell.apl(s_dell.events,'event8',',',2);
	}
	// set application complete
	if(loc.indexOf("/financing/approved.aspx")>-1 || loc.indexOf("/financing/us_ca/approved.aspx")>-1 || loc.indexOf("/financing/declined.aspx")>-1 || loc.indexOf("/financing/reviewed.aspx")>-1 || loc.indexOf("/financing/us_ca/reviewed.aspx")>-1){
		s_dell.events=s_dell.apl(s_dell.events,'event9',',',2);
	}
/* updated 11/3/08 */
	// set S&P Visits
	if(loc.indexOf("accessories")>-1 || s_dell.pageName.indexOf("accessories")>-1){
		s_dell.events=s_dell.apl(s_dell.events,'event12',',',2);
	}

	/*** Read and format cookie values ***/
	s_dell.prop45=s_dell.c_r('GAAuth');
	if(!s_dell.prop45){
		var cookieArray = document.cookie.split(';');
		for(var i=0;i < cookieArray.length;i++) {
			var cookie = cookieArray[i];
			while (cookie.charAt(0)==' ') cookie = cookie.substring(1,cookie.length);
			if(cookie.match(/gahot=/i)) s_dell.prop45=cookie.substring(6,cookie.length);
		}
	}
	s_dell.prop46=s_dell.c_r('Profile')?s_dell.c_r('Profile'):s_dell.c_r('profile');
	

// issues with this cookie read for prt:Prof?
	s_dell.prop48=s_dell.parseCookie('prt:Prof','cnm,sid,cs',',')
	s_dell.prop49=s_dell.parseCookie('lwp','c,cs,l,s,systemid,servicetag',',')
	s_dell.prop16=s_dell.parseCookie('StormPCookie','penv',',')
		s_dell.prop16=s_dell.getQueryParam('penv','',s_dell.prop16)
	
	if(s_dell.prop45)s_dell.prop12="logged in";else s_dell.prop12="not logged in";
	
/*** Campaign tracking ***/
	if(!s_dell.campaign)s_dell.campaign=s_dell.getQueryParam('cid'); //landing_page_cid
		s_dell.campaign=s_dell.getValOnce(s_dell.campaign,"cid",0)
	if(!s_dell.eVar1)s_dell.eVar1=s_dell.getQueryParam('lid');    //landing_page_lid
		s_dell.eVar1=s_dell.getValOnce(s_dell.eVar1,"lid",0)
	if(!s_dell.eVar2){var dgc=s_dell.getQueryParam('dgc');s_dell.eVar2=dgc}    //landing_page_dgv
		s_dell.eVar2=s_dell.getValOnce(s_dell.eVar2,"dgc",0)
	if(!s_dell.eVar3)s_dell.eVar3=s_dell.getQueryParam('st');     //external_search_keyword
		s_dell.eVar3=s_dell.getValOnce(s_dell.eVar3,"st",0)
	if(!s_dell.eVar28)s_dell.eVar28=s_dell.getQueryParam('acd');   //affiliate code
		s_dell.eVar28=s_dell.getValOnce(s_dell.eVar28,"acd",0)
	if(!s_dell.eVar43)s_dell.eVar43=s_dell.getQueryParam('mid');   //aprimo message id
		s_dell.eVar43=s_dell.getValOnce(s_dell.eVar43,"mid",0)
	if(!s_dell.eVar44)s_dell.eVar44=s_dell.getQueryParam('rid');   //aprimo recipient id
		s_dell.eVar44=s_dell.getValOnce(s_dell.eVar44,"rid",0)

	/*** ODG visits ***/
	if(typeof(s_dell.linkType)=='undefined'){
		s_dell.odgValues='|af|ba|bf|cj|co|db|dc|ds|ec|em|ls|mb|ms|mt|rs|sm|ss|st|';
		s_dell.internalURLRegex='(javascript:|dellpartnerdirect.com|.dell|allpowerful.com|ideastorm.com)';
		var country,
			segment,
			countrySegment;
		country = s_dell.getQueryParam('c');
		segment = s_dell.getQueryParam('s');
		if (country && segment){
			countrySegment = country + '-' + segment;
		}else {
			if(s_dell.prop49){
				var prop49Array = s_dell.split(s_dell.prop49.substring(1,s_dell.prop49.length),'&');
				for (i in prop49Array){
					if(prop49Array[i].toString().indexOf('c=')==0){
						country = prop49Array[i].substring(prop49Array[i].indexOf('c=')+2,prop49Array[i].length);
					}else if(prop49Array[i].toString().indexOf('s')==0){
						segment = prop49Array[i].substring(prop49Array[i].indexOf('s=')+2,prop49Array[i].length);
					}
				}
				if (country && segment){
					countrySegment = country + '-' + segment;
				}
			}
		}
		if(countrySegment){
			var d=new Date(),
				valueNotDeleted=true;
			if(s_dell.c_r('e21')&&s_dell.c_r('e21').indexOf(countrySegment)>-1){
				var e21Array=s_dell.split(s_dell.c_r('e21'),'::');
				for (i in e21Array){
					if(e21Array[i].toString().indexOf(countrySegment)>-1){
						var e21Array2=s_dell.split(e21Array[i],':');
						if(d.getTime()>e21Array2[1]){
							if(e21Array.length==1){
								d.setTime(d.getTime()-86400000);
								s_dell.c_w('e21','',d);
							}
							else{
								e21Array.splice(i,1);
								d.setTime(d.getTime()+30*86400000);
								s_dell.c_w('e21',e21Array,d);
							}
							valueNotDeleted=false;
						}
						if(valueNotDeleted){
							var tempReferrer=s_dell.d.referrer.substring(0,s_dell.d.referrer.indexOf('?'))
							if((s_dell.eVar2&&s_dell.odgValues.indexOf(s_dell.eVar2.toLowerCase()+'|')==-1&&s_dell.eVar2!='ir')||(!s_dell.getQueryParam('dgc')&&tempReferrer&&!tempReferrer.match(s_dell.internalURLRegex))){
								if(e21Array.length==1){
									d.setTime(d.getTime()-86400000);
									s_dell.c_w('e21','',d);
								}
								else{
									e21Array.splice(i,1);
									d.setTime(d.getTime()+30*86400000);
									s_dell.c_w('e21',e21Array,d);
								}
							}
							else{
								s_dell.events=s_dell.apl(s_dell.events,'event21',',',1);
							}
						}
					}
				}	
			}else if((s_dell.eVar2&&s_dell.odgValues.indexOf(s_dell.eVar2.toLowerCase()+'|')>-1)){
				s_dell.events=s_dell.apl(s_dell.events,'event21',',',1);
				d.setTime(d.getTime()+30*86400000);
				var e21Cookie=s_dell.c_r('e21');
				e21Cookie=e21Cookie?e21Cookie+'::'+countrySegment+':'+d.getTime():countrySegment+':'+d.getTime();
				s_dell.c_w('e21',e21Cookie,d);
			}
		}
	}

/*** Internal Promos tracking ***/
	// if a IR code is present, clear other campaign variables
	if(dgc&&dgc.toLowerCase()=="ir"){
		s_dell.eVar29=s_dell.getQueryParam('cid') + ":" + s_dell.getQueryParam('lid');
		s_dell.eVar29=s_dell.getValOnce(s_dell.eVar29,"ir",0);
		s_dell.campaign="";
		s_dell.eVar1="";
		s_dell.eVar2="";
		s_dell.eVar3="";
		s_dell.eVar28="";
	}

/** added in 10/24/08 release **/
	/*** FIX: blank eVar30 value when pagename contains "ecomm" ***/
	loc=''+document.location;
	if(loc.indexOf('ecomm')>-1 && s_dell.eVar30){
		s_dell.eVar30='';
		s_dell.eVar30='';
		if(s_dell.events.indexOf('event10')>-1){
			var eventlist=s_dell.split(s_dell.events,',');
			for(i in eventlist){
				if(eventlist[i]=='event10')eventlist[i]=''
			}
			s_dell.events='';
			for(i in eventlist){
				if(eventlist[i])s_dell.events=s_dell.apl(s_dell.events,eventlist[i],',',2);
			}
		}
	}
	
/** added on 07/31/09 **/
	/*** FIX: prefix ANAV caption ***/
	loc=''+document.location;
	loc=loc&&loc.indexOf('?')>-1?loc.substring(0, loc.indexOf('?')).toLowerCase():loc.toLowerCase();
	
/** updated in 10/29/08 release **/	
	/*** Set misc variables ***/
	if(!s_dell.server){
		s_dell.server=document.location.hostname;
	}

/** added in 5/19/09 release **/
	/*** T&T integration ***/
	s_dell.tnt=s_dell.trackTNT();
	
/** added in 8/09 release **/
	/*** file downloads ***/
	s_dell.downloadURL=s_dell.downloadLinkHandler();
	if(s_dell.downloadURL){
		s_dell.prop33=s_dell.downloadURL;
		s_dell.prop33=s_dell.prop33.indexOf('//')?s_dell.prop33.substring(s_dell.prop33.indexOf('//')+2):s_dell.prop33;
		s_dell.eVar23=s_dell.prop33;
		s_dell.prop32=s_dell.pageName;
		s_dell.events=s_dell.apl(s_dell.events,'event24',',',2);
				
		s_dell.linkTrackVars=s_dell.apl(s_dell.linkTrackVars,'prop32',',',2);
		s_dell.linkTrackVars=s_dell.apl(s_dell.linkTrackVars,'prop33',',',2);
		s_dell.linkTrackVars=s_dell.apl(s_dell.linkTrackVars,'eVar23',',',2);
		s_dell.linkTrackVars=s_dell.apl(s_dell.linkTrackVars,'events',',',2);
		s_dell.linkTrackEvents=s_dell.apl(s_dell.linkTrackEvents,'event24',',',2);
	}
	
/** added in 8/09 release **/
	/*** iPerceptions integration ***/
	if(window.location.protocol=='https:'){
		s_dell.iPerceptionsURL='https://si.cdn.dell.com/images/global/omniture/ipge.htm';		
	}else{
		s_dell.iPerceptionsURL='http://i.dell.com/images/global/omniture/ipge.htm';
	}
	s_dell.GenesisExchange.setExchangePageURL('iPerceptions', s_dell.iPerceptionsURL);

/** navigation method - merchandising eVar **/
	if(s_dell.inList('event6',s_dell.events,',')){
		s_dell.eVar40='site search';
	}else if(s_dell.eVar30&&s_dell.eVar31){
		s_dell.eVar40='anav';
		if(s_dell.p_gh()) s_dell.linkTrackVars=s_dell.apl(s_dell.linkTrackVars,'eVar40',',',2);
	}else if(s_dell.getQueryParam('~ck').toLowerCase()=='mn'){
		s_dell.eVar40='masthead';
	}else if(s_dell.getQueryParam('~ck').toLowerCase()=='hbn'||s_dell.getQueryParam('ref').toLowerCase()=='hbn'||s_dell.getQueryParam('~ck').toLowerCase()=='bnn'||s_dell.getQueryParam('ref').toLowerCase()=='bnn'){
		s_dell.eVar40='banner';
	}else if(s_dell.pageName){
		if(s_dell.pageName.indexOf('advisorweb')>-1){
			s_dell.eVar40='advisor';
		}
	}
	
/** updated in 8/09 release **/	
	/*** force all variables to lowercase ***/
	s_dell.manageVars('lowercaseVars','events',2);
	
	s_dell.linkTrackVars=s_dell.apl(s_dell.linkTrackVars,'prop49',',',2);
	s_dell.linkTrackVars=s_dell.apl(s_dell.linkTrackVars,'prop46',',',2);
}
s_dell.doPlugins=s_dell_doPlugins
/************************** PLUGINS SECTION *************************/
/* You may insert any plugins you wish to use here.                 */
// Omniture Ad Track
var ADValue = "";
adTrackimpression();
function adTrackimpression() {
	try {
		jQuery(".omnitureADTrack[@omnitureadid]").each(function() {
			jQuery(this).click(function() {
				try {
					s_dell.eVar6 = jQuery(this).attr("omnitureadid");
					s_dell.prop28 = "";
					s_dell.linkTrackVars = s_dell.apl(s_dell.linkTrackVars, 'eVar6', ',', 2);
					s_dell.tl(this, 'o', 'ADTrack');
					s_dell.eVar6 = "";
				}
				catch (e){}
			});
			if (ADValue != null && ADValue.length > 0 && ADValue != "undefined") {
				ADValue += '|';
			}
			ADValue += jQuery(this).attr("omnitureadid");
		});
		s_dell.prop28 = ADValue;
	}
	catch (e) {}
}

/*
 * Plugin: getQueryParam 2.3
 */
s_dell.getQueryParam=new Function("p","d","u",""
+"var s=this,v='',i,t;d=d?d:'';u=u?u:(s.pageURL?s.pageURL:s.wd.locati"
+"on);if(u=='f')u=s.gtfs().location;while(p){i=p.indexOf(',');i=i<0?p"
+".length:i;t=s.p_gpv(p.substring(0,i),u+'');if(t){t=t.indexOf('#')>-"
+"1?t.substring(0,t.indexOf('#')):t;}if(t)v+=v?d+t:t;p=p.substring(i="
+"=p.length?i:i+1)}return v");
s_dell.p_gpv=new Function("k","u",""
+"var s=this,v='',i=u.indexOf('?'),q;if(k&&i>-1){q=u.substring(i+1);v"
+"=s.pt(q,'&','p_gvf',k)}return v");
s_dell.p_gvf=new Function("t","k",""
+"if(t){var s=this,i=t.indexOf('='),p=i<0?t:t.substring(0,i),v=i<0?'T"
+"rue':t.substring(i+1);if(p.toLowerCase()==k.toLowerCase())return s."
+"epa(v)}return ''");

/*
 * Plugin: getValOnce 0.2 - get a value once per session or number of days
 */
s_dell.getValOnce=new Function("v","c","e",""
+"var s=this,k=s.c_r(c),a=new Date;e=e?e:0;if(v){a.setTime(a.getTime("
+")+e*86400000);s.c_w(c,v,e?a:0);}return v==k?'':v");
/*
 * Plugin: getPreviousValue_v1.0 - return previous value of designated
 *   variable (requires split utility)
 */
s_dell.getPreviousValue=new Function("v","c","el",""
+"var s=this,t=new Date,i,j,r='';t.setTime(t.getTime()+1800000);if(el"
+"){if(s.events){i=s.split(el,',');j=s.split(s.events,',');for(x in i"
+"){for(y in j){if(i[x]==j[y]){if(s.c_r(c)) r=s.c_r(c);v?s.c_w(c,v,t)"
+":s.c_w(c,'no value',t);return r}}}}}else{if(s.c_r(c)) r=s.c_r(c);v?"
+"s.c_w(c,v,t):s.c_w(c,'no value',t);return r}");

/*
 * DynamicObjectIDs v1.4: Setup Dynamic Object IDs based on URL
 */
s_dell.setupDynamicObjectIDs=new Function(""
+"var s=this;if(!s.doi){s.doi=1;if(s.apv>3&&(!s.isie||!s.ismac||s.apv"
+">=5)){if(s.wd.attachEvent)s.wd.attachEvent('onload',s.setOIDs);else"
+" if(s.wd.addEventListener)s.wd.addEventListener('load',s.setOIDs,fa"
+"lse);else{s.doiol=s.wd.onload;s.wd.onload=s.setOIDs}}s.wd.s_semapho"
+"re=1}");
s_dell.setOIDs=new Function("e",""
+"var s=s_c_il["+s_dell._in+"],b=s.eh(s.wd,'onload'),o='onclick',x,l,u,c,i"
+",a=new Array;if(s.doiol){if(b)s[b]=s.wd[b];s.doiol(e)}if(s.d.links)"
+"{for(i=0;i<s.d.links.length;i++){l=s.d.links[i];c=l[o]?''+l[o]:'';b"
+"=s.eh(l,o);z=l[b]?''+l[b]:'';u=s.getObjectID(l);if(u&&c.indexOf('s_"
+"objectID')<0&&z.indexOf('s_objectID')<0){u=s.repl(u,'\"','');u=s.re"
+"pl(u,'\\n','').substring(0,97);l.s_oc=l[o];a[u]=a[u]?a[u]+1:1;x='';"
+"if(c.indexOf('.t(')>=0||c.indexOf('.tl(')>=0||c.indexOf('s_gs(')>=0"
+")x='var x=\".tl(\";';x+='s_objectID=\"'+u+'_'+a[u]+'\";return this."
+"s_oc?this.s_oc(e):true';if(s.isns&&s.apv>=5)l.setAttribute(o,x);l[o"
+"]=new Function('e',x)}}}s.wd.s_semaphore=0;return true");

/*
 * Plugin: downloadLinkHandler 0.5 - identify and report download links
 */
s_dell.downloadLinkHandler=new Function("p",""
+"var s=this,h=s.p_gh(),n='linkDownloadFileTypes',i,t;if(!h||(s.linkT"
+"ype&&(h||s.linkName)))return '';i=h.indexOf('?');t=s[n];s[n]=p?p:t;"
+"if(s.lt(h)=='d')s.linkType='d';else h='';s[n]=t;return h;");

/*
 * Utility Function: p_gh
 */
s_dell.p_gh=new Function(""
+"var s=this;if(!s.eo&&!s.lnk)return '';var o=s.eo?s.eo:s.lnk,y=s.ot("
+"o),n=s.oid(o),x=o.s_oidt;if(s.eo&&o==s.eo){while(o&&!n&&y!='BODY'){"
+"o=o.parentElement?o.parentElement:o.parentNode;if(!o)return '';y=s."
+"ot(o);n=s.oid(o);x=o.s_oidt}}return o.href?o.href:'';");
/*
 * Utility clearVars v0.1 - clear variable values (requires split 1.5)
 */
s_dell.clearVars=new Function("l","f",""
+"var s=this,vl,la,vla;l=l?l:'';f=f?f:'';vl='pageName,purchaseID,chan"
+"nel,server,pageType,campaign,state,zip,events,products';for(var n=1"
+";n<51;n++)vl+=',prop'+n+',eVar'+n+',hier'+n;if(l&&(f==1||f==2)){if("
+"f==1){vl=l}if(f==2){la=s.split(l,',');vla=s.split(vl,',');vl='';for"
+"(x in la){for(y in vla){if(la[x]==vla[y]){vla[y]=''}}}for(y in vla)"
+"{vl+=vla[y]?','+vla[y]:'';}}s.pt(vl,',','p_clr',0);return true}else"
+" if(l==''&&f==''){s.pt(vl,',','p_clr',0);return true}else{return fa"
+"lse}");
s_dell.p_clr=new Function("t","var s=this;s[t]=''");
/*
 * Plugin Utility: apl v1.1
 */
s_dell.apl=new Function("l","v","d","u",""
+"var s=this,m=0;if(!l)l='';if(u){var i,n,a=s.split(l,d);for(i=0;i<a."
+"length;i++){n=a[i];m=m||(u==1?(n==v):(n.toLowerCase()==v.toLowerCas"
+"e()));}}if(!m)l=l?l+d+v:v;return l");
/*
 * Utility: inList v1.0 - find out if a value is in a list
 */
s_dell.inList=new Function("v","l","d",""
+"var s=this,ar=Array(),i=0,d=(d)?d:',';if(typeof(l)=='string'){if(s."
+"split)ar=s.split(l,d);else if(l.split)ar=l.split(d);else return-1}e"
+"lse ar=l;while(i<ar.length){if(v==ar[i])return true;i++}return fals"
+"e;");
/*
 * Plugin Utility: split v1.5 (JS 1.0 compatible)
 */
s_dell.split=new Function("l","d",""
+"var i,x=0,a=new Array;while(l){i=l.indexOf(d);i=i>-1?i:l.length;a[x"
+"++]=l.substring(0,i);l=l.substring(i+d.length);}return a");
/*
 * Plugin Utility: replace v1.0
 */
s_dell.repl=new Function("x","o","n",""
+"var i=x.indexOf(o),l=n.length;while(x&&i>=0){x=x.substring(0,i)+n+x."
+"substring(i+o.length);i=x.indexOf(o,i+l)}return x");
/*
 * Utility manageVars v0.2 - clear variable values (requires split 1.5)
 */
s_dell.manageVars=new Function("c","l","f",""
+"var s=this,vl,la,vla;l=l?l:'';f=f?f:1 ;if(!s[c])return false;vl='pa"
+"geName,purchaseID,channel,server,pageType,campaign,state,zip,events"
+",products,transactionID';for(var n=1;n<51;n++){vl+=',prop'+n+',eVar"
+"'+n+',hier'+n;}if(l&&(f==1||f==2)){if(f==1){vl=l;}if(f==2){la=s.spl"
+"it(l,',');vla=s.split(vl,',');vl='';for(x in la){for(y in vla){if(l"
+"a[x]==vla[y]){vla[y]='';}}}for(y in vla){vl+=vla[y]?','+vla[y]:'';}"
+"}s.pt(vl,',',c,0);return true;}else if(l==''&&f==1){s.pt(vl,',',c,0"
+");return true;}else{return false;}");
s_dell.clearVars=new Function("t","var s=this;s[t]='';");
s_dell.lowercaseVars=new Function("t",""
+"var s=this;if(s[t]){s[t]=s[t].toLowerCase();}");

/*
 * Custom Dell Plugin: parseCookie for desired params, format as query string 
 * (requires s.split, s.apl)
 */
s_dell.parseCookie=new Function("c","pl","d",""
+"var s=this,pla,ca,o='',j,l;c=s.c_r(c);if(c){pla=s.split(pl,d);ca=s.s"
+"plit(c,'&');for(x in pla){for(y in ca){j=pla[x]+'=';l=''+ca[y];l=l.t"
+"oLowerCase();l=l.indexOf(j.toLowerCase());if(l>-1)o=s.apl(o,ca[y],'&"
+"',0)}}if(o)o='?'+o;}return o");
/*
 * Custom Dell Plugin: dedupVal 
 */
s_dell.dedupVal=new Function("c","v",""
+"var s=this,r;if(s.c_r(c)){r=s.c_r(c);if(v==r)return '';else s.c_w(c,"
+"v)}else{s.c_w(c,v)}return v");

/*
* TNT Integration Plugin v1.0
*/
s_dell.trackTNT =new Function("v","p","b",""
+"var s=this,n='s_tnt',p=p?p:n,v=v?v:n,r='',pm=false,b=b?b:true;if(s."
+"getQueryParam){pm=s.getQueryParam(p);}if(pm){r+=(pm+',');}if(s.wd[v"
+"]!=undefined){r+=s.wd[v];}if(b){s.wd[v]='';}return r;");

/**
 * GenesisExchange v0.3.1 (compact)
 */ 
s_dell.createGEObject=new Function("s",""
+"var _g=new Object;_g.s=s;_g.p=new Object;_g.setPartnerEventHandler=function(pId,eh){if(!this.p[pId]){this.p[pId]=new Object;}this.p[pId].__eh=eh;};_g.firePartnerEvent=function(pId,eId,ePm){this.p[pId"
+"].__eh(eId,ePm);};_g.setExchangePageURL=function(pId,url){if(!this.p[pId]){this.p[pId]=new Object;}this.p[pId].__ep=url;};_g.getPageData=function(pId){var q='';if(this.p[pId]&&this.p[pId].__ep){q+='g"
+"e_pId='+this._euc(pId)+'&ge_url='+this._euc(this.p[pId].__ep)+'&pageURL='+this._euc(document.location.href);}var v='pageName,server,channel,pageType,products,events,campaign,purchaseID,hier1,hier2,hi"
+"er3,hier4,hier5';for(var i=1;i<=50;i++){v+=',prop'+i+',eVar'+i;}var a=this._split(v,',');for(var i=0;i<a.length;i++){if(this.s[a[i]]){q+=(q?'&':'')+a[i]+'='+this._euc(this.s[a[i]]);}}return q;};_g.ge"
+"tObjectFromQueryString=function(qsParam){var v=this._getQParam(qsParam);var r=new Object;if(v){v=this._duc(v);l=this._split(v,'&');for(i=0;i<l.length;i++){kv=this._split(l[i],'=');r[kv[0]]=this._duc("
+"kv[1]);}}return r;};_g.productsInsert=function(p,e,v){var i=0,j=0,r='',pd=this._split(p,',');for(i=0;i<pd.length;i++){if(i>0){r+=',';}var el=this._split(pd[i],';');for(j=0;j<6;j++){if(j<4){r+=(el.len"
+"gth>j?el[j]:'')+';';}else if(j==4){r+=(el[j]?el[j]+'|':'')+e+';';}else if(j==5){r+=(el[j]?el[j]+'|':'')+v;}}}return r;};_g._getQParam=function(k,q){var m=this,l,i,kv;if(q==undefined||!q){q=window.loc"
+"ation.href;}if(q){i=q.indexOf('?');if(i>=0){q=q.substring(i+1);}l=this._split(q,'&');for(i=0;i<l.length;i++){kv=this._split(l[i],'=');if(kv[0]==k){return kv[1];}}}return '';};_g._euc=function(str){re"
+"turn typeof encodeURIComponent=='function'?encodeURIComponent(str):escape(str);};_g._duc=function(str){return typeof decodeURIComponent=='function'?decodeURIComponent(str):unescape(str);};_g._split=fun"
+"ction(l,d){var i,x=0,a=new Array;while(l){i=l.indexOf(d);i=i>-1?i:l.length;a[x++]=l.substring(0,i);l=l.substring(i+d.length);}return a;};return _g;");
s_dell.GenesisExchange=s_dell.createGEObject(s_dell);

/*
 * Function - read combined cookies v 0.3
 */
if(!s_dell.__ccucr){s_dell.c_rr=s_dell.c_r;s_dell.__ccucr = true;
s_dell.c_r=new Function("k",""
+"var s=this,d=new Date,v=s.c_rr(k),c=s.c_rr('s_pers'),i,m,e;if(v)ret"
+"urn v;k=s.ape(k);i=c.indexOf(' '+k+'=');c=i<0?s.c_rr('s_sess'):c;i="
+"c.indexOf(' '+k+'=');m=i<0?i:c.indexOf('|',i);e=i<0?i:c.indexOf(';'"
+",i);m=m>0?m:e;v=i<0?'':s.epa(c.substring(i+2+k.length,m<0?c.length:"
+"m));if(m>0&&m!=e)if(parseInt(c.substring(m+1,e<0?c.length:e))<d.get"
+"Time()){d.setTime(d.getTime()-60000);s.c_w(s.epa(k),'',d);v='';}ret"
+"urn v;");}
/*
 * Function - write combined cookies v 0.3
 */
if(!s_dell.__ccucw){s_dell.c_wr=s_dell.c_w;s_dell.__ccucw = true;
s_dell.c_w=new Function("k","v","e",""
+"this.new2 = true;"
+"var s=this,d=new Date,ht=0,pn='s_pers',sn='s_sess',pc=0,sc=0,pv,sv,"
+"c,i,t;d.setTime(d.getTime()-60000);if(s.c_rr(k)) s.c_wr(k,'',d);k=s"
+".ape(k);pv=s.c_rr(pn);i=pv.indexOf(' '+k+'=');if(i>-1){pv=pv.substr"
+"ing(0,i)+pv.substring(pv.indexOf(';',i)+1);pc=1;}sv=s.c_rr(sn);i=sv"
+".indexOf(' '+k+'=');if(i>-1){sv=sv.substring(0,i)+sv.substring(sv.i"
+"ndexOf(';',i)+1);sc=1;}d=new Date;if(e){if(e.getTime()>d.getTime())"
+"{pv+=' '+k+'='+s.ape(v)+'|'+e.getTime()+';';pc=1;}}else{sv+=' '+k+'"
+"='+s.ape(v)+';';sc=1;}if(sc) s.c_wr(sn,sv,0);if(pc){t=pv;while(t&&t"
+".indexOf(';')!=-1){var t1=parseInt(t.substring(t.indexOf('|')+1,t.i"
+"ndexOf(';')));t=t.substring(t.indexOf(';')+1);ht=ht<t1?t1:ht;}d.set"
+"Time(ht);s.c_wr(pn,pv,d);}return v==s.c_r(s.epa(k));");}

/* WARNING: Changing any of the below variables will cause drastic
changes to how your visitor data is collected.  Changes should only be
made when instructed to do so by your account manager.*/
s_dell.visitorNamespace="dell"
s_dell.trackingServer="nsm.dell.com"
s_dell.trackingServerSecure="sm.dell.com"
s_dell.dc=112

s_dell.lwpParams='http://www.dell.com'+s_dell.parseCookie('lwp','c,cs,l,s,systemid,servicetag',',');
s_dell.surveyCheck=s_dell.getQueryParam('cs','',s_dell.lwpParams);
s_dell.surveyCheck=s_dell.surveyCheck.toLowerCase();
// don't load survey for these cookie IDs
if(s_dell.surveyCheck!='rc1047167'&&s_dell.surveyCheck!='rc1193519'&&s_dell.surveyCheck!='rc1193518'){
	s_dell.loadModule("Survey")
	s_dell.Survey.suites="dellglobalonline";
}

/****************************** MODULES *****************************/
/* Module: Survey */
s_dell.m_Survey_c="var m=s.m_i(\"Survey\");m.launch=function(i,e,c,o,f){this._boot();var m=this,g=window.s_sv_globals||{},l,j;if(g.unloaded||m._blocked())return 0;i=i&&i.constructor&&i.constructor==A"
+"rray?i:[i];l=g.manualTriggers;for(j=0;j<i.length;++j)l[l.length]={l:m._suites,i:i[j],e:e||0,c:c||0,o:o||0,f:f||0};m._execute();return 1;};m._t=function(){this._boot();var m=this,s=m.s,g=window.s_sv"
+"_globals||{},l;if(m._blocked())return;l=g.pageImpressions;l[l.length]={l:m._suites,n:s.pageName||\"\",u:s.pageURL||\"\",r:s.referrer||\"\",c:s.campaign||\"\"};m._execute();};m._rr=function(){var g="
+"window.s_sv_globals||{},f=g.onScQueueEmpty||0;if(f)f();};m._blocked=function(){var m=this,g=window.s_sv_globals||{};return !m._booted||g.stop||!g.pending&&!g.triggerRequested;};m._execute=function("
+"){if(s_sv_globals.execute)setTimeout(\"s_sv_globals.execute();\",0);};m._boot=function(){var m=this,s=m.s,w=window,g,c,d=s.dc,e=s.visitorNamespace,n=navigator.appName.toLowerCase(),a=navigator.user"
+"Agent,v=navigator.appVersion,h,i,j,k,l,b;if(w.s_sv_globals)return;if(!((b=v.match(/AppleWebKit\\/([0-9]+)/))?521<b[1]:n==\"netscape\"?a.match(/gecko\\//i):(b=a.match(/opera[ \\/]?([0-9]+).[0-9]+/i)"
+")?7<b[1]:n==\"microsoft internet explorer\"&&!v.match(/macintosh/i)&&(b=v.match(/msie ([0-9]+).([0-9]+)/i))&&(5<b[1]||b[1]==5&&4<b[2])))return;g=w.s_sv_globals={};g.module=m;g.pending=0;g.incomingL"
+"ists=[];g.pageImpressions=[];g.manualTriggers=[];e=\"survey\";c=g.config={};m._param(c,\"dynamic_root\",(e?e+\".\":\"\")+d+\".2o7.net/survey/dynamic\");m._param(c,\"gather_root\",(e?e+\".\":\"\")+d"
+"+\".2o7.net/survey/gather\");g.url=location.protocol+\"//\"+c.dynamic_root;g.gatherUrl=location.protocol+\"//\"+c.gather_root;g.dataCenter=d;g.onListLoaded=new Function(\"r\",\"b\",\"d\",\"i\",\"l"
+"\",\"s_sv_globals.module._loaded(r,b,d,i,l);\");m._suites=(m.suites||s.un).toLowerCase().split(\",\");l=m._suites;b={};for(j=0;j<l.length;++j){i=l[j];if(i&&!b[i]){h=i.length;for(k=0;k<i.length;++k)"
+"h=(h&0x03ffffff)<<5^h>>26^i.charCodeAt(k);b[i]={url:g.url+\"/suites/\"+(h%251+100)+\"/\"+encodeURIComponent(i.replace(/\\|/,\"||\").replace(/\\//,\"|-\"))};++g.pending;}}g.suites=b;setTimeout(\"s_s"
+"v_globals.module._load();\",0);m._booted=1;};m._param=function(c,n,v){var p=\"s_sv_\",w=window,u=\"undefined\";if(typeof c[n]==u)c[n]=typeof w[p+n]==u?v:w[p+n];};m._load=function(){var m=this,g=s_s"
+"v_globals,q=g.suites,r,i,n=\"s_sv_sid\",b=m.s.c_r(n);if(!b){b=parseInt((new Date()).getTime()*Math.random());m.s.c_w(n,b);}for(i in q){r=q[i];if(!r.requested){r.requested=1;m._script(r.url+\"/list."
+"js?\"+b);}}};m._loaded=function(r,b,d,i,l){var m=this,g=s_sv_globals,n=g.incomingLists;--g.pending;if(!g.commonRevision){g.bulkRevision=b;g.commonRevision=r;g.commonUrl=g.url+\"/common/\"+b;}else i"
+"f(g.commonRevision!=r)return;if(!l.length)return;n[n.length]={r:i,l:l};if(g.execute)g.execute();else if(!g.triggerRequested){g.triggerRequested=1;m._script(g.commonUrl+\"/trigger.js\");}};m._script"
+"=function(u){var d=document,e=d.createElement(\"script\");e.type=\"text/javascript\";e.src=u;d.getElementsByTagName(\"head\")[0].appendChild(e);};if(m.onLoad)m.onLoad(s,m)";
s_dell.m_i("Survey");

/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_code='',s_objectID;function s_gi(un,pg,ss){var c="=fun`p(~.substring(~){`Ns=^0~#G ~.indexOf(~;$D~`a$D~=new Fun`p(~.length~.toLowerCase()~=new Object~`Ns#Lc_il['+s^L@9],~};s.~){$D~`bMigrationSer"
+"ver~.toUpperCase~s.wd~','~);s.~')q='~=new Array~ookieDomainPeriods~.location~var ~^OingServer~dynamicAccount~link~s.m_~=='~s.apv~^zc_i~BufferedRequests~Element~)$Dx^e!Object#lObject.prototype#lObje"
+"ct.prototype[x])~:#q+~etTime~else ~visitor~='+@h(~referrer~s.pt(~s.maxDelay~}c#R(e){~=''~.lastIndexOf(~@g(~}$D~for(~.protocol~=new Date~^zobjectID=@k=$R=$Rv1=$Rv2=$Rv3~ction~javaEnabled~onclick~Nam"
+"e~ternalFilters~javascript~s.dl~@Bs.b.addBehavior(\"# default# ~=parseFloat(~typeof(v)==\"~window~this~cookie~while(~s.vl_g~Type~;i++){~tfs~s.un~&&s.~o^zoid~browser~.parent~document~colorDepth~Stri"
+"ng~.host~s.fl(~s.eo~'+tm@V~s.sq~parseInt(~._i~s.p_l~t=s.ot(o)~track~nload~');~j='1.~#fURL~}else{~s.c_r(~s.c_w(~s.vl_l~lugins~'){q='~dynamicVariablePrefix~Sampling~s.rc[un]~)s.d.write(~Event~&&(~loa"
+"dModule~resolution~'s_~s.eh~s.isie~\"m_\"+n~Secure~Height~tcf~isopera~ismac~escape(~.href~screen.~s#Lgi(~Version~harCode~\"'+~name~variableProvider~.s_~idth~)s_sv(v,n[k],i)}~')>=~){s.~)?'Y':'N'~u=m"
+"[t+1](~i)clearTimeout(~e&&l$mSESSION'~&&!~n+'~home#f~;try{~.src~,$y)~s.ss~s.rl[u~o.type~s.vl_t~=s.sp(~Lifetime~s.gg('objectID~$7new Image;i~sEnabled~ExternalLinks~\",\"~charSet~lnk~onerror~http~cur"
+"rencyCode~disable~.get~MigrationKey~(''+~'+(~f',~){t=~){p=~r=s[f](~u=m[t](~Opera~Math.~s.rep~s.ape~s.fsg~s.oun~s.ppu~s.ns6~conne~InlineStats~&&l$mNONE'~Track~'0123456789~true~height~ in ~+\"_c\"]~s"
+".epa(~t.m_nl~s.va_t~m._d~=1 border=~s.d.images~n=s.oid(o)~,'sqs',q);~LeaveQuery~?'&~'=')~n){~n]=~),\"\\~){n=~'_'+~'+n;~,255)}~if(~vo)~s.sampled~=s.oh(o);~'<im'+'g ~1);~&&o~:'';h=h?h~sess~campaign~l"
+"if~1900~s.co(~ffset~s.pe~m._l~s.c_d~s.brl~s.nrs~s[mn]~,'vo~s.pl~=(apn~space~\"s_gs(\")~vo._t~b.attach~2o7.net'~ alt=\"\">~Listener~.set~Year(~d.create~=s.n.app~)}}}~!='~'||t~)+'/~+'\")~s()+'~():''~"
+";n++)~a['!'+t]~&&c){~://')i+=~){v=s.n.~channel~100~.target~o.value~s_si(t)~')dc='1~\".tl(\")~etscape~s_')t=t~sr'+'c=~omePage~+=(~)){~i);~&&t~[b](e);~\"){n[k]~';s.va_~a+1,b):~return~mobile~events~ra"
+"ndom~code~=s_~,pev~'MSIE ~rs,~'fun~floor(~atch~transa~s.num(~m._e~s.c_gd~s.mr~,'lt~tm.g~.inner~;s.gl(~,f1,f2~=s.p_c~idt='+~',s.bc~page~Group,~.fromC~sByTag~++;~')<~||!~+';'~y+=~l&&~''+x~'')~[t]=~[i"
+"]=~[n];~' '+~'+v]~>=5)~+1))~!a[t])~~s._c=^hc';`G=`z`5!`G`U$6`G`Ul`K;`G`Un=0;}s^Ll=`G`Ul;s^Ln=`G`Un;s^Ll[s^L$7s;`G`Un#js.an#Lan;s.cls`0x,c){`Ni,y`h`5!c)c=^0.an;`li=0;i<x`8^5n=x`1i,i+1)`5c`4n)>=0)#nn"
+"}`3y`Cfl`0x,l){`3x?@Xx)`10,l):x`Cco`0o`D!o)`3o;`Nn`A,x;`lx@to)$Dx`4'select#k0&&x`4'filter#k0)n[x]=o[x];`3n`Cnum`0x){x`h+x;`l`Np=0;p<x`8;p++)$D(@q')`4x`1p,p#x<0)`30;`31`Crep#Lrep;s.sp#Lsp;s.jn#Ljn;@"
+"h`0x`2,h=@qABCDEF',i,c=s.@P,n,l,e,y`h;c=c?c`F$r`5x){x`h+x`5c`SAUTO'^e#q.c^vAt){`li=0;i<x`8^5c=x`1i,i+$In=x.c^vAt(i)`5n>127){l=0;e`h;^2n||l<4){e=h`1n%16,n%16+1)+e;n=(n-n%16)/16;l++}#n'%u'+e}`6c`S+')"
+"#n'%2B';`a#n^qc)}x=y^Tx=x?`j^q#p),'+`H%2B'):x`5x&&c^8em==1&&x`4'%u#k0&&x`4'%U#k0){i=x`4'%^Q^2i>=0){i++`5h`18)`4x`1i,i+1)`F())>=0)`3x`10,i)+'u00'+x`1#Ai=x`4'%',i$l}`3x`Cepa`0x`2;`3x?un^q`j#p,'+`H ')"
+"):x`Cpt`0x,d,f,a`2,t=x,z=0,y,r;^2t){y=t`4d);y=y<0?t`8:y;t=t`10,y);@ct,a)`5r)`3r;z+=y+d`8;t=x`1z,x`8);t=z<x`8?t:''}`3''`Cisf`0t,a){`Nc=a`4':')`5c>=0)a=a`10,c)`5t`10,2)`S#5`12);`3(t!`h#B==a)`Cfsf`0t,"
+"a`2`5`ea,`H,'is@Zt))@i#8@i!`h?`H`Yt;`30`Cfs`0x,f`2;@i`h;`ex,`H,'fs@Zf);`3@i`Csi`0wd`2,c`h+s_gi,a=c`4\"{\"),b=c`i\"}\"),m;c#Lfe(a>0&&b>0?c`1#F0)`5wd&&wd.^C$uwd.s`Zout(#P`p s_sv(o,n,k){`Nv=o[k],i`5v`"
+"D`ystring\"||`ynumber\")n[k]=v;`aif (`yarray#D`K;`li=0;i<v`8;i++@1`aif (`yobject#D`A;`li@tv@1}}fun`p #1{`Nwd=`z,s,i,j,c,a,b;wd^zgi`7\"un@Opg@Oss\",^wc$p;wd.^t^ws.ou@9\");s=wd.s;s.sa(^w^7+'\"`I^6=wd"
+";`e^3,@O,\"vo1\",t`I@Q=^H=s.`Q`s=s.`Q^4=`G`o=\\'\\'`5t.m_#o@w)`li=0;i<@w`8^5n=@w[i]`5$6m=t#tc=t[^k]`5m$uc=\"\"+c`5c`4\"fun`p\")>=0){a=c`4\"{\");b=c`i\"}\");c=a>0&&b>0?c`1#F0;s[^k@u=c`5#U)s.^f(n)`5s"
+"[n])`lj=0;j<$S`8;j++)s_sv(m,s[n],$S[j]$l}}`Ne,o,t@Bo=`z.opener`5o$J^zgi@ao^zgi(^w^7$p`5t)#1}`g}',1)}`Cc_d`h;#Vf`0t,a`2`5!#Tt))`31;`30`Cc_gd`0`2,d=`G`M^F^x,n=s.fpC`L,p`5!n)n=s.c`L`5d@8$T$9n?^Kn):2;n"
+"=n>2?n:2;p=d`i'.')`5p>=0){^2p>=0&&n>1@bd`i'.',p-$In--}$T=p>0&&`ed,'.`Hc_gd@Z0)?d`1p):d}}`3$T`Cc_r`0k`2;k=@h(k);`Nc=#us.d.^1,i=c`4#uk+$5,e=i<0?i:c`4';',i),v=i<0?'':@vc`1i+2+k`8,e<0?c`8:e));`3v$m[[B]"
+"]'?v:''`Cc_w`0k,v,e`2,d=#V(),l=s.^1@J,t;v`h+v;l=l?@Xl)`F$r`5@7@o@a(v!`h?^Kl?l:0):-60)`5t){e`n;e.s`Z(e.g`Z()+(t*$y0))}`kk@o@3d.^1=k+'`cv!`h?v:'[[B]]')+'; path=/;@Y@7?' expires='+e.toGMT^E()#m`Y(d?' "
+"domain='+d#m:'^Q`3^Uk)==v}`30`Ceh`0o,e,r,f`2,b=^h'+e+$As^Ln,n=-1,l,i,x`5!^il)^il`K;l=^il;`li=0;i<l`8&&n<0;i++`Dl[i].o==o&&l[i].e==e)n=i`kn<0$9i;l[n]`A}x=l#tx.o=o;x.e=e;f=r?x.b:f`5r||f){x.b=r?0:o[e]"
+";x.o[e]=f`kx.b){x.o[b]=x.b;`3b}`30`Ccet`0f,a,t,o,b`2,r,^n`5`T>=5^e!s.^o||`T>=7#9^n`7's`Hf`Ha`Ht`H`Ne,r@B@ca)`gr=s[t](e)}`3r^Qr=^n(s,f,a,t)^T$Ds.^p^8u`4#N4@20)r=s[b](a);else{^i(`G,'@R',0,o);@ca`Ieh("
+"`G,'@R',1)}}`3r`Cg^6et`0e`2;`3s.^6`Cg^6oe`7'e`H`Bc;^i(`z,\"@R\",1`Ie^6=1;c=s.t()`5c^cc`Ie^6=0;`3@r'`Ig^6fb`0a){`3`z`Cg^6f`0w`2,p=w^B,l=w`M;s.^6=w`5p&&p`M!=#op`M^F==l^F@3^6=p;`3s.g^6f(s.^6)}`3s.^6`C"
+"g^6`0`2`5!s.^6@3^6=`G`5!s.e^6)s.^6=s.cet('g^6@Zs.^6,'g^6et',s.g^6oe,'g^6fb')}`3s.^6`Cmrq`0u`2,l=@F],n,r;@F]=0`5l)`ln=0;n<l`8$s{r=l#t#W(0,0,r.r,0,r.t,r.u)}`Cbr`0id,rs`2`5s.@U`V#l^V^hbr',rs))$U=rs`Cf"
+"lush`V`0){^0.fbr(0)`Cfbr`0id`2,br=^U^hbr')`5!br)br=$U`5br`D!s.@U`V)^V^hbr`H'`Imr(0,0,br)}$U=0`Cmr`0$L,q,#Oid,ta,u`2,dc=s.dc,t1=s.`O,t2=s.`O^l,tb=s.`OBase,p='.sc',ns=s.`b`s$a,un=s.cls(u?u:(ns?ns:s.f"
+"un)),r`A,l,imn=^hi_@Yun),im,b,e`5!rs`Dt1`Dt2^8ssl)t1=t2^T$D!tb)tb='$e`5dc)dc=@Xdc)`9;`adc='d1'`5tb`S$e`Ddc`Sd1#212';`6dc`Sd2#222';p`h}t1=u@9.'+dc+'.'+p+tb}rs='@S@Y@El?'s'`Y'://'+t1+'/b/ss/'+^7+'/@Y"
+"s.#H?'5.1':'1'$oH.20.3/'+$L+'?AQB=1&ndh=1@Yq?q`Y'&AQE=1'`5^j@8s.^p`D`T>5.5)rs=^G#O4095);`ars=^G#O2047)`kid@3br(id,rs);#G}`k$0&&`T>=3^e!s.^o||`T>=7)^e@l<0||`T>=6.1)`D!s.rc)s.rc`A`5!^b){^b=1`5!s.rl)s"
+".rl`A;@Fn]`K;s`Zout('$D`z`Ul)`z`Ul['+s^L@9].mrq(^wu@9\")',750)^Tl=@Fn]`5l){r.t=ta;r.u=un;r.r=rs;l[l`8]=r;`3''}imn+=$A^b;^b++}im=`G[imn]`5!im)im=`G[im@Lm^zl=0;im.o^P`7'e`H^0^zl=1;`Nwd=`z,s`5wd`Ul){s"
+"=wd`Ul['+s^L@9];#Wq(^wu@9\"`Inrs--`5!$V)`Rm(\"rr\")}')`5!$V@3nrs=1;`Rm('rs')}`a$V#jim@C=rs`5rs`4'&pe=@20^e!ta||ta`S_self$na`S_top'||(`G.^x#Ba==`G.^x)#9b=e`n;^2!im^z#oe.g`Z()-b.g`Z()<500)e`n}`3''}`3"
+"$H#6^wrs+'\" w@0=1 @s@z0$f'`Cgg`0v`2`5!`G[^h#v)`G[^h#v`h;`3`G[^h#v`Cglf`0t,a`Dt`10,2)`S#5`12);`Ns=^0,v=s.gg(t)`5v)s#rv`Cgl`0v`2`5s.pg)`ev,`H,'gl@Z0)`Crf`0x`2,y,i,j,h,l,a,b`h,c`h,t`5x){y`h+x;i=y`4'?"
+"')`5i>0){a=y`1i+$Iy=y`10,#Ah=y`9;i=0`5h`10,7)`S@S$v7;`6h`10,8)`S@Ss$v8;h=h`1#Ai=h`4\"/\")`5i>0){h=h`10,i)`5h`4'google@20){a@Ia,'&')`5a`8>1){l=',q,ie,start,search_key,word,kw,cd,';`lj=0;j<a`8;j++@aa"
+"[j];i=t`4$5`5i>0&&l`4`H+t`10,i)+`H)>=0)b#8b$4'`Yt;`ac#8c$4'`Yt`kb$u#n'?'+b+'&'+c`5#p!=y)x=y}}}}}}`3x`Chav`0`2,qs`h,fv=s.`Q@pVa#Ofe=s.`Q@p^ds,mn,i`5$R){mn=$R`10,1)`F()+$R`11)`5$W){fv=$W.^OVars;fe=$W"
+".^O^ds}}fv=fv?fv+`H+^W+`H+^W2:'';`li=0;i<@x`8^5`Nk=@x[i],v=s[k],b=k`10,4),x=k`14),n=^Kx),q=k`5v&&k$m`Q`s'&&k$m`Q^4'`D$R||s.@Q||^H`Dfv^e`H+fv+`H)`4`H+k+`H)<0)v`h`5k`S#I'&&fe)v=s.fs(v,fe)`kv`Dk`S^Z`J"
+"D';`6k`S`bID`Jvid';`6k`S^S^Yg';v=^Gv$C`6k`S`d^Yr';v=^Gs.rf(v)$C`6k`Svmk'||k`S`b@W`Jvmt';`6k`S`E^Yvmf'`5@El^8`E^l)v`h}`6k`S`E^l^Yvmf'`5!@El^8`E)v`h}`6k`S@P^Yce'`5v`F()`SAUTO')v='ISO8859-1';`6s.em==2"
+")v='UTF-8'}`6k`S`b`s$a`Jns';`6k`Sc`L`Jcdp';`6k`S^1@J`Jcl';`6k`S^y`Jvvp';`6k`S@T`Jcc';`6k`S$x`Jch';`6k`S#S`pID`Jxact';`6k`S$M`Jv0';`6k`S^g`Js';`6k`S^D`Jc';`6k`S`u^u`Jj';`6k`S`q`Jv';`6k`S^1@M`Jk';`6k"
+"`S^AW@0`Jbw';`6k`S^A^m`Jbh';`6k`S@m`p^4`Jct';`6k`S@A`Jhp';`6k`Sp^X`Jp';`6#Tx)`Db`Sprop`Jc$B`6b`SeVar`Jv$B`6b`Slist`Jl$B`6b`Shier^Yh$Bv=^Gv$C`kv)qs+='&'+q+'=@Yk`10,3)$mpev'?@h(v):v$l`3qs`Cltdf`0t,h@"
+"at?t`9$K`9:'';`Nqi=h`4'?^Qh=qi>=0?h`10,qi):h`5t&&h`1h`8-(t`8#x`S.'+t)`31;`30`Cltef`0t,h@at?t`9$K`9:''`5t&&h`4t)>=0)`31;`30`Clt`0h`2,lft=s.`QDow^PFile^4s,lef=s.`QEx`t,$N=s.`QIn`t;$N=$N?$N:`G`M^F^x;h"
+"=h`9`5s.^ODow^PLinks&&lft&&`elft,`H#Xd@Zh))`3'd'`5s.^O@N&&h`10,1)$m# '^elef||$N)^e!lef||`elef,`H#Xe@Zh))^e!$N#l`e$N,`H#Xe@Zh)))`3'e';`3''`Clc`7'e`H`Bb=^i(^0,\"`r\"`I@Q=$P^0`It(`I@Q=0`5b)`3^0#C`3@r'"
+"`Ibc`7'e`H`Bf,^n`5s.d^8d.all^8d.all.cppXYctnr)#G;^H=e@C`W?e@C`W:e$z;^n`7\"s@O`Ne@B$D^H^e^H.tag`s||^H^B`W||^H^BNode))s.t()`g}\");^n(s`Ieo=0'`Ioh`0o`2,l=`G`M,h=o^r?o^r:'',i,j,k,p;i=h`4':^Qj=h`4'?^Qk="
+"h`4'/')`5h^ei<0||(j>=0&&i>j)||(k>=0&&i>k))@bo`m$J`m`8>1?o`m:(l`m?l`m:'^Qi=l.path^x`i'/^Qh=(p?p+'//'`Y(o^F?o^F:(l^F?l^F:#q)+(h`10,1)$m/'?l.path^x`10,i<0?0:i$o'`Yh}`3h`Cot`0o){`Nt=o.tag`s;t=t#B`F?t`F"
+"$r`5t`SSHAPE')t`h`5t`Dt`SINPUT'&&@G&&@G`F)t=@G`F();`6!t$J^r)t='A';}`3t`Coid`0o`2,^N,p,c,n`h,x=0`5t@8^9@bo`m;c=o.`r`5o^r^et`SA$n`SAREA')^e!c#lp||p`9`4'`u#k0))n$G`6c$9`j@g(`j@g@Xc,\"\\r\",''$8n\",''$"
+"8t\",#q,' `H^Qx=2}`6#0^et`SINPUT$n`SSUBMIT')$9#0;x=3}`6o@C#B`SIMAGE')n=o@C`5$6^9=^Gn@D;^9t=x}}`3^9`Crqf`0t,un`2,e=t`4$5,u=e>=0?`H+t`10,e)+`H:'';`3u&&u`4`H+un+`H)>=0?@vt`1e#x:''`Crq`0un`2,c=un`4`H),"
+"v=^U^hsq'),q`h`5c<0)`3`ev,'&`Hrq@Zun);`3`eun,`H,'rq',0)`Csqp`0t,a`2,e=t`4$5,q=e<0?'':@vt`1e+1)`Isqq[q]`h`5e>=0)`et`10,e),`H$2`30`Csqs`0un,q`2;^Ju[u$7q;`30`Csq`0q`2,k=^hsq',v=^Uk),x,c=0;^Jq`A;^Ju`A;"
+"^Jq[q]`h;`ev,'&`Hsqp',0`Ipt(^7,`H$2v`h;`lx@t^Ju`X)^Jq[^Ju[x]]#8^Jq[^Ju[x]]?`H`Yx;`lx@t^Jq`X^8sqq[x]^ex==q||c<2#9v#8v$4'`Y^Jq[x]+'`cx);c++}`3^Vk,v,0)`Cwdl`7'e`H`Br=@r,b=^i(`G,\"o^P\"),i,o,oc`5b)r=^0"
+"#C`li=0;i<s.d.`Qs`8^5o=s.d.`Qs[i];oc=o.`r?\"\"+o.`r:\"\"`5(oc`4$b<0||oc`4\"^zoc(\")>=0)$Jc`4#3<0)^i(o,\"`r\",0,s.lc);}`3r^Q`Gs`0`2`5`T>3^e!^j#ls.^p||`T#w`Ds.b^8$d^d)s.$d^d('`r#e);`6s.b^8b.add^d$g)s"
+".b.add^d$g('click#e,false);`a^i(`G,'o^P',0,`Gl)}`Cvs`0x`2,v=s.`b^a,g=s.`b^a#gk=^hvsn_'+^7+(g?$Ag:#q,n=^Uk),e`n,y=e@V$i);e$h$iy+10+(y<$O?$O:0))`5v){v*=$y`5!n`D!^Vk,x,e))`30;n=x`kn%$y00>v)`30}`31`Cdy"
+"asmf`0t,m`Dt&&m&&m`4t)>=0)`31;`30`Cdyasf`0t,m`2,i=t?t`4$5:-1,n,x`5i>=0&&m){`Nn=t`10,i),x=t`1i+1)`5`ex,`H,'dyasm@Zm))`3n}`30`Cuns`0`2,x=s.`PSele`p,l=s.`PList,m=s.`PM#R,n,i;^7=^7`9`5x&&l`D!m)m=`G`M^F"
+"`5!m.toLowerCase)m`h+m;l=l`9;m=m`9;n=`el,';`Hdyas@Zm)`5n)^7=n}i=^7`4`H`Ifun=i<0?^7:^7`10,i)`Csa`0un`2;^7=un`5!@j)@j=un;`6(`H+@j+`H)`4`H+un+`H)<0)@j+=`H+un;^7s()`Cp_e`0i,c`2,p`5!^M)^M`A`5!^M[i]@b^M["
+"i]`A;p^Ll=`G`Ul;p^Ln=`G`Un;p^Ll[p^L$7p;`G`Un#jp.i=i;p.s=s;p.si=s.p_si;p.sh=s.p_sh;p.cr#cr;p.cw#cw}p=^M[i]`5!p.e@8c){p.e=1`5!@k)@k`h;@k#8@k?`H`Yi}`3p`Cp`0i,l`2,p=s.p_e(i,1),n`5l)`ln=0;n<l`8$sp[l[n]."
+"$7l[n].f`Cp_m`0n,a,c`2,m`A;m.n=n`5!c){c=a;a='\"p@Os@Oo@Oe\"'}`aa='^w`ja,@O,\"\\\",\\\"\")+'\"';eval('m.f`7'+a+',^w`j@g(`j@g(c,\"\\\\\",\"\\\\\\\\\"$8\"@O\\\\\\\"\"$8r@O\\\\r\"$8n@O\\\\n\")$p^Q`3m`C"
+"p_si`0u){`Np=^0,s=p.s,n,i;n=^hp_i_'+p.i`5!p.u@8@E^c$H^x=^w@9\" @Yu?'#6^wu+'\" '`Y'@s=1 w@0@z0$f^Q`6u^es.ios||@E#9i=`G[n]?`G[n]:$0[n]`5!i)i=`G[@L@C=u}p.u=1`Cp_sh`0h){`Np=^0,s=p.s`5!p.h&&h^ch);p.h=1`"
+"Cp_cr`0k){`3^0.^Uk)`Cp_cw`0k,v,e){`3^0.^Vk,v,e)`Cp_r`0`2,p,n`5^M)`ln@t^M@b^M[n]`5p&&p.e`Dp$hup@8p.c)p$hup(p,s)`5p.run)p.run(p,s)`5!p.c)p.c=0;p.c++}}`Cm_i`0n,a`2,m,f=n`10,1),r,l,i`5!`Rl)`Rl`A`5!`Rnl"
+")`Rnl`K;m=`Rl[n]`5!a&&m&&#U@8m^L)`Ra(n)`5!m){m`A,m._c=^hm';m^Ln=`G`Un;m^Ll=s^Ll;m^Ll[m^L$7m;`G`Un#jm.s=s;m._n=n;$S`K('_c`H_in`H_il`H_i`H_e`H_d`H_dl`Hs`Hn`H_r`H_g`H_g1`H_t`H_t1`H_x`H_x1`H_rs`H_rr`H_"
+"l'`Im_l[$7m;`Rnl[`Rnl`8]=n}`6m._r@8m._m){r=m._r;r._m=m;l=$S;`li=0;i<l`8;i++)$Dm[l[i]])r[l[i]]=m[l[i]];r^Ll[r^L$7r;m=`Rl[$7r`kf==f`F())s[$7m;`3m`Cm_a`7'n`Hg`He`H$D!g)g=^k;`Bc=s[g@u,m,x,f=0`5!c)c=`G["
+"\"s_\"+g@u`5c&&s_d)s[g]`7\"s\",s_ft(s_d(c)));x=s[g]`5!x)x=`G[\\'s_\\'+g]`5!x)x=`G[g];m=`Ri(n,1)`5x^e!m^L||g!=^k#9m^L=f=1`5(\"\"+x)`4\"fun`p\")>=0)x(s);`a`Rm(\"x\",n,x,e)}m=`Ri(n,1)`5@yl)@yl=@y=0;`v"
+"t();`3f'`Im_m`0t,n,d,e@a$At;`Ns=^0,i,x,m,f=$At,r=0,u`5`R#o`Rnl)`li=0;i<`Rnl`8^5x=`Rnl[i]`5!n||x==$6m=`Ri(x);u=m[t]`5u`D@Xu)`4#P`p@20`Dd&&e)@dd,e);`6d)@dd);`a@d)}`ku)r=1;u=m[t+1]`5u@8m[f]`D@Xu)`4#P`"
+"p@20`Dd&&e)@5d,e);`6d)@5d);`a@5)}}m[f]=1`5u)r=1}}`3r`Cm_ll`0`2,g=`Rdl,i,o`5g)`li=0;i<g`8^5o=g[i]`5o)s.^f(o.n,o.u,o.d,o.l,o.e,$Ig#s0}`C^f`0n,u,d,l,e,ln`2,m=0,i,g,o=0#b,c=s.h?s.h:s.b,b,^n`5$6i=n`4':'"
+")`5i>=0){g=n`1i+$In=n`10,i)}`ag=^k;m=`Ri(n)`k(l||(n@8`Ra(n,g)))&&u^8d&&c^8$j`W`Dd){@y=1;@yl=1`kln`D@El)u=`ju,'@S:`H@Ss:^Qi=^hs:'+s^L@9:'+@9:'+g;b='`Bo=s.d@V`WById(^wi$p`5s$J`D!o.#o`G.'+g+'){o.l=1`5"
+"o.@6o.#Ao.i=0;`Ra(^w@9\",^wg+'^w(e?',^we+'\"'`Y')}';f2=b+'o.c++`5!`f)`f=250`5!o.l$J.c<(`f*2)/$y)o.i=s`Zout(o.f2@D}';f1`7'e',b+'}^Q^n`7's`Hc`Hi`Hu`Hf1`Hf2`H`Ne,o=0@Bo=s.$j`W(\"script\")`5o){@G=\"tex"
+"t/`u\";@Yn?'o.id=i;o.defer=@r;o.o^P=o.onreadystatechange=f1;o.f2=f2;o.l=0;'`Y'o@C=u;c.appendChild(o);@Yn?'o.c=0;o.i=s`Zout(f2@D'`Y'}`go=0}`3o^Qo=^n(s,c,i,u#b)^To`A;o.n=@9:'+g;o.u=u;o.d=d;o.l=l;o.e="
+"e;g=`Rdl`5!g)g=`Rdl`K;i=0;^2i<g`8&&g[i])i#jg#so}}`6$6m=`Ri(n);#U=1}`3m`Cvo1`0t,a`Da[t]||$t)^0#ra[t]`Cvo2`0t,a`D#y{a#r^0[t]`5#y$t=1}`Cdlt`7'`Bd`n,i,vo,f=0`5`vl)`li=0;i<`vl`8^5vo=`vl[i]`5vo`D!`Rm(\"d"
+"\")||d.g`Z()-$c>=`f){`vl#s0;s.t($E}`af=1}`k`v@6`vi`Idli=0`5f`D!`vi)`vi=s`Zout(`vt,`f)}`a`vl=0'`Idl`0vo`2,d`n`5!$Evo`A;`e^3,`H$X2',$E;$c=d.g`Z()`5!`vl)`vl`K;`vl[`vl`8]=vo`5!`f)`f=250;`vt()`Ct`0vo,id"
+"`2,trk=1,tm`n,sed=Math&&@f#J?@f#Q@f#J()*$y00000000000):#Y`Z(),$L='s'+@f#Q#Y`Z()/10800000)%10+sed,y=tm@V$i),vt=tm@VDate($o^IMonth($o@Yy<$O?y+$O:y)+' ^IHour$q:^IMinute$q:^ISecond$q ^IDay()+#u#Y`Zzone"
+"O$Q(),^n,^6=s.g^6(),ta`h,q`h,qs`h,#K`h,vb`A#a^3`Iuns(`Im_ll()`5!s.td){`Ntl=^6`M,a,o,i,x`h,c`h,v`h,p`h,bw`h,bh`h,^R0',k=^V^hcc`H@r',0@4,hp`h,ct`h,pn=0,ps`5^E&&^E.prototype){^R1'`5j.m#R){^R2'`5tm$hUT"
+"CDate){^R3'`5^j^8^p&&`T#w^R4'`5pn.toPrecisio$6^R5';a`K`5a.forEach){^R6';i=0;o`A;^n`7'o`H`Ne,i=0@Bi=new Iterator(o)`g}`3i^Qi=^n(o)`5i&&i.next)^R7'}}}}`k`T>=4)x=^sw@0+'x'+^s@s`5s.isns||s.^o`D`T>=3$w`"
+"q(@4`5`T>=4){c=^spixelDepth;bw=`G#ZW@0;bh=`G#Z^m}}$Y=s.n.p^X}`6^j`D`T>=4$w`q(@4;c=^s^D`5`T#w{bw=s.d.^C`W.o$QW@0;bh=s.d.^C`W.o$Q^m`5!s.^p^8b){^n`7's`Htl`H`Ne,hp=0`wh#7\");hp=s.b.isH#7(tl)?\"Y\":\"N"
+"\"`g}`3hp^Qhp=^n(s,tl);^n`7's`H`Ne,ct=0`wclientCaps\");ct=s.b.@m`p^4`g}`3ct^Qct=^n(s$l`ar`h`k$Y)^2pn<$Y`8&&pn<30){ps=^G$Y[pn].^x@D#m`5p`4ps)<0)p+=ps;pn++}s.^g=x;s.^D=c;s.`u^u=j;s.`q=v;s.^1@M=k;s.^A"
+"W@0=bw;s.^A^m=bh;s.@m`p^4=ct;s.@A=hp;s.p^X=p;s.td=1`k$E{`e^3,`H$X2',vb`Ipt(^3,`H$X1',$E`ks.useP^X)s.doP^X(s);`Nl=`G`M,r=^6.^C.`d`5!s.^S)s.^S=l^r?l^r:l`5!s.`d@8s._1_`d@3`d=r;s._1_`d=1`k(vo&&$c)#l`Rm"
+"('d'#9`Rm('g')`5s.@Q||^H){`No=^H?^H:s.@Q`5!o)`3'';`Np=s.#f`s,w=1,^N,$1,x=^9t,h,l,i,oc`5^H$J==^H){^2o@8n#B$mBODY'){o=o^B`W?o^B`W:o^BNode`5!o)`3'';^N;$1;x=^9t}oc=o.`r?''+o.`r:''`5(oc`4$b>=0$Jc`4\"^zo"
+"c(\")<0)||oc`4#3>=0)`3''}ta=n?o$z:1;h$Gi=h`4'?^Qh=s.`Q$3^E||i<0?h:h`10,#Al=s.`Q`s;t=s.`Q^4?s.`Q^4`9:s.lt(h)`5t^eh||l))q+='&pe=@Q_@Yt`Sd$n`Se'?@h(t):'o')+(h$4pev1`ch)`Y(l$4pev2`cl):'^Q`atrk=0`5s.^O@"
+"n`D!p@bs.^S;w=0}^N;i=o.sourceIndex`5@K')$9@K^Qx=1;i=1`kp&&n#B)qs='&pid`c^Gp,255))+(w$4p#dw`Y'&oid`c^Gn@D)+(x$4o#dx`Y'&ot`ct)+(i$4oi='+i:#q}`k!trk@8qs)`3'';$F=s.vs(sed)`5trk`D$F)#K=#W($L,(vt$4t`cvt)"
+"`Ys.hav()+q+(qs?qs:s.rq(^7)),0,id,ta);qs`h;`Rm('t')`5s.p_r)s.p_r(`I`d`h}^J(qs);^T`v($E;`k$E`e^3,`H$X1',vb`I@Q=^H=s.`Q`s=s.`Q^4=`G`o`h`5s.pg)`G^z@Q=`G^zeo=`G^z`Q`s=`G^z`Q^4`h`5!id@8s.tc@3tc=1;s.flus"
+"h`V()}`3#K`Ctl`0o,t,n,vo`2;s.@Q=$Po`I`Q^4=t;s.`Q`s=n;s.t($E}`5pg){`G^zco`0o){`N^t\"_\",1,$I`3$Po)`Cwd^zgs`0u$6`N^tun,1,$I`3s.t()`Cwd^zdc`0u$6`N^tun,$I`3s.t()}}@El=(`G`M`m`9`4'@Ss@20`Id=^C;s.b=s.d.b"
+"ody`5s.d@V`W#i`s@3h=s.d@V`W#i`s('HEAD')`5s.h)s.h=s.h[0]}s.n=navigator;s.u=s.n.userAgent;@l=s.u`4'N#46/^Q`Napn$k`s,v$k^u,ie=v`4#N'),o=s.u`4'@e '),i`5v`4'@e@20||o>0)apn='@e';^j$Z`SMicrosoft Internet "
+"Explorer'`Iisns$Z`SN#4'`I^o$Z`S@e'`I^p=(s.u`4'Mac@20)`5o>0)`T`xs.u`1o+6));`6ie>0){`T=^Ki=v`1ie+5))`5`T>3)`T`xi)}`6@l>0)`T`xs.u`1@l+10));`a`T`xv`Iem=0`5^E#h^v){i=^q^E#h^v(256))`F(`Iem=(i`S%C4%80'?2:"
+"(i`S%U0$y'?1:0))}s.sa(un`Ivl_l='^Z,`bID,vmk,`b@W,`E,`E^l,ppu,@P,`b`s$a,c`L,^1@J,#f`s,^S,`d,@T#El@I^W,`H`Ivl_t=^W+',^y,$x,server,#f^4,#S`pID,purchaseID,$M,state,zip,#I,products,`Q`s,`Q^4';`l`Nn=1;n<"
+"51$s@H+=',prop'+@9,eVar'+@9,hier'+@9,list$B^W2=',tnt,pe#M1#M2#M3,^g,^D,`u^u,`q,^1@M,^AW@0,^A^m,@m`p^4,@A,p^X';@H+=^W2;@x@I@H,`H`Ivl_g=@H+',`O,`O^l,`OBase,fpC`L,@U`V,#H,`b^a,`b^a#g`PSele`p,`PList,`P"
+"M#R,^ODow^PLinks,^O@N,^O@n,`Q$3^E,`QDow^PFile^4s,`QEx`t,`QIn`t,`Q@pVa#O`Q@p^ds,`Q`ss,@Q,eo,_1_`d#Eg@I^3,`H`Ipg=pg#a^3)`5!ss)`Gs()",
w=window,l=w.s_c_il,n=navigator,u=n.userAgent,v=n.appVersion,e=v.indexOf('MSIE '),m=u.indexOf('Netscape6/'),a,i,s;if(un){un=un.toLowerCase();if(l)for(i=0;i<l.length;i++){s=l[i];if(!s._c||s._c=='s_c'){if(s.oun==un)return s;else if(s.fs&&s.sa&&s.fs(s.oun,un)){s.sa(un);return s}}}}w.s_an='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
w.s_sp=new Function("x","d","var a=new Array,i=0,j;if(x){if(x.split)a=x.split(d);else if(!d)for(i=0;i<x.length;i++)a[a.length]=x.substring(i,i+1);else while(i>=0){j=x.indexOf(d,i);a[a.length]=x.subst"
+"ring(i,j<0?x.length:j);i=j;if(i>=0)i+=d.length}}return a");
w.s_jn=new Function("a","d","var x='',i,j=a.length;if(a&&j>0){x=a[0];if(j>1){if(a.join)x=a.join(d);else for(i=1;i<j;i++)x+=d+a[i]}}return x");
w.s_rep=new Function("x","o","n","return s_jn(s_sp(x,o),n)");
w.s_d=new Function("x","var t='`^@$#',l=s_an,l2=new Object,x2,d,b=0,k,i=x.lastIndexOf('~~'),j,v,w;if(i>0){d=x.substring(0,i);x=x.substring(i+2);l=s_sp(l,'');for(i=0;i<62;i++)l2[l[i]]=i;t=s_sp(t,'');d"
+"=s_sp(d,'~');i=0;while(i<5){v=0;if(x.indexOf(t[i])>=0) {x2=s_sp(x,t[i]);for(j=1;j<x2.length;j++){k=x2[j].substring(0,1);w=t[i]+k;if(k!=' '){v=1;w=d[b+l2[k]]}x2[j]=w+x2[j].substring(1)}}if(v)x=s_jn("
+"x2,'');else{w=t[i]+' ';if(x.indexOf(w)>=0)x=s_rep(x,w,t[i]);i++;b+=62}}}return x");
w.s_fe=new Function("c","return s_rep(s_rep(s_rep(c,'\\\\','\\\\\\\\'),'\"','\\\\\"'),\"\\n\",\"\\\\n\")");
w.s_fa=new Function("f","var s=f.indexOf('(')+1,e=f.indexOf(')'),a='',c;while(s>=0&&s<e){c=f.substring(s,s+1);if(c==',')a+='\",\"';else if((\"\\n\\r\\t \").indexOf(c)<0)a+=c;s++}return a?'\"'+a+'\"':"
+"a");
w.s_ft=new Function("c","c+='';var s,e,o,a,d,q,f,h,x;s=c.indexOf('=function(');while(s>=0){s++;d=1;q='';x=0;f=c.substring(s);a=s_fa(f);e=o=c.indexOf('{',s);e++;while(d>0){h=c.substring(e,e+1);if(q){i"
+"f(h==q&&!x)q='';if(h=='\\\\')x=x?0:1;else x=0}else{if(h=='\"'||h==\"'\")q=h;if(h=='{')d++;if(h=='}')d--}if(d>0)e++}c=c.substring(0,s)+'new Function('+(a?a+',':'')+'\"'+s_fe(c.substring(o+1,e))+'\")"
+"'+c.substring(e+1);s=c.indexOf('=function(')}return c;");
c=s_d(c);if(e>0){a=parseInt(i=v.substring(e+5));if(a>3)a=parseFloat(i)}else if(m>0)a=parseFloat(u.substring(m+10));else a=parseFloat(v);if(a>=5&&v.indexOf('Opera')<0&&u.indexOf('Opera')<0){w.s_c=new Function("un","pg","ss","var s=this;"+c);return new s_c(un,pg,ss)}else s=new Function("un","pg","ss","var s=new Object;"+s_ft(c)+";return s");return s(un,pg,ss)}
