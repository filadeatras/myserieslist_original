-- phpMyAdmin SQL Dump
-- version 2.11.9.4
-- http://www.phpmyadmin.net
--
-- Servidor: mysql.myserieslist.com
-- Tiempo de generación: 30-05-2010 a las 08:52:56
-- Versión del servidor: 5.1.39
-- Versión de PHP: 5.2.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de datos: `myserieslist_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `body` varchar(2000) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog_comments`
--

CREATE TABLE IF NOT EXISTS `blog_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_articulo` int(11) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_articulo` (`id_articulo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas`
--

CREATE TABLE IF NOT EXISTS `idiomas` (
  `id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(10) NOT NULL DEFAULT '',
  `estado` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE IF NOT EXISTS `paises` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `id_idioma` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `nombre` varchar(150) NOT NULL DEFAULT '',
  `x` float(13,10) NOT NULL DEFAULT '0.0000000000',
  `y` float(13,10) NOT NULL DEFAULT '0.0000000000',
  PRIMARY KEY (`id`,`id_idioma`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=247 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises_localidades`
--

CREATE TABLE IF NOT EXISTS `paises_localidades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_region` smallint(6) unsigned NOT NULL DEFAULT '0',
  `id_pais` smallint(6) unsigned NOT NULL DEFAULT '0',
  `id_idioma` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `nombre` varchar(150) NOT NULL DEFAULT '',
  `x` float(13,10) NOT NULL DEFAULT '0.0000000000',
  `y` float(13,10) NOT NULL DEFAULT '0.0000000000',
  `exacto` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`id_region`,`id_pais`,`id_idioma`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=790236 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises_regiones`
--

CREATE TABLE IF NOT EXISTS `paises_regiones` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `id_pais` smallint(6) unsigned NOT NULL DEFAULT '0',
  `id_idioma` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `nombre` varchar(150) NOT NULL DEFAULT '',
  `x` float(13,10) NOT NULL DEFAULT '0.0000000000',
  `y` float(13,10) NOT NULL DEFAULT '0.0000000000',
  `exacto` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`id_pais`,`id_idioma`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2202 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `series`
--

CREATE TABLE IF NOT EXISTS `series` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `insert_userid` int(11) NOT NULL,
  `imdbid` int(7) unsigned zerofill NOT NULL,
  `duration` int(11) NOT NULL,
  `year` varchar(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `imdbid` (`imdbid`),
  KEY `insert_userid` (`insert_userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=70 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `series_comments`
--

CREATE TABLE IF NOT EXISTS `series_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_series` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IP` varchar(15) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_series` (`id_series`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `series_episodes`
--

CREATE TABLE IF NOT EXISTS `series_episodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_series` int(11) NOT NULL,
  `season` int(11) NOT NULL,
  `episode_num` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_series` (`id_series`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4664 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `series_episodes_comments`
--

CREATE TABLE IF NOT EXISTS `series_episodes_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_episode` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IP` varchar(15) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_episode` (`id_episode`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `series_episodes_stars`
--

CREATE TABLE IF NOT EXISTS `series_episodes_stars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_episode` int(11) NOT NULL,
  `num_stars` int(1) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_episode` (`id_episode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `series_stars`
--

CREATE TABLE IF NOT EXISTS `series_stars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_series` int(11) NOT NULL,
  `num_stars` int(1) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_series` (`id_series`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `series_titles`
--

CREATE TABLE IF NOT EXISTS `series_titles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_series` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_series` (`id_series`),
  KEY `id_idioma` (`id_idioma`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `series_watched`
--
CREATE TABLE IF NOT EXISTS `series_watched` (
`id_series` int(11)
,`name` varchar(100)
,`year` varchar(5)
,`duration` int(11)
,`id_usuario` int(11)
,`id_episode` int(11)
,`state` varchar(30)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `id_pais` smallint(6) DEFAULT NULL,
  `sex` varchar(1) DEFAULT NULL,
  `realname` varchar(30) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `postal_code` int(5) unsigned zerofill DEFAULT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IP` varchar(15) NOT NULL,
  `signup_date` date NOT NULL,
  `level` decimal(10,0) NOT NULL,
  `preferred_language` varchar(3) NOT NULL,
  `twitter_token` varchar(50) NOT NULL,
  `twitter_token_secret` varchar(50) NOT NULL,
  `twitter_userid` varchar(50) NOT NULL,
  `twitter_screen_name` varchar(50) NOT NULL,
  `asterisk_passwd` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `id_pais` (`id_pais`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `users_with_country`
--
CREATE TABLE IF NOT EXISTS `users_with_country` (
`username` varchar(16)
,`level` decimal(10,0)
,`email` varchar(50)
,`sex` varchar(1)
,`realname` varchar(30)
,`lastname` varchar(50)
,`nombre` varchar(150)
,`postal_code` int(5) unsigned zerofill
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `watched`
--

CREATE TABLE IF NOT EXISTS `watched` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_series` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_episode` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `state` varchar(30) DEFAULT 'watching',
  PRIMARY KEY (`id`),
  KEY `id_series` (`id_series`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_episode` (`id_episode`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `series_watched`
--
DROP TABLE IF EXISTS `series_watched`;

CREATE ALGORITHM=UNDEFINED DEFINER=`grap`@`208.113.128.0/255.255.128.0` SQL SECURITY DEFINER VIEW `myserieslist_db`.`series_watched` AS select `myserieslist_db`.`series`.`id` AS `id_series`,`myserieslist_db`.`series`.`name` AS `name`,`myserieslist_db`.`series`.`year` AS `year`,`myserieslist_db`.`series`.`duration` AS `duration`,`myserieslist_db`.`watched`.`id_usuario` AS `id_usuario`,`myserieslist_db`.`watched`.`id_episode` AS `id_episode`,`myserieslist_db`.`watched`.`state` AS `state` from (`myserieslist_db`.`series` left join `myserieslist_db`.`watched` on((`myserieslist_db`.`series`.`id` = `myserieslist_db`.`watched`.`id_series`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `users_with_country`
--
DROP TABLE IF EXISTS `users_with_country`;

CREATE ALGORITHM=UNDEFINED DEFINER=`grap`@`208.113.128.0/255.255.128.0` SQL SECURITY DEFINER VIEW `myserieslist_db`.`users_with_country` AS select `myserieslist_db`.`users`.`username` AS `username`,`myserieslist_db`.`users`.`level` AS `level`,`myserieslist_db`.`users`.`email` AS `email`,`myserieslist_db`.`users`.`sex` AS `sex`,`myserieslist_db`.`users`.`realname` AS `realname`,`myserieslist_db`.`users`.`lastname` AS `lastname`,`myserieslist_db`.`paises`.`nombre` AS `nombre`,`myserieslist_db`.`users`.`postal_code` AS `postal_code` from (`myserieslist_db`.`users` left join `myserieslist_db`.`paises` on(((`myserieslist_db`.`paises`.`id` = `myserieslist_db`.`users`.`id_pais`) and (`myserieslist_db`.`paises`.`id_idioma` = 3))));

--
-- Filtros para las tablas descargadas (dump)
--

--
-- Filtros para la tabla `blog`
--
ALTER TABLE `blog`
  ADD CONSTRAINT `blog_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `blog_comments`
--
ALTER TABLE `blog_comments`
  ADD CONSTRAINT `blog_comments_ibfk_3` FOREIGN KEY (`id_articulo`) REFERENCES `blog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blog_comments_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `blog_comments_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `series`
--
ALTER TABLE `series`
  ADD CONSTRAINT `series_ibfk_1` FOREIGN KEY (`insert_userid`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `series_comments`
--
ALTER TABLE `series_comments`
  ADD CONSTRAINT `series_comments_ibfk_4` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `series_comments_ibfk_3` FOREIGN KEY (`id_series`) REFERENCES `series` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `series_episodes`
--
ALTER TABLE `series_episodes`
  ADD CONSTRAINT `series_episodes_ibfk_1` FOREIGN KEY (`id_series`) REFERENCES `series` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `series_episodes_comments`
--
ALTER TABLE `series_episodes_comments`
  ADD CONSTRAINT `series_episodes_comments_ibfk_4` FOREIGN KEY (`id_episode`) REFERENCES `series_episodes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `series_episodes_comments_ibfk_3` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `series_episodes_stars`
--
ALTER TABLE `series_episodes_stars`
  ADD CONSTRAINT `series_episodes_stars_ibfk_4` FOREIGN KEY (`id_episode`) REFERENCES `series_episodes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `series_episodes_stars_ibfk_3` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `series_stars`
--
ALTER TABLE `series_stars`
  ADD CONSTRAINT `series_stars_ibfk_4` FOREIGN KEY (`id_series`) REFERENCES `series` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `series_stars_ibfk_3` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `series_titles`
--
ALTER TABLE `series_titles`
  ADD CONSTRAINT `series_titles_ibfk_1` FOREIGN KEY (`id_series`) REFERENCES `series` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `watched`
--
ALTER TABLE `watched`
  ADD CONSTRAINT `watched_ibfk_1` FOREIGN KEY (`id_series`) REFERENCES `series` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `watched_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `watched_ibfk_3` FOREIGN KEY (`id_episode`) REFERENCES `series_episodes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
