<?php
require_once('includes/basic.php');
$login = new Login;
if($login->getLevel() || (!$login->logged)) {
	$style = new style;
	
	$style->cap();
	
	if(isset($_POST['username'])){
		$username = $_POST['username'];
		$password = $_POST['password'];
		$login->doLogin($username, $password);
	}
	
	else if(urlParameters(1) == "test"){
		echo '<a href="http://'.$_SERVER['SERVER_NAME'].'/twitterlogin"><img src="http://'.$_SERVER['SERVER_NAME'].'/includes/oauth/twitter/images/lighter.png" alt="Sign in with Twitter"/></a>';
	}
	
	//When twitter goes out
	else if(urlParameters(1) == "twitterlogin"){
		$login->twitterLogin();
	}
	
	else if(urlParameters(1) == "twitterupdate"){
		require_once('includes/series.class.php');
		$series = new Series;
		$series->tweetForm();
	}
	
	else if(urlParameters(1) == "examplesendtweet"){
		require_once('includes/series.class.php');
		$series = new Series;
		$series->tweetSeries('This tweet is a test');
	}
	
	// When twitter comes back
	else if(urlParameters(1) == "twitter"){
		$login->twitterCallback();
	}
	
	else if(urlParameters(1) == "blog"){
		require_once('includes/blog.class.php');
		$blog = new Blog;
		$article = urlParameters(2);
		
		if (!empty($article) && ($blog->exists_post(urlParameters(2)))) {
			$blog->showArticle(urlParameters(2));
		}
		else $blog->showArticles();
		
		echo '</div>';
		
	}
	
	else if(urlParameters(1) == "report"){
		require_once('includes/series.class.php');
		$serie= new Series;
		
			if(!$login->logged()) {
				echo '<h2>Must be logged...</h2>';	
			}
			else {
				$serie->reportError();
			}
	}
	
	else if(urlParameters(1) == "signup"){
		require_once('includes/users.class.php');
		$user= new User;
		
			if(!$login->logged()) {
				$user->signup();	
			}
			else {
				echo '<script type="text/javascript">
					location.href="users/'.$login->getUsername().'";
				</script>';	
			}
	}

	else if(urlParameters(1) == "users"){
		require_once('includes/users.class.php');
		$login = new Login;
		$user= new User;
		$url = urlParameters(2);
		
		if(!empty($url)){
			if($url == $login->getUsername()){
				$user->myProfile();
				$user->changeUserPassword();
				$user->changeAsteriskPassword();
				$user->changeUserLanguage();
			}
			else $user->profile(urlParameters(2));
		}
		else $user->mapOfUsers();
	}
	
	else if(urlParameters(1) == "admin" ) {
		require_once('includes/admin.class.php');
		
		echo '<div id="accordion">';
		
		if($login->getLevel() >= 2){
			
			$admin = new Admin;
		
			if ($login->getLevel() >= 2){
			
				if(urlParameters(2)) {
					echo '<h2><a href="#">'.ADD_SERIES.'</a></h2><div>';
					$admin->addSeries(urlParameters(2));
					//if (isset())
					echo '</div>';	
				}
				
				else{
					echo '<h2><a href="#">'.ADD_SERIES.'</a></h2><div>';
					$admin->findUrl();
					//if (isset())
					echo '</div>';	
				}
				
				echo '<h2><a href="#">'.EDIT_SERIES.'</a></h2><div>';
				
				$id_serie = urlParameters(2);
				$admin->editSeries();
				echo '</div>';
			}
			
			
				
			if ($login->getLevel() > 2){
				echo '<h2><a href="#">'.EDIT_USERS.'</a></h2><div>';
				$admin->editUsers();
				echo '</div>';
				echo '<h2><a href="#">'.ADD_POST.'</a></h2><div>';
				$admin->blogPost();
				echo '</div>';
				echo '<h2><a href="#">'.EDIT_POSTS.'</a></h2><div>';
				$admin->BlogEntries();
				echo '</div>';
			}
		}	
		
		else echo "<br><h1>".NOT_ADMIN."</h1>";

		echo '</div>';
	}
	
	else if(urlParameters(1) == "series"){
		require_once('includes/series.class.php');
		$series = new Series;
		
		$name = urlParameters(2);
		$episode = urlParameters(3);
		
		
		if(!empty($episode)){
			if($series->exists_episode($episode)) $series->showEpisode(urlParameters(3));
			else {
				if(!$login->logged()){
					$series->showAllSeries();
					$series->topLiked();
				}
				else $series->showAllSeriesEditable();
			}
		}
		else if(!empty($name)){
			if($series->exists_serie($name)) {
				$series->showSeries(urlParameters(2));
			}
			else {
				if(!$login->logged()){
					$series->showAllSeries();
					$series->topLiked();
				}
				else $series->showAllSeriesEditable();
			}
		}
		else {
			if(!$login->logged()){
				$series->showAllSeries();
				$series->topLiked();
			}
			else $series->showAllSeriesEditable();
		}
	}
	
	else if (urlParameters(1) == ""){
		require_once('includes/series.class.php');
		$series = new Series;
		
		echo '<div id="marquee"><marquee scrollamount="2" behavior="alternate" direction="right" width="540">'.MARQUEE_MESSAGE.'</marquee></div>';
		$series->randomSeries();
		$series->top5();
		$series->latestNews();
		$series->latestViews();
		
		
	}
	
	else {
		require_once('includes/error404.class.php');
		$error = new error404;
		$error->showError();
	}
	
	$style->peu();
}
?>