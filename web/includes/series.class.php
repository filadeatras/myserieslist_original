<?php
class Series {
    
    //- Series ( Peque�o cuadro de informacion, ultimos seguidores, reviews con avg, recomendaciones "tambien te podria gustar ...", visitas, twitter)
        //Organizadas por Tags, Titulo, Year, Puntuacion

    public function tweetSeries($msg){
        $tweetlogin =  new Login;
        
        $access_token = $tweetlogin->twitterConnect();
        
        if(!empty($access_token['oauth_token'])){
        /* Create a TwitterOauth object with consumer/user tokens. */
        $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

        /* Get logged in user to help with tests. */
        $user = $connection->get('account/verify_credentials');

        $show = $connection->post('statuses/update', array('status' => $msg));
        echo 'true';
        }
        else header('Location: ./twitterlogin');
    }
    
    public function tweetForm(){
		echo '<br><br><br><form id="tweetForm">';
                echo 'Your tweet: <input type="text" id="tweet"><br>';
		echo '<input type="submit"></form>
                        <span id="count1"></span> Remaining
                
                <script type="text/javascript" src="http://'.$_SERVER['SERVER_NAME'].'/js/jquery.NobleCount.min.js"> </script>
                <script language="javascript">
                $(\'#tweet\').NobleCount(\'#count1\', {max_chars:140});
                $(\'#tweetForm\').submit(function() {
                        $.ajax({
                                type: \'POST\',
                                url: \'http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php\',
                                data: { tweet : $(\'#tweet\').val() },
                                // Mostramos un mensaje con la respuesta de PHP
                                success: function(data) {
                                        if(data=="true"){
                                                $("#tweetForm").append("Success! ");
                                        }else {
                                                $("#tweetForm").append("Error! ");
                                        }
                                }
                        })        
                        return false;
                 });
                
                
                </script>
                ';
    }
        
    public function lastReviews(){
        require_once('includes/basic.php');
        $bd = new Database;
        $bd->conectar();
        
        $bd->select('series_comments.*,users.username','series_comments,users','
                    series_comments.id_usuario=users.id order by date desc limit 0,10');
        
        $todo = $bd->dump();
        echo '<pre>';
        print_r($todo);
        echo '</pre>';
        $bd->desconectar();
    }
    
    public function topLiked(){
        require_once('includes/basic.php');
        $bd = new Database;
        $bd->conectar();
        
        $bd->select('avg(series_stars.num_stars) as med,series.name','series_stars, series','
                    series_stars.id_series=series.id
                    group by series.name
                    order by 1 desc limit 10');
        
        $todo = $bd->dump();
        
        echo '<div id="topLiked">
            <h2>Popular</h2>
            <table id="tableTop">
            <thead>
		<tr>
			<th></th>
                        <th></th>
		</tr>
                    </thead>
                    <tbody>';
                for($i=0;$i<count($todo);$i++){
                    echo '<tr><td>'.($i+1).'. <a href="http://'.$_SERVER['SERVER_NAME'].'/series/'.str_replace(" ", "-", $todo[$i]['name']).'">'.$todo[$i]['name'].'</a></td><td>'.$todo[$i]['med'].'</td></tr>';
                }
                echo '</tbody>
                </table>
            </div>';
                    
        $bd->desconectar();
    }

    public function CommentSeries($series_id){
        //Shoutbox for series
        require_once('includes/basic.php');
        $login = new Login;
           
        $bd = new Database;
        
        $bd->conectar();
        $bd->select('series_comments.*,users.username','series_comments, users','
                    series_comments.id_usuario=users.id and id_series='.$series_id.' order by date');
        $resultat = $bd->obj();
        $bd->desconectar();
        

        echo '<div id="serieReviews">
                <h1>'.REVIEWS.'</h1>';
            echo "<div id=\"comments\">";
	    echo "<ol class=\"commentList\" id=\"divComments\">";
            
                foreach ($resultat as $comment){
		    echo '<li id="comment'.$comment->id.'">';
		    echo '<div id="dataComment">
				<div id="imgComment"><img src="/images/noAvatar.png" /></div>
				<div id="commentAuthor"><a href="http://'.$_SERVER['SERVER_NAME'].'/users/'.$comment->username.'">'.$comment->username.'</a></div>
				<div id="commentDate"><i>'.$comment->date.'</i></div>
			    </div>
			<div id="textComment">'.$comment->comment.'</div>
			';
			
		    echo '</li>';
                }
		echo "</ol>";
		echo "</div>";
        
            if($login->logged()) $this->newCommentSerie($series_id);
            else echo 'If you want to comment, you must register';
    }
    
    public function newCommentSerie($series_id){
        $login = new Login;
      
        echo '<br>'.$login->getUsername().' '.SAYS.' <textarea id="commentBox"></textarea>
        <input type="submit" onclick="sendComment()">
        
        <script type="text/javascript">
        
            $( \'#commentBox\' ).ckeditor({
              toolbar : \'Basic\'
            });
            
            function sendComment(){
                var i = Math.ceil(10000*Math.random());
                var text = $( \'#commentBox\' ).val();
                $("#divComments").append("<div id=\"spinner"+i+"\"><li><img src=\"/images/spinner.gif\" alt=\"Wait\" /></li></div>");
                $.post(
                    "http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php",
                    { review: text , idseries : '.$series_id.' },
                    function(data){
                        if ((data != "exceded") && (data != "error")) {
                            $("#spinner"+i).html(" ");
                            $("#divComments").append("<li>"+data+"</li><br>");
                        }
                        else if (data == "exceded")  {
                            $("#spinner"+i).html(" ");
                            alert("Wait 1 minute to comment one more time");
                        }
                        else if (data == "error")  {
                            $("#spinner"+i).html(" ");
                            alert("Error posting de message, try again later.");
                        }
                        
                });
            }
        </script>';
    }
    
    public function CommentSeriesEpisodes(){
        //Shoutbox for episodes
        require_once('includes/basic.php');
        $login = new Login;
           
        $bd = new Database;
        
        $bd->conectar();
        $bd->select('series_episodes_comments.*,users.username','series_episodes_comments,users','
                    series_episodes_comments.id_usuario=users.id and id_episode='.urlParameters(3).' order by date');
        $resultat = $bd->obj();
        $bd->desconectar();
        

        echo '<div id="serieEpisodeReviews">
                <h1>'.EPISODE_REVIEWS.'</h1>';
            echo "<div id=\"comments\">";
	    echo "<ol class=\"commentList\" id=\"divComments\">";
            
            foreach ($resultat as $comment){
                echo '<li id="comment'.$comment->id.'">';
                echo '<div id="dataComment">
                            <div id="imgComment"><img src="/images/noAvatar.png" /></div>
                            <div id="commentAuthor"><a href="http://'.$_SERVER['SERVER_NAME'].'/users/'.$comment->username.'">'.$comment->username.'</a></div>
                            <div id="commentDate"><i>'.$comment->date.'</i></div>
                        </div>
                    <div id="textComment">'.$comment->comment.'</div>';
                    
                echo '</li>';
            }
            
            echo "</ol>";
            echo "</div>";
        
            if($login->logged()) $this->newCommentSerieEpisode();
            else echo 'If you want to comment, you must register';
    }
    
    public function newCommentSerieEpisode(){
        require_once('includes/basic.php');
        
        $login = new Login;
      
        echo '<br>'.$login->getUsername().' '.SAYS.' <textarea id="commentBox"></textarea>
        <input type="submit" onclick="sendComment()">
        
        <script type="text/javascript">
        
            $( \'#commentBox\' ).ckeditor({
              toolbar : \'Basic\'
            });
            
            function sendComment(){
                var i = Math.ceil(10000*Math.random());
                var text = $( \'#commentBox\' ).val();
                $("#divComments").append("<div id=\"spinner"+i+"\"><li><img src=\"/images/spinner.gif\" alt=\"Wait\" /></li></div>");
                $.post(
                    "http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php",
                    { shoutboxEpisode: text , idepisode : '.urlParameters(3).' },
                    function(data){
                        if ((data != "exceded") && (data != "error")) {
                            $("#spinner"+i).html(" ");
                            $("#divComments").append("<li>"+data+"</li><br>");
                        }
                        else if (data == "exceded")  {
                            $("#spinner"+i).html(" ");
                            alert("Wait 1 minute to comment one more time");
                        }
                        else if (data == "error")  {
                            $("#spinner"+i).html(" ");
                            alert("Error posting de message, try again later.");
                        }
                        
                });
            }
            
        </script>';
    }
    
    public function seriesStars($id_series){
        //calculate stars
        $login = new Login;
        $userid =  $login->getUserID();

        $bd = new Database;
        $bd->conectar();
        
        //antes de nada miramos si el usuario ha metido alguna votacion para la serie
        
        $bd->select("num_stars", "series_stars", "id_series = '$id_series' and id_usuario ='$userid'");
	$stars = $bd->dump();
        
        $numStars = 0;
        $numStars += $stars[0]['num_stars'];
        
        $string = " checked=\"checked\" ";
        
        $bd->select('num_stars','series_stars','id_series='.$id_series);
        $todo = $bd->dump();
        
        $suma = 0;
        for($i=0;$i<count($todo);$i++){
            $suma = $todo[$i]['num_stars'] + $suma;
        }
        
        @$avgVotes = $suma / count($todo);
        
        
        // Y si ya he votado?
        //$bd->select('num_stars','series_stars','id_usuario=\''.$login->getUserID().'\' and id_series='.$id_series);
        //if(!$bd->num_rows()){
        echo '<div id="puntuacion">
                <form action="http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php" method="post">
                    '.RATING.': <span id="stars-cap"></span>
                     <div id="stars-wrapper1">
                        <input type="radio" name="newrate" value="1" title="'.VERY_POOR.'"'; if($numStars == 1) echo $string; echo'./>
                        <input type="radio" name="newrate" value="2" title="'.POOR.'"'; if($numStars == 2) echo $string; echo'./>
                        <input type="radio" name="newrate" value="3" title="'.NOT_BAD.'"'; if($numStars == 3) echo $string; echo'./>
                        <input type="radio" name="newrate" value="4" title="'.FAIR.'"'; if($numStars == 4) echo $string; echo'./>
                        <input type="radio" name="newrate" value="5" title="'.AVERAGE.'"'; if($numStars == 5) echo $string; echo'./>
                        <input type="radio" name="newrate" value="6" title="'.ALMOST_GOOD.'"'; if($numStars == 6) echo $string; echo'./>
                        <input type="radio" name="newrate" value="7" title="'.GOOD.'"'; if($numStars == 7) echo $string; echo'./>
                        <input type="radio" name="newrate" value="8" title="'.VERY_GOOD.'"'; if($numStars == 8) echo $string; echo'./>
                        <input type="radio" name="newrate" value="9" title="'.EXCELLENT.'"'; if($numStars == 9) echo $string; echo'./>
                        <input type="radio" name="newrate" value="10" title="'.PERFECT.'"'; if($numStars == 10) echo $string; echo'./>
                    </div>
                </form>
                <div id="ajax_response">General: '.number_format($avgVotes,2).'/10<br></div>
            </div>';
            
        $bd->desconectar();
               
        //Recuerda que si IP esta repetida, no vale el voto
    }
    
    public function seriesEpisodesStars($id_episode){
        
        //calculate stars
        $login = new Login;
        $userid =  $login->getUserID();

        $bd = new Database;
        $bd->conectar();
        
        //antes de nada miramos si el usuario ha metido alguna votacion para el capitulo
        $bd->select("num_stars", "series_episodes_stars", "id_episode = '$id_episode' and id_usuario ='$userid'");
	$stars = $bd->dump();
        
        $numStars = 0;
        $numStars += $stars[0]['num_stars'];
        
        $string = " checked=\"checked\" ";
        
        $bd->select('num_stars','series_episodes_stars','id_episode='.$id_episode);
        $todo = $bd->dump();
        
        $suma = 0;
        for($i=0;$i<count($todo);$i++){
            $suma = $todo[$i]['num_stars'] + $suma;
        }
        
        @$avgVotes = $suma / count($todo);
        
        // Y si ya he votado?
        //$bd->select('num_stars','series_stars','id_usuario=\''.$login->getUserID().'\' and id_series='.$id_series);
        //if(!$bd->num_rows()){
        echo '<div id="puntuacion">
                <form action="http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php" method="post">
                    '.RATING.': <span id="stars-cap"></span>
                     <div id="stars-episodes-wrapper1">
                        <input type="radio" name="newrate" value="1" title="'.VERY_POOR.'"'; if($numStars == 1) echo $string; echo'./>
                        <input type="radio" name="newrate" value="2" title="'.POOR.'"'; if($numStars == 2) echo $string; echo'./>
                        <input type="radio" name="newrate" value="3" title="'.NOT_BAD.'"'; if($numStars == 3) echo $string; echo'./>
                        <input type="radio" name="newrate" value="4" title="'.FAIR.'"'; if($numStars == 4) echo $string; echo'./>
                        <input type="radio" name="newrate" value="5" title="'.AVERAGE.'"'; if($numStars == 5) echo $string; echo'./>
                        <input type="radio" name="newrate" value="6" title="'.ALMOST_GOOD.'"'; if($numStars == 6) echo $string; echo'./>
                        <input type="radio" name="newrate" value="7" title="'.GOOD.'"'; if($numStars == 7) echo $string; echo'./>
                        <input type="radio" name="newrate" value="8" title="'.VERY_GOOD.'"'; if($numStars == 8) echo $string; echo'./>
                        <input type="radio" name="newrate" value="9" title="'.EXCELLENT.'"'; if($numStars == 9) echo $string; echo'./>
                        <input type="radio" name="newrate" value="10" title="'.PERFECT.'"'; if($numStars == 10) echo $string; echo'./>
                    </div>
                </form>
                <div id="ajax_response">General: '.number_format($avgVotes,2).'/10<br></div>
            </div>';
            
        $bd->desconectar();
    }
    
    public function showEpisode($episode_id){
        require_once('includes/basic.php');
        require_once('includes/imdbphp/imdb.class.php');
        
        $bd = new Database;
        $bd->conectar();
        
        $bd->select('series_episodes.*,series_episodes.name as episode_name, series.imdbid,series.name','series, series_episodes','
                    series.id=series_episodes.id_series and series_episodes.id='.$episode_id);
        
        $todo = $bd->dump();
        $this->seriesEpisodesStars($todo[0]['id']);
       
       
        $series = new imdb($todo[0]['imdbid']);
        $series->setid($todo[0]['imdbid']);
        $episodes = $series->episodes();
       
        echo '<div id="episodeComments"><h2>'.$todo[0]['name'].'</h2>';
        echo '<h3>'.SEASON.' '.$todo[0]['season'].' '.EPISODE.' '.$todo[0]['episode_num'].': '.$todo[0]['episode_name'].'</h3>
            <small>'.AIRED.': '.$episodes[$todo[0]['season']][$todo[0]['episode_num']]['airdate'].'</small><br>';
       
        
        echo $episodes[$todo[0]['season']][$todo[0]['episode_num']]['plot'];
        echo '<h4>'.COMMENT.'</h4>';
        $bd->desconectar();
        
        $this->CommentSeriesEpisodes();
        echo'</div>';
    }
    public function showSeries($name){
        require_once('includes/basic.php');
        require_once('includes/imdbphp/imdb.class.php');
        $login = new Login;
        
        //Check if the name had spaces
        $rules = array("-"=>" ");
        $name = strtr($name, $rules);
        
        
        $bd = new Database;
        $bd->conectar();
        //Mostrar info de la serie
        $bd->select('imdbid,id','series','name=\''.$name.'\'');
        $imdbid = $bd->dump();
        
        $this->seriesStars($imdbid[0]['id']);
        
        $series = new imdb($imdbid[0]['imdbid']);
        $series->setid($imdbid[0]['imdbid']);
        
        $string = "";
        $string = $series->photo_localurl();
        if($string == "") $string.="/images/no-image.png";  
        echo '<div id="serieInfo">
        
            <table>
            <thead>
		<tr>
			<th></th>
                        <th></th>
		</tr>
            
            </thead><tbody>
                <tr><td><img src="'.$string.'"></td><td><h2>'.$series->title().'
                ';
                if($login->logged()) echo '<a href="#" onClick="wopen(\'http://'.$_SERVER['SERVER_NAME'].'/twitterlogin\', \'popup\', 450, 400); return false;">
                <img src="http://'.$_SERVER['SERVER_NAME'].'/images/social/Twitter-32.png" height="20" width="20" >
                </a>';
                echo '<a href="#" onClick="wopen(\'http://del.icio.us/post?title='.urlParameters(2).'&url=http://'.$_SERVER['SERVER_NAME'].'/series/'.urlParameters(2).'\', \'popup\', 800, 600); return false;">
                <img src="http://'.$_SERVER['SERVER_NAME'].'/images/social/Delicious-32.png" height="20" width="20" >
                </a>
                <a href="#" onClick="wopen(\'https://www.google.com/bookmarks/mark?op=add&hl=en&bkmk=http://'.$_SERVER['SERVER_NAME'].'/series/'.urlParameters(2).'&annotation=&labels=&title='.urlParameters(2).'/\', \'popup\', 800, 600); return false;">
                <img src="http://'.$_SERVER['SERVER_NAME'].'/images/social/Google-32.png" height="20" width="20">
                </a>
                <a href="#" onClick="wopen(\'http://'.$_SERVER['SERVER_NAME'].'/report\', \'popup\', 450, 400); return false;"><br><small>Report error </small></a>
                </h2></td></tr>
                <tr><td>'.RUNTIME.'</td><td>'.$series->runtime().'</td></tr>
                <tr><td>'.YEAR.'</td><td>'.$series->year().'</td></tr>
                <tr><td>'.DIRECTORS.'</td>
                    <td>';
                    $director = $series->director();
                    $all = count($director);
                    if ($all>3) $all=3;
                    for($i=0;$i<$all;$i++){ //Should limit to 15 or so
                        echo $director[$i]['name'].'<br>';
                    }
                    echo '</td>
                    </tr>
                    <tr><td>'.CAST.'</td>
                    <td> ';
                    $cast = $series->cast();
                    $all = count($cast);
                    if ($all>10) $all=10;
                    for($i=0;$i<$all;$i++){
                        echo $cast[$i]['name'].' as '.$cast[$i]['role'].'<br>';
                    }
                    echo'   
                    </td>
                </tr>
                <tr><td>'.PLOT.'</td><td>'.$series->plotoutline().'</td></tr>
                </tbody>
            </table>
        </div> ';


        $this->CommentSeries($imdbid[0]['id']);
        
        
        
        //Mostrar episodis
        $bd->select('series_episodes.*','series_episodes,series','
                    series.id=series_episodes.id_series
                    and series.name=\''.$name.'\'
                    order by season,episode_num');
        
        $todo = $bd->dump();
     
        echo '<div id="seriesEpis">';
        echo '<div id="table3"><table id="serieEpisodes"><thead>
		<tr>
			<th></th>
                </tr>
            </thead><tbody>';
        for($i=0;$i<count($todo);$i++){            
            echo '<tr><td><a href="http://'.$_SERVER['SERVER_NAME'].'/series/'.urlParameters(2).'/'.$todo[$i]['id'].'">S'.str_pad($todo[$i]['season'], 2, "0", STR_PAD_LEFT).'E'.str_pad($todo[$i]['episode_num'], 2, "0", STR_PAD_LEFT).': '.$todo[$i]['name'].'</a></td></tr>';
        }
        echo "</tbody></table></div></div>";
       
        
        $bd->desconectar();        
    }
    
    public function showAllSeriesEditable(){
        require_once('includes/imdbphp/imdb.class.php');

        $conn=new Database;
        $conn->conectar();
        $login = new Login;
  
        $conn->query(" SELECT series_watched.id_series as id, series_watched.name, series_watched.year, series_watched.duration,
                     max( if( series_watched.id_usuario=".$login->getUserID().", series_watched.id_usuario, NULL ) ) AS seguidor,
                     max(if( series_watched.id_usuario=".$login->getUserID().",series_watched.id_episode, NULL)) as episodi,
                     max( if( series_watched.id_usuario=".$login->getUserID().", series_watched.state, NULL ) ) as status,
                     max( if( series_watched.id_usuario=".$login->getUserID().", series_episodes.name, NULL ) ) as epi,
                     series_episodes.season, series_episodes.episode_num
                     FROM series_watched
                     LEFT JOIN series_episodes on series_episodes.id=series_watched.id_episode
                     GROUP BY series_watched.name");
        $matrix=$conn->dump();
        echo "<div id=\"table1\"><table id=\"myOtherTable2\">";
        echo '<thead>
		<tr>
			<th>'.NAME.'</th>
                        <th>'.YEAR.'</th>
			<th>'.RUNTIME.'</th>
                        <th>'.EPISODE.'</th>
                        <th>'.STATUS.'</th>
		</tr>

	</thead><tbody>';
        for($i=0;$i<count($matrix);$i++){
            $rules = array(" "=>"-");
            $name = strtr($matrix[$i]['name'], $rules);
            
            echo "<tr>
                    <td><a href=\"http://".$_SERVER['SERVER_NAME']."/series/$name\">".$matrix[$i]['name']."</a></td>
                    <td>".$matrix[$i]['year']."</td>
                    <td>".$matrix[$i]['duration']."</td>
                    <td id=".$matrix[$i]['id']." class=\"episode\">";
                    if(!empty($matrix[$i]['epi'])) echo $matrix[$i]['season']."x".$matrix[$i]['episode_num'].": ".$matrix[$i]['epi'];
                    echo "</td>
                    <td id=".$matrix[$i]['id']." class=\"status\">".$matrix[$i]['status']."</td>

                </tr>";
        }
        echo "</tbody></table></div>";
        
        $conn->desconectar();
    }
    
    public function showAllSeries(){
        
        require_once('includes/imdbphp/imdb.class.php');

        $conn=new Database;
        $conn->conectar();
  
        $conn->query("SELECT series.id, series.name, series.year, series.duration, watched.state, watched.id_episode FROM series LEFT JOIN watched ON watched.id_series = series.id group by series.id");
        $matrix=$conn->dump();
        echo "<div id=\"table1\"><table id=\"myOtherTable\">";
        echo '<thead>
		<tr>
			<th>'.NOMBRE.'</th>
                        <th>'.YEAR.'</th>
			<th>'.RUNTIME.'</th>
		</tr>

	</thead><tbody>';
        for($i=0;$i<count($matrix);$i++){
            $rules = array(" "=>"-");
            $name = strtr($matrix[$i]['name'], $rules);
            
            echo "<tr>
                    <td><a href=\"http://".$_SERVER['SERVER_NAME']."/series/$name\">".$matrix[$i]['name']."</a></td>
                    <td>".$matrix[$i]['year']."</td>
                    <td>".$matrix[$i]['duration']."</td>

                </tr>";
        }
        echo "</tbody></table></div>";
        
        $conn->desconectar();
    }
    
    public function reportError(){
        echo '<div id="reportError">
                <form id="reportEr">
                <br>
                    Topic<br> <input type="text" size="70" maxlength="70" name="topic" /><br>
                    Problem<br><textarea rows="4" cols="50" name="problem"></textarea>
                    <br><br><input type="submit"/>
                </form>';
    }
    
    public function randomSeries(){
        require('includes/imdbphp/imdb.class.php');
        $bd = new Database;
        $bd->conectar();
        
        $bd->select('series.imdbid, series.name, avg(series_stars.num_stars) as med','
                    series,series_stars','
                    series.id=series_stars.id_series group by series.name order by rand() limit 1');
        
        $todo = $bd->dump();
        
        $serie = new imdb($todo[0]['imdbid']);
        $serie->setid($todo[0]['imdbid']);
        
        $string = "";
        $string = $serie->photo_localurl();
        if($string == "") $string.="/images/no-imageP.png";  
        
        //Some cleaning and formatting
        echo '<div id="randomSeries">';
        echo '<table id="tableRandomSeries">
        <tr>
            <td rowspan=2><img src="'.$string.'"></td><td><h2><a href="http://'.$_SERVER['SERVER_NAME'].'/series/'.str_replace(" ", "-", str_replace("&#x22;", "", $serie->title())).'">'.$serie->title().'</a></h2></td>
        </tr>
        <tr>
            <td><div id="stars-wrapper-portada">';
            for($i=1;$i<11;$i++){
                if($i== ceil($todo[0]['med'])) echo '<input type="radio" name="newrate" value="'.$i.'" checked/>';
                   else echo '<input type="radio" name="newrate" value="'.$i.'"/>';
            }
            echo '</div></td>
        </tr>
        <tr>
            <td colspan="2">'.$serie->plotoutline().'</td>
        </tr>
        </table>
        ';
        echo '</div>';
        $bd->desconectar();
    }
    public function top5(){
        //Portada
        require_once('includes/basic.php');
        $bd = new Database;
        $bd->conectar();
        
        echo '<div id="topFive"><h2>'.TOP_5.'</h2>';
        
        $bd->select('avg(series_stars.num_stars) as med,series.name','series_stars, series','
                    series_stars.id_series=series.id
                    group by series.name
                    order by 1 desc limit 10');
        
        $todo = $bd->dump();
        
        echo '<table id="tableTop">
            <thead>
		<tr>
			<th>'.SERIES.'</th>
                        <th>'.AVG.'</th>
		</tr>
                    </thead>
                    <tbody>';
                for($i=0;$i<count($todo);$i++){
                    echo '<tr><td>'.($i+1).' <a href="http://'.$_SERVER['SERVER_NAME'].'/series/'.str_replace(" ", "-", $todo[$i]['name']).'">'.$todo[$i]['name'].'</a></td><td>'.$todo[$i]['med'].'</td></tr>';
                }
                echo '</tbody>
                </table>';
                    
        $bd->desconectar();
        
        
        
        
        echo '</div>';    
    }
    public function latestViews(){
        echo '<div id="latestViews">
        <h2>'.LAST_FOLLOWED.'</h2><marquee onmouseover="this.stop();" onmouseout="this.start();" behavior="scroll" direction="down" scrollamount="2" height="130" width="250">';
        
        $bd = new Database;
        $bd->conectar();
        
        $bd->select('series.name, users.username, watched.date','watched, series, users','            
                    series.id=watched.id_series and
                    users.id=watched.id_usuario
                    order by watched.date desc limit 5
                    ');
        $todo = $bd->dump();
        
        for($i=0;$i<count($todo);$i++){
            echo '<b>'.$todo[$i]['username'].'</b> '.UPDATED.' <a href="http://'.$_SERVER['SERVER_NAME'].'/series/'.str_replace(" ", "-", $todo[$i]['name']).'">'.$todo[$i]['name'].'</a><br> '.ON.' <i>'.$todo[$i]['date'].'</i><br><br>';
        }        
        
        $bd->desconectar();
        echo '</marquee></div>';
    }
    public function latestNews(){
        echo '<div id="latestNews">
        <h2>'.LAST_BLOG.'</h2><marquee onmouseover="this.stop();" onmouseout="this.start();" behavior="scroll" direction="down" scrollamount="2" height="130" width="250">';
        $bd = new Database;
        $bd->conectar();
        $bd->query("select id,title,body from blog limit 3");
        $todo = $bd->dump();
        
        for($i=0;$i<count($todo);$i++){
            echo '<b><a href="http://'.$_SERVER['SERVER_NAME'].'/blog/'.$todo[$i]['id'].'">'.$todo[$i]['title'].'</a></b><br>'.substr($todo[$i]['body'],0,100).'...<br><br>';
        }
        $bd->desconectar();
        
        echo '</marquee></div>';
    }

    public function exists_serie($string){
        
        $bd = new Database;
        $bd->conectar();
        
        $string = $bd->_clean($string);
        $string = str_replace("-", " ", $string);
        
        $bd->select('id', 'series', 'name =  "'.$string.'"');
        
        if ($bd->num_rows() > 0) {
            $bd->desconectar();
            return true;
        }
        else {
            $bd->desconectar();
            return false;
        }
    }
    
    public function exists_episode($id){
        
        $bd = new Database;
        $bd->conectar();
        
        $id = $bd->_clean($id);
        $bd->select('id', 'series_episodes', "id =  $id");
        
        if ($bd->num_rows() > 0) {
            return true;
        }
        else { 
            $bd->desconectar();
            return  false;
        }
    }    
}
?>