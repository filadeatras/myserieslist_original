<?php
class User{

    public function signup(){
        require_once('includes/recaptcha/recaptchalib.php');
        $login = new Login;
        echo '
            <div id="regForm">
                <form id="myform" method="POST" action="http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php">
                    <table width="400px">
                    <tr><td colspan="2"><h2>'.SIGN_UP.'</h2></td></tr>
                    <tr><td colspan="2"><h3>'.ACC_INFO.'</h3></td></tr>
                    <tr><td>'.USERNAME.':</td><td><input id="usernames" name="uname" size="16" type="text" onChange="checkUser();"/></label></td></tr>
                    <tr><td>'.PASSWORD.':</td><td><input id="passw1" name="pass1" type="password""/></td></tr>
                    <tr><td>'.REPEAT_PASS.':</td><td><input id="passw2" name="pass2" type="password"/></td></tr>
                    <tr><td>'.EMAIL.':</td><td><input size="35" id="email" name="email" type="text"/</td></tr>
                    <tr><td colspan="2"><h3>'.PERSONAL_INFO.'</h3></td></tr>
                    <tr><td>'.SEX.':</td><td>'.MALE.': <input name="sex" value="Male" type="radio" checked/>'.FEMALE.': <input name="sex" value="Female" type="radio"/></td></tr>
                    <tr><td>'.NAME.':</td><td><input name="realname" size="15" type="text" /></td></tr>
                    <tr><td>'.LASTNAME.':</td><td><input name="lastname" size="25" type="text"/></td></tr>
                    <tr><td>'.COUNTRY.':</td><td><input id="country" name="country" type="text" /></td></tr>
                    <tr><td>'.POSTAL_CODE.':</td><td><input id="postalCode" name="postalCode" size="15" maxlength="10" type="text" /></td></tr>
                    <tr><td>'.LANGUAGE.':</td>
                        <td>
                            <select id="language" name="language">
                                <option value="en">English</option>
                                <option value="es">Espa&ntilde;ol</option>
                            </select>
                        </td>
                    </tr>
                    <tr><td colspan="2"><h3>'.VERIFICATION.'</h3></td></tr>
                   <tr><td colspan="2">'.recaptcha_get_html(RECAP_PUBLIC).'</td></tr>
                    <tr><td colspan="2"><input type="submit" value="'.SEND.'"></td></tr>
                    </table>    
                </form>
                </div id="responseDiv"></div>
                 <script>
                    $(document).ready(function(){
                    
                            var options = { 
                                target:        \'#regForm\',   // target element(s) to be updated with server response 
                                beforeSubmit:  validate,  // pre-submit callback 
                                success:       showResponse  // post-submit callback 
                            }; 
                         
                            $(\'#myform\').submit(function() { 
                                $(this).ajaxSubmit(options); 
                         
                                // !!! Important !!! 
                                // always return false to prevent standard browser submit and page navigation 
                                return false; 
                            });
                            
                    });
                    
                    function checkUser(){
                        var uname = document.getElementById("usernames").value;
                        if (uname.length < 5){
                            $("#usernames").css("background-color", "red");
                            $("#usernames").css("border", "1px solid #7F9DB9");
                            $("#usernames").css("padding", "2px 1px 2px 1px");
                            validForm = false;
                        } else{
                            //comprovació usuari existeix
                            $.post(
                                "http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php",
                                { existUser : uname },
                                function(data){
                                    if(data=="true") {
                                        $("#usernames").css("background-color", "red");
                                        $("#usernames").css("border", "1px solid #7F9DB9");
                                        $("#usernames").css("padding", "2px 1px 2px 1px");
                                        validForm = false;
                                    } else{
                                        $("#usernames").css("background-color", "#66FF66");
                                        $("#usernames").css("border", "1px solid #7F9DB9");
                                        $("#usernames").css("padding", "2px 1px 2px 1px");
                                    }
                                }
                            )
                        }
                    }
                    
                    function validate(formData, jqForm, options) {
                        var validForm = true;
                        
                        var pass1 = document.getElementById("passw1").value;
                        var pass2 = document.getElementById("passw2").value;
                        var email = document.getElementById("email").value;
                        
                        //comprovacio de contrasenyes
                        if (pass1.length < 6){
                            $("#passw1").css("background-color", "red");
                            $("#passw1").css("border", "1px solid #7F9DB9");
                            $("#passw1").css("padding", "2px 1px 2px 1px");
                            $("#passw2").css("background-color", "red");
                            $("#passw2").css("border", "1px solid #7F9DB9");
                            $("#passw2").css("padding", "2px 1px 2px 1px");
                            validForm = false;
                        }else if (pass1 != pass2) {
                            $("#passw1").css("background-color", "red");
                            $("#passw1").css("border", "1px solid #7F9DB9");
                            $("#passw1").css("padding", "2px 1px 2px 1px");
                            $("#passw2").css("background-color", "red");
                            $("#passw2").css("border", "1px solid #7F9DB9");
                            $("#passw2").css("padding", "2px 1px 2px 1px");
                            validForm = false;
                        }else{
                            $("#passw1").css("background-color", "#66FF66");
                            $("#passw1").css("border", "1px solid #7F9DB9");
                            $("#passw1").css("padding", "2px 1px 2px 1px");
                            $("#passw2").css("background-color", "#66FF66");
                            $("#passw2").css("border", "1px solid #7F9DB9");
                            $("#passw2").css("padding", "2px 1px 2px 1px");
                        }
                        
                        // comprovació email
                        var filter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
                        if (!filter.test(email)) {
                            $("#email").css("background-color", "red");
                            $("#email").css("border", "1px solid #7F9DB9");
                            $("#email").css("padding", "2px 1px 2px 1px");
                            validForm = false;
                        } else {
                            $("#email").css("background-color", "#66FF66");
                            $("#email").css("border", "1px solid #7F9DB9");
                            $("#email").css("padding", "2px 1px 2px 1px");
                        }

                        return validForm;
                    } 
                    //autosuggest country
                    var options = {
                            script: "http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php?",
                            varname: "country",
                            json: true,
                            maxresults: 5
                    };
                    
                    var contry = new bsn.AutoSuggest(\'country\', options);
                    
                    function actualizaCountry(){
                        var c = document.getElementById("country").value;
                            var options2 = {
                                script: "http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php?coun="+c+"&",
                                varname: "region",
                                json: true,
                                maxresults: 5
                        };
                        var region = new bsn.AutoSuggest(\'region\', options2);
                    }
                    
                    function showResponse(responseText, statusText, xhr, $form){ 
                        
                    } 
                  
                </script>

            </div>
        
        ';
        
    }
    
    
    public function profile($name){
        
        $bd = new Database;
        $bd->conectar();
        
        $bd->query('select * from users_with_country where username=\''.$name.'\'');

        $todo = $bd->dump();
        //Si no existeix, fora!
        if(empty($todo[0]['username'])) echo '<h2>'.USER_DOESNT_EXIST.'</h2>';
        else {
            echo '<div id="userInfo"><h1><img src="http://'.$_SERVER['SERVER_NAME'].'/images/noAvatar.png">&nbsp;&nbsp;&nbsp;'.$todo[0]['username'].'</h1>';
        
        
            if($todo[0]['level'] == 1 ) echo '<small> '.USER.' </small>';
            else if($todo[0]['level'] == 2 ) echo '<small> '.MAINTAINER.' </small>';
            else if($todo[0]['level'] == 3 ) echo '<small> '.ADMINISTRATOR.' </small>';
            else if($todo[0]['level'] < 0 ) echo '<small> '.BANNED.' </small>';
            
            echo '<div id="userProp">
                <br>'.NAME.':  '.$todo[0]['realname'].'<br>
                '.LASTNAME.':  '.$todo[0]['lastname'].'<br>
                '.COUNTRY.':  '.$todo[0]['nombre'].'<br>
                '.POSTAL_CODE.':  '.$todo[0]['postal_code'].'<br>
            </div>';
            
            //informacion del usuario
            if($todo[0]['postal_code'] != "00000"){
                echo '<div id="userMap">';
                $this->mapUser($name);
                echo '</div></div>';
               
            }
            
           
        }
        $this->allSeries($name);
        
        $this->latestSeries($name);
        $this->latestReviews($name);

        $bd->desconectar();
        
    }
    
    public function myProfile(){
        $login = new Login;        
        $bd = new Database;
        $bd->conectar();
        
        $bd->query('select * from users_with_country where username=\''.$login->getUsername().'\'');
        $todo = $bd->dump();
        //Si no existeix, fora!
        if(empty($todo[0]['username'])) echo USER_DOESNT_EXIST;
        else {
            
            echo '<div id="userInfo"><h1><img src="http://'.$_SERVER['SERVER_NAME'].'/images/noAvatar.png">&nbsp;&nbsp;&nbsp;'.$todo[0]['username'].'</h1>';
        
        
            if($todo[0]['level'] == 1 ) echo '<small> '.USER.' </small>';
            else if($todo[0]['level'] == 2 ) echo '<small> '.MAINTAINER.' </small>';
            else if($todo[0]['level'] == 3 ) echo '<small> '.ADMINISTRATOR.' </small>';
            else if($todo[0]['level'] < 0 ) echo '<small> '.BANNED.' </small>';
            
            echo '<div id="userProp">
            <table id="myProfile">
            <thead>
            <tr>
                <th></th><th>'.CLICK_EDIT.'</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td>'.NAME.':</td><td class="editableProfile" id="realname">'.$todo[0]['realname'].'</td>
            </tr>
            <tr>
            <td>'.LASTNAME.':</td><td class="editableProfile" id="lastname"> '.$todo[0]['lastname'].'</td>
            </tr>
            <tr>
            <td>'.EMAIL.':</td><td id="email" class="editableProfile" > '.$todo[0]['email'].'</td>
            </tr>
            <tr>
            <td>'.COUNTRY.':</td><td class="country">'.$todo[0]['nombre'].'</td>
            </tr>
            <tr>
            <td>'.POSTAL_CODE.':</td><td class="editableProfile" id="postal_code">'.$todo[0]['postal_code'].'</td>
            </tr>
            </tbody>
            </table>
            </div></div>';
            echo '<br><br><br><br>';
        }

        
        
        $this->allSeriesEditable($todo[0]['username']);
        
        $this->latestReviews($todo[0]['username']);
        $this->latestSeries($todo[0]['username']);

        $bd->desconectar();
    }
    
    public function mapUser($name){
        echo '<div id="minimap_canvas"></div>';
            $bd = new Database;
            $login = new Login;
            $bd->conectar();
            $bd->select('users.username, users.postal_code, paises.nombre','users, paises','paises.id=users.id_pais and paises.id_idioma=3 and users.username=\''.urlParameters(2).'\'');
            $todo = $bd->dump();
            
            echo '<script type="text/javascript">
				google.load("maps", "3", {"other_params":"sensor=true"} );
  var geocoder;
  var map;
  var marker;
  var start = false;
  var nombre = new Array();
  var address = new Array();';
  
echo 'nombre[0] = \''.$todo[0]['username'].'\';'."\n";
echo 'address[0] = \''.$todo[0]['postal_code'].', '.$todo[0]['nombre'].'\''."\n";
  
  echo 'function initialize() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(41.727255, 1.863556);
        var myOptions = {
          zoom: 8,
          center: latlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("minimap_canvas"), myOptions);

        if (geocoder) {
                        geocoder.geocode( { \'address\' : address[0] }, function(results, status) {
                          if (status == google.maps.GeocoderStatus.OK) {
                            if(start == false){
                                map.setCenter(results[0].geometry.location);
                                start = true;
                            }
                            marker = new google.maps.Marker({
                                map: map, 
                                position: results[0].geometry.location 
                            });
                          }
                        });
        
            
        }
    

    }

    google.setOnLoadCallback(initialize);
</script>';            
            $bd->desconectar();
    }
    
    public function latestReviews($name){
        $bd = new Database;
        $bd->conectar();
        echo '<div id="lastReviews"><h2>'.LAST_REVIEWS.'</h2><marquee onmouseover="this.stop();" onmouseout="this.start();" behavior="scroll" direction="up" scrollamount="2" height="200" width="300">';
        $bd->query(' SELECT series_episodes_comments.comment, series_episodes_comments.date, series.name
                    FROM series_episodes_comments, series, series_episodes, users
                    WHERE series_episodes_comments.id_episode = series_episodes.id
                    AND series.id = series_episodes.id_series
                    AND series_episodes_comments.id_usuario = users.id
                    AND users.username = \''.$name.'\'  
                    UNION SELECT series_comments.comment, series_comments.date, series.name
                    FROM users, series, series_comments
                    WHERE series_comments.id_series = series.id
                    AND series_comments.id_usuario = users.id
                    AND users.username = \''.$name.'\'  
                    ORDER BY 2 DESC
                    LIMIT 10');
        $todo = $bd->dump();
        
        for($i=0;$i<count($todo);$i++){
            echo '<b>'.strip_tags($todo[$i]['comment']).'</b> '.EN.' <a href="http://'.$_SERVER['SERVER_NAME'].'/series/'.$todo[$i]['name'].'">'.$todo[$i]['name'].'</a> '.ON.' <i>'.$todo[$i]['date'].'</i><br><br>';
        }
        $bd->desconectar();
        echo '</marquee></div>';
    }
    
    public function allSeries($name){
        $bd = new Database;
        $bd->conectar();
        echo '<h2>'.ALL_USER_SERIES.'</h2>';
        $bd->select('users.username, series.id as series_id , series.name as series_name, series_episodes.name, series_episodes.season, series_episodes.episode_num, watched.state
                    ','users, series, series_episodes, watched','
                    watched.id_series=series.id and 
                    watched.id_usuario=users.id and
                    watched.id_episode=series_episodes.id and
                    users.username=\''.$name.'\' order by watched.date desc');
        $todo = $bd->dump();
        
        echo '<div id="table1"><table id="myTable">
        <thead>
            <tr>
                <th>'.TV_SERIES.'</th>
                <th>'.CURRENT_EPISODE.'</th>
                <th>'.STATUS.'</th>
            </tr>
        </thead>
        <tbody>
        ';
        for($i=0;$i<count($todo);$i++){
            echo '<tr>';
            echo '<td>'.$todo[$i]['series_name'].'</td><td>'.$todo[$i]['season'].'x'.$todo[$i]['episode_num'].' - '.$todo[$i]['name'].'</td><td>'.$todo[$i]['state'].'</td>';
            echo '</tr>';
        }
        echo '</tbody></table></div>';

        
        $bd->desconectar();
        
    }
    
    public function allSeriesEditable($name){
        $bd = new Database;
        $bd->conectar();
        echo '<h1>'.ALL_USER_SERIES.'</h1>';
        $bd->select('users.username, series.id as series_id , series.name as series_name, series_episodes.name, series_episodes.season, series_episodes.episode_num, watched.state
                    ','users, series, series_episodes, watched','
                    watched.id_series=series.id and 
                    watched.id_usuario=users.id and
                    watched.id_episode=series_episodes.id and
                    users.username=\''.$name.'\' order by watched.date desc');
        $todo = $bd->dump();
        
        echo '<div id=\"tablaLlarga\"><table id="myOtherTable">
        <thead>
            <tr>
                <th>'.TV_SERIES.'</th>
                <th>'.CURRENT_EPISODE.'</th>
                <th>'.STATUS.'</th>
            </tr>
        </thead>
        <tbody>
        ';
        for($i=0;$i<count($todo);$i++){
            echo '<tr>';
            echo '  <td>'.$todo[$i]['series_name'].'</td>
                    <td id="'.$todo[$i]['series_id'].'" class="episode">'.$todo[$i]['season'].'x'.$todo[$i]['episode_num'].' - '.$todo[$i]['name'].'</td>
                    <td id="'.$todo[$i]['series_id'].'" class="status">'.$todo[$i]['state'].'</td>';
            echo '</tr>';
        }
        echo '</tbody></table></div>';
    }
    
    
    public function latestSeries($name){
        $bd = new Database;
        $bd->conectar();
        echo '<div id="lastViews"><h2>'.LAST_VIEWS.'</h2><marquee onmouseover="this.stop();" onmouseout="this.start();" behavior="scroll" direction="up" scrollamount="2" height="200" width="300">';
        $bd->select('users.username, series.name as sname, series_episodes.name as ename, watched.state, watched.date','users, series, series_episodes, watched','
                    watched.id_series=series.id and 
                    watched.id_usuario=users.id and
                    watched.id_episode=series_episodes.id and
                    users.username=\''.$name.'\' order by watched.date desc  limit 10');
        $todo = $bd->dump();
        for($i=0;$i<count($todo);$i++){
            echo '<br><b>'.$todo[$i]['sname'].'</b><br>
                      '.$todo[$i]['ename'].'<br>
                      '.ON.' <i>'.$todo[$i]['date'].'</i><br>';
        }
        
        $bd->desconectar();
        echo '</marquee></div>';
    }
    
    public function mapOfUsers(){

            $bd = new Database;
            $bd->conectar();
            
            $bd->select('username, postal_code, nombre','users_with_country','');
            $todo = $bd->dump();
            
            echo '<script type="text/javascript">
				google.load("maps", "3", {"other_params":"sensor=true"} );
  var geocoder;
  var map;
  var marker;
  var start = false;
  var nombre = new Array();
  var address = new Array(); ';
  
    for($i=0;$i<count($todo);$i++){
        echo 'nombre['.$i.'] = \''.$todo[$i]['username'].'\';'."\n";
        echo 'address['.$i.'] = \''.$todo[$i]['postal_code'].', '.$todo[$i]['nombre'].'\''."\n";
    }
  
  echo 'function initialize() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(41.727255, 1.863556);
        var myOptions = {
          zoom: 8,
          center: latlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    
  
        for(i=0;i<nombre.length;i++){

            if (geocoder) {
                            geocoder.geocode( { \'address\' : address[i] }, function(results, status) {
                              if (status == google.maps.GeocoderStatus.OK) {
                                if(start == false){
                                    map.setCenter(results[0].geometry.location);
                                    start = true;
                                }
                                marker = new google.maps.Marker({
                                    map: map, 
                                    position: results[0].geometry.location 
                                });
                                }
                            });
            }
            
        }
    

    }

    google.setOnLoadCallback(initialize);
</script>
        <h1>'.MAP_OF_USERS.'</h1>
        <div id="map_canvas"></div>
 		            <div style="right: 0px; top: 40px; position: absolute;">';
                            
                            $bd->query('select username from users order by signup_date desc');
                            
 		            $todo = $bd->dump();
                            
                            echo '<h1>'.TOTAL_REGISTERED.': '.count($todo).'</h1>
                            <h2>'.LAST_10.':</h2>';
                            
                            
                            
 		            for($i=0;$i<10;$i++){ 
 		                echo '<br><a href="http://'.$_SERVER['SERVER_NAME'].'/users/'.str_replace(" ", "-", $todo[$i]['username']).'">'.$todo[$i]['username'].'</a>'; 
 		            } 
 		             
 		            $bd->desconectar(); 
 		            echo '</div>'; 
            $bd->desconectar();
            echo '</div>';
    }   
    
	public function changeUserPassword(){
		
		$login = new Login;
		
		if ($login->logged()){
			echo '<div id="newUserPassword"><br><br><br>
					<h1>'.CHANGE_PASSWORD.'</h1>
					<form id="formNewPassword">
						'.OLD_PASSWD.': &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input type="password" name="oldPassword" maxlength="25" length="20"/><br><br>
						'.NEW_PASSWD.': &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<input type="password" name="newPassword" maxlength="25" length="20"/><br><br>
						'.CONFIRM_PASSWD.': &nbsp; &nbsp;<input type="password" name="newPasswordConfirm" maxlength="25" length="20"/><br><br>
						<input type="submit" value="Change"/>
					</form>
					<div id="failLogin"></div>
				</div>';
		}
    }
	
    public function changeAsteriskPassword(){
		
		$login = new Login;
		
		if ($login->logged()){
			echo '<div id="asteriskPassword">
					<h1>'.CHANGE_VOICE_PASSWORD.'</h1>
					<form id="newAsteriskPassword">
						'.CONFIRM_PASSWD.': &nbsp; &nbsp;<input type="text" name="asteriskPassword" maxlength="25" length="20"/><br><br>
						<input type="submit" value="Change"/>
					</form>
					<div id="failLogin2"></div>
				</div>';
		}
    }
    
    public function changeUserLanguage(){
		
		$login = new Login;
		
		if ($login->logged()){
			echo '<div id="changeLanguage">
                                    <h1>'.CHANGE_USER_LANGUAGE.'</h1>
                                    <form id="newLanguage">
                                            <select id="language" name="newLanguage">
                                                <option value="en">English</option>
                                                <option value="es">Espa&ntilde;ol</option>
                                            </select>
                                           <br><input type="submit" value="Change"/>
                                    </form>
                            </div>';
		}
    } 
    
        
    public function timeSpentOnSeries($name){
        //GD para crear imagen
    }
}
?>
