<?php

class Admin {
    
    public function getSeriesInfo(){
        // Retrieve URL from wikipedia or imdb
    }
    
    public function BlogEntries(){
               require_once('includes/imdbphp/imdb.class.php');
            
            $login = new Login;
            
            $conn=new Database;
            $conn->conectar();
      
            $conn->query("SELECT * from blog");
            $matrix=$conn->dump();
            echo "<div id=\"table1\"><table id=\"BlogAdminTable\" class=\"limited\">";
            echo '<thead>
                    <tr> 	 	 	
                            <th>title</th>
                            <th>body</th>
                            <th>date</th>
                            <th>delete</th>
                    </tr>
    
            </thead><tbody>';
            for($i=0;$i<count($matrix);$i++){
                
                $id = $matrix[$i]['id'];
                echo "<tr id=".$matrix[$i]['id'].">
                        <td class=\"editBlog\" id=\"title\">".$matrix[$i]['title']."</td>
                        <td class=\"editBlog\" id=\"body\">".$matrix[$i]['body']."</td>
                        <td class=\"editBlog\" id=\"date\">".$matrix[$i]['date']."</td>
                        <td class=\"deletePost\" id=\"postId\"><label onclick=\"borraPostBlog($id)\"><img src=\"images/deny.png\"></label></td>
                    </tr>";
            }
            echo "</tbody></table></div>";
            
            $conn->desconectar();
    
    }
    
    public function editUsers(){
        require_once('includes/imdbphp/imdb.class.php');
            
            $login = new Login;
            
            $conn=new Database;
            $conn->conectar();
      
            $conn->query("SELECT * from users");
            $matrix=$conn->dump();
            echo "<div id=\"table1\"><table id=\"myUsersAdminTable\">";
            echo '<thead>
                    <tr> 	 	 	
                            <th>username</th>
                            <th>email</th>
                            <th>id_pais</th>
                            <th>realname</th>
                            <th>lastname</th>
                            <th>postal_code</th>
                            <th>level</th>
                    </tr>
    
            </thead><tbody>';
            for($i=0;$i<count($matrix);$i++){
                $rules = array(" "=>"-");
                $name = strtr($matrix[$i]['name'], $rules);
                
                echo "<tr id=".$matrix[$i]['id'].">
                        <td class=\"editeProfileA\" id=\"username\" >".$matrix[$i]['username']."</td>
                        <td class=\"editeProfileA\" id=\"email\" >".$matrix[$i]['email']."</td>
                        <td class=\"editeProfileA\" id=\"id_pais\" >".$matrix[$i]['id_pais']."</td>
                        <td class=\"editeProfileA\" id=\"realname\" >".$matrix[$i]['realname']."</td>
                        <td class=\"editeProfileA\" id=\"lastname\" >".$matrix[$i]['lastname']."</td>
                        <td class=\"editeProfileA\" id=\"postal_code\" >".$matrix[$i]['postal_code']."</td>
                        <td class=\"editeProfileA\" id=\"level\" >".$matrix[$i]['level']."</td>
                    </tr>";
            }
            echo "</tbody></table></div>";
            
            $conn->desconectar();   
    }
    
    public function editSeries(){
            require_once('includes/imdbphp/imdb.class.php');
            
            $login = new Login;
            
            $conn=new Database;
            $conn->conectar();
      
            $conn->query("SELECT * from series");
            $matrix=$conn->dump();
            echo "<div id=\"table1\"><table id=\"mySeriesAdminTable\">";
            echo '<thead>
                    <tr> 	 	 	
                            <th>imdbid</th>
                            <th>duration</th>
                            <th>year</th>
                            <th>name</th>
                    </tr>
    
            </thead><tbody>';
            for($i=0;$i<count($matrix);$i++){
                $rules = array(" "=>"-");
                $name = strtr($matrix[$i]['name'], $rules);
                
                echo "<tr id=".$matrix[$i]['id'].">
                        <td class=\"editeSeries\" id=\"imdbid\">".$matrix[$i]['imdbid']."</td>
                        <td class=\"editeSeries\" id=\"duration\">".$matrix[$i]['duration']."</td>
                        <td class=\"editeSeries\" id=\"year\">".$matrix[$i]['year']."</td>
                        <td class=\"editeSeries\" id=\"name\">".$matrix[$i]['name']."</td>
                    </tr>";
            }
            echo "</tbody></table></div>";
            
            $conn->desconectar();         
    }
    
    public function findUrl(){
        //Put a imdb url and go to addSeries()
        echo LINKS_ALLOWED;
        echo '<br>Provide url: <input type="text" id="url" size=50>';
        echo '<br><input type="submit" onclick="searchID()">';
        echo '<div id="daLink">here</div>';
        
        echo '<script type="text/javascript">
        
            function searchID(){
		var url = $(\'#url\').val();
                $.post(
		    "http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php",
		    { url: url },
		    function(data){
                        $(\'#daLink\').html( data );
		});
	    }
	</script>';
    }
    
    public function addSeries($imdbid){
        require_once('includes/imdbphp/imdb.class.php');
        require_once('includes/basic.php');
        $login = new Login;
        
        // Series define
        $series = new imdb($imdbid);
        $series->setid($imdbid);
        
        //Take out the &#34
        $rules = array("&#34;"=>"","&#x22;"=>"");
        $title = strtr($series->title(),$rules);
        
        
                $login = new Login;
	
        echo '<br>Imdb ID: <input type="text" id="imdbid" value="'.$imdbid.'" readonly>';
        echo '<br>Title: <input type="text" id="title2" value="'.$title.'">';
        echo '<br>Duration: <input type="text" id="duration" value="'.$series->runtime().'">';
        echo '<br>Year: <input type="text" id="year" value="'.$series->year().'">';
        
        echo '<br><input type="submit" onclick="sendSeries()">
        
        <script type="text/javascript">
        
            function sendSeries(){
		var imdbid = $(\'#imdbid\').val();
                var title = $(\'#title2\').val();
                var duration = $(\'#duration\').val();
                var year = $(\'#year\').val();
                $.post(
		    "http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php",
		    { imdbid: imdbid , title: title, duration: duration, year: year },
		    function(data){
			
                        //Falta un spinner y un poco mas de emocion
                        alert(data);
		});
	    }
	</script>';
        
    }
    
    
    public function blogPost(){
        //Shoutbox for series
        require_once('includes/basic.php');
        $login = new Login;
        
        if($login->getLevel() > 2){
            $bd = new Database;
    
            echo '<div id="newPostDiv">';
                    echo '<table class="limited">
                         <tr>  
                            <td>Titulo entrada<br><input type="text" length="40" maxlength="50" name="title" id="title"></td>  
                         </tr>  
                         <tr>  
                            <td><textarea id="newpost"></textarea></td>  
                         </tr>  
                         <tr>  
                            <td><input type="submit" value="'.POST.'!" onclick="sendPost()"/><br></td>  
                         </tr>  
                     </table>
                     </div>';
                  
                  echo '<script type="text/javascript">
                    
                    $( \'#newpost\' ).ckeditor();
                    
                    function sendPost(){
                            var title = $(\'#title\').val();
                            var bodyText = $(\'#newpost\').val();
                            $.post(
                                "http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php",
                                { title: title , bodyText : bodyText },
                                function(data){
                                    if(data == "done") $(\'#newPostDiv\').html("<br>Publicat correctament");
                                    else $(\'#newPostDiv\').append("Error publicant...");
                            });
                        }
                        
                    </script> ';
        } 
    }
    
}


?>