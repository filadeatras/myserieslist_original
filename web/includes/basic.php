<?php

require_once('configuration.php');

// Num de segment de la url (comen�a per 1)
function urlParameters($segment){
	$navString = $_SERVER['REQUEST_URI']; // Agafa la URL
	$parts = explode('/', $navString); // La parteix per "/"
	return $parts[$segment];
}

// Si es vol insertar una cadena a la bbdd, toca fer aix�
function clean($text){
	return mysql_real_escape_string($text);
}

class style{
	
	private $sitename = "MySeriesList";
	private $siteurl;
	
	public function cap(){
		$login = new Login;
		$login->chooseLang();
		$this->siteurl = $_SERVER['SERVER_NAME'];

		$serie_name = str_replace("-", " ", urlParameters(2));
		$serie_episode_id = urlParameters(3);
		echo '
		
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
		<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html;">
			<title>'.$this->sitename.'</title>
			<script type="text/javascript" src="http://'.$this->siteurl.'/js/jquery-1.4.2.min.js"> </script>
			<script type="text/javascript" src="http://'.$this->siteurl.'/js/jquery.form.js"> </script>
			<script type="text/javascript" src="http://'.$this->siteurl.'/js/ckeditor/ckeditor.js"> </script>
			<script type="text/javascript" src="http://'.$this->siteurl.'/js/ckeditor/adapters/jquery.js"> </script>
			<script type="text/javascript" src="http://'.$this->siteurl.'/js/bsn.AutoSuggest_2.1.3.js"> </script>
			<script type="text/javascript" src="http://'.$this->siteurl.'/js/jquery.dataTables.min.js"> </script>
			<script type="text/javascript" src="http://'.$this->siteurl.'/js/jquery.jeditable.js"> </script>
			<script type="text/javascript" src="http://'.$this->siteurl.'/js/jquery-ui-1.8.1.custom.min.js"> </script>
			<script type="text/javascript" src="http://'.$this->siteurl.'/js/jquery.ui.stars.min.js"> </script>
			<script type="text/javascript" src="http://'.$this->siteurl.'/js/jquery.Metadata.js"> </script>
			<script type="text/javascript" src="http://'.$this->siteurl.'/js/jquery.marquee.js"> </script>
			<script type="text/javascript" src="http://www.google.com/jsapi"></script>
			<link rel="stylesheet" type="text/css" href="http://'.$this->siteurl.'/css/autosuggest_inquisitor.css" />
			<link rel="stylesheet" type="text/css" href="http://'.$this->siteurl.'/css/jquery.ui.stars.css" />
			<link rel="stylesheet" type="text/css" href="http://'.$this->siteurl.'/css/style-gen.css" />
			<link rel="stylesheet" type="text/css" href="http://'.$this->siteurl.'/css/table.css" />
			<link rel="stylesheet" type="text/css" href="http://'.$this->siteurl.'/css/south-street/jquery-ui-1.8.1.custom.css" />
			
			<script lang="javascript">
			
			function borraPostBlog(idPost){
			
				confirma = prompt(\'Escriu Borrar per borrar\',\'\');
				
				if(confirma == "Borrar"){
				
					$.post("http://'.$this->siteurl.'/ajaxMethods.php",
						{
						borraPost: idPost
						},
						function(data)
					{
						if (data == "done")
							window.location="/admin";
						else alert("Error borrant");
					});
				}
			}
			
			function wopen(url, name, w, h){
				w += 32;
				h += 96;
				 var win = window.open(url,
				  name,
				  \'width=\' + w + \', height=\' + h + \', \' +
				  \'location=no, menubar=no, \' +
				  \'status=no, toolbar=no, scrollbars=no, resizable=no\');
				 win.resizeTo(w, h);
				 win.focus();
			}
				$(document).ready(function(){

					$("#accordion").accordion({ 
						autoHeight:  false,
						navigation: true
					    });			
					
					var oTable;
					
					/* For users */
					$(\'.status\').editable( \'http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php?saveStatus\', {
							"callback": function( sValue, y ) {
								var aPos = oTable.fnGetPosition( this );
								oTable.fnUpdate( sValue, aPos[0], aPos[1] );
							},
							"submitdata": function ( value, settings ) {
								return { "row_id": this.parentNode.getAttribute(\'id\') };
							},
						data   : " {\'watching\':\''.WATCHING.'\',\'dropped\':\''.DROPPED.'\',\'to_be\':\''.TO_BE_WATCHED.'\', \'watched\':\''.WATCHED.'\', \'stalled\':\''.STALLED.'\'}",
						type   : \'select\',
						submit : \'OK\'

						} );
					
					$(\'.editeProfileA\').editable( \'http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php?editProfileAdmin\', {
							"callback": function( sValue, y ) {
								var aPos = oTable.fnGetPosition( this );
								oTable.fnUpdate( sValue, aPos[0], aPos[1] );
							},
							"submitdata": function ( value, settings ) {
								return { "row_id": this.parentNode.getAttribute(\'id\') };
							},
							height: "14",
							submit: "OK"

						} );
					
					$(\'.editeSeries\').editable( \'http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php?editSeriesAdmin\', {
							"callback": function( sValue, y ) {
								var aPos = oTable.fnGetPosition( this );
								oTable.fnUpdate( sValue, aPos[0], aPos[1] );
							},
							"submitdata": function ( value, settings ) {
								return { "row_id": this.parentNode.getAttribute(\'id\') };
							},
							height: "14",
							submit: "OK"

						} );
						
					$(\'.editBlog\').editable( \'http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php?editBlogEntry\', {
						"callback": function( sValue, y ) {
							var aPos = oTable.fnGetPosition( this );
							oTable.fnUpdate( sValue, aPos[0], aPos[1] );
						},
						"submitdata": function ( value, settings ) {
							return { "row_id": this.parentNode.getAttribute(\'id\') };
						},
						height: "14",
						submit: "OK"

					} );						
						
					$(\'.editableProfile\').editable( \'http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php?editProfile\', {
							"callback": function( sValue, y ) {
								var aPos = oTable.fnGetPosition( this );
								oTable.fnUpdate( sValue, aPos[0], aPos[1] );
							},
							"submitdata": function ( value, settings ) {
								return { "row_id": this.parentNode.getAttribute(\'id\') };
							},
							height: "14",
							submit: "OK"
						} );
						
					$(\'.episode\').editable( \'http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php?saveEpisode\', {
							"callback": function( sValue, y ) {
								var aPos = oTable.fnGetPosition( this );
								oTable.fnUpdate( sValue, aPos[0], aPos[1] );
							},
							"submitdata": function ( value, settings ) {
								return { "row_id": this.parentNode.getAttribute(\'id\') };
							},
							loadurl: \'http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php?listEpisodes\',	
							type   : \'select\',
							submit : \'OK\'
						} );
					

						
					
					$(\'#myOtherTable\').dataTable( {
						"bPaginate": true,
						"bLengthChange": true,
						"bFilter": true,
						"bSort": true,
						"bInfo": true,
						"bAutoWidth": false } );
					
					$(\'#myOtherTable2\').dataTable( {
						"bPaginate": true,
						"bLengthChange": true,
						"bFilter": true,
						"bSort": true,
						"bInfo": true,
						"bAutoWidth": true } );
					
					/* For Series */
					$(\'#myProfile\').dataTable( {
						"bPaginate": false,
						"bLengthChange": false,
						"bFilter": false,
						"bSort": false,
						"bInfo": false,
						"bAutoWidth": false } );	
					
					$(\'.country\').editable( \'http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php?selectCountry\', {
							"callback": function( sValue, y ) {
								var aPos = oTable.fnGetPosition( this );
								oTable.fnUpdate( sValue, aPos[0], aPos[1] );
							},
							"submitdata": function ( value, settings ) {
								return { "row_id": this.parentNode.getAttribute(\'id\') };
							},
							loadurl: \'http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php?listCountries\',	
							type   : \'select\',
							submit : \'OK\'
						} );
						
					$(\'#myTable\').dataTable( {
						"bPaginate": true,
						"bLengthChange": true,
						"bFilter": true,
						"bSort": true,
						"bInfo": true,
						"bAutoWidth": false } );
						
					$(\'#serieInfo\').dataTable( {
						"bPaginate": false,
						"bLengthChange": true,
						"bFilter": false,
						"bSort": false,
						"bInfo": false,
						"bAutoWidth": false } );
					
					$(\'#serieEpisodes\').dataTable( {
						"bPaginate": true,
						"bLengthChange": true,
						"bFilter": true,
						"bSort": false,
						"bInfo": false,
						"bAutoWidth": false } );
						
					$(\'#tableTop\').dataTable( {
						"bPaginate": false,
						"bLengthChange": true,
						"bFilter": false,
						"bSort": false,
						"bInfo": false,
						"bAutoWidth": false } );
						
						
					$(\'#myUsersAdminTable\').dataTable( {
						"bPaginate": true,
						"bLengthChange": true,
						"bFilter": true,
						"bSort": true,
						"bInfo": true,
						"bAutoWidth": false } );
						
					$(\'#mySeriesAdminTable\').dataTable( {
						"bPaginate": true,
						"bLengthChange": true,
						"bFilter": true,
						"bSort": true,
						"bInfo": true,
						"bAutoWidth": false } );
					
					$(\'#BlogAdminTable\').dataTable( {
						"bPaginate": true,
						"bLengthChange": true,
						"bFilter": true,
						"bSort": true,
						"bInfo": true,
						"bAutoWidth": false } );
										
					$("#stars-wrapper1").stars({
						captionEl: $("#stars-cap"),
						split: 2,
						callback: function(ui, type, value){
							$.post("http://'.$this->siteurl.'/ajaxMethods.php", {serie_rate: value, serie_name: \''.$serie_name.'\'}, function(data)
							{
								$("#ajax_response").html(data);
							});
						}
					});
					
					$("#stars-episodes-wrapper1").stars({
						captionEl: $("#stars-cap"),
						split: 2,
						callback: function(ui, type, value){
							$.post("http://'.$this->siteurl.'/ajaxMethods.php", {serie_episode_rate: value, serie_episode_id: \''.$serie_episode_id.'\'}, function(data)
							{
								$("#ajax_response").html(data);
							});
						}
					});
					
					$("#stars-wrapper-portada").stars({
					     disabled: true
					});
					
					// Enviamos el formulario usando AJAX
					
					$(\'#form1\').submit(function() {
						$("#validateLogin").html(" ");
						$("#validateLogin").hide();
						$("#validateLogin").append("<img src=\"http://'.$this->siteurl.'/images/ajax-loader.gif\" />");
						$("#validateLogin").fadeIn();
						$.ajax({
							type: \'POST\',
							url: \'http://'.$this->siteurl.'/ajaxMethods.php\',
							data: $(this).serialize(),
							// Mostramos un mensaje con la respuesta de PHP
							success: function(data) {
								if(data=="true"){
									location.reload();
								}else {
									$("#validateLogin").html(" ");
									$("#validateLogin").append("Error! ");
									$("#validateLogin").fadeIn();
								}
							}
						})        
						return false;
					 });
				   
					 $(\'#form2\').submit(function() {
						$.ajax({
							type: \'POST\',
							url: \'http://'.$this->siteurl.'/ajaxMethods.php\',
							data: $(this).serialize(),
							// Mostramos un mensaje con la respuesta de PHP
							success: function(data) {
								if(data=="logoff"){
									location.reload();
								}
							}
						})        
						return false;
					 });
					 
					$(\'#formNewPassword\').submit(function() {
						$.ajax({
							type: \'POST\',
							url: \'http://'.$this->siteurl.'/ajaxMethods.php\',
							data: $(this).serialize(),
							// Mostramos un mensaje con la respuesta de PHP
							success: function(data) {
								if(data=="changed"){
									$("#failLogin").html(" ");
									$("#failLogin").append("'.PASSWORD_CHANGED.'");
									$("#failLogin").fadeIn();
								}
								else if (data=="fail"){
									$("#failLogin").hide();
									$("#failLogin").html(" ");
									$("#failLogin").append("'.PASSWORD_INCORRECT.'");
									$("#failLogin").fadeIn();
								}
							}
						})        
						return false;
					});
					 
					$(\'#newAsteriskPassword\').submit(function() {
					       $.ajax({
						       type: \'POST\',
						       url: \'http://'.$this->siteurl.'/ajaxMethods.php\',
						       data: $(this).serialize(),
						       // Mostramos un mensaje con la respuesta de PHP
						       success: function(data) {
							       if(data=="changed"){
								       $("#failLogin2").html(" ");
								       $("#failLogin2").append("'.PASSWORD_CHANGED.'");
								       $("#failLogin2").fadeIn();
							       }
							       else if (data=="fail"){
								       $("#failLogin2").hide();
								       $("#failLogin2").html(" ");
								       $("#failLogin2").append("'.PASSWORD_INCORRECT.'");
								       $("#failLogin2").fadeIn();
							       }
						       }
					       })        
					       return false;
					});
					 
					$(\'#newLanguage\').submit(function() {
					       $.ajax({
						       type: \'POST\',
						       url: \'http://'.$this->siteurl.'/ajaxMethods.php\',
						       data: $(this).serialize(),
						       // Mostramos un mensaje con la respuesta de PHP
						       success: function(data) {
								window.location.reload();
						       }
					       })        
					       return false;
					});
					
					$(\'#reportEr\').submit(function() {
						$.ajax({
							type: \'POST\',
							url: \'http://'.$this->siteurl.'/ajaxMethods.php\',
							data: $(this).serialize(),
							// Mostramos un mensaje con la respuesta de PHP
							success: function(data) {
								if(data=="done"){
									$("#reportError").html(" ");
									$("#reportError").append("Thanks for your advise.");
									$("#reportError").fadeIn();
								}
								else if (data=="error"){
									$("#reportError").html(" ");
									$("#reportError").append("It seems to be a problem, relog or try later.");
									$("#reportError").fadeIn();
								}
							}
						})  
						return false;
					});
					 
				});
			</script>
	</head>
	<body>
	<div id="pageup"></div>
		<div id="pagecenter"></div>
		<div id="titlebar">
			<a href="http://'.$this->siteurl.'" id="logo"><img src="http://'.$this->siteurl.'/images/logo.png" /></a>
			<div id="upOther">
				<div id="login">';
				
					$login=new Login;
										
					if($login->logged()){
						echo '<div id="loginForm">
								<form id="form2" method="POST">
									<div id="logout">
										<input id="logout" type="text" name="logout" value="1"/></span></label>
									</div>
									'.WELCOME.' '.$login->getUsername().'&nbsp;&nbsp;&nbsp;<input type="submit" value="Log Out" class="confi"/>&nbsp;&nbsp;&nbsp;&nbsp;
									<p><a href="http://'.$this->siteurl.'/users/'.$login->getUsername().'">'.USER_PROFILE.'</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
								</form>
						</div>';
					} else{
				
						echo '<div id="loginForm">
							<form id="form1" method="POST">
								<label><input type="text" id="username" name="username"/></label>&nbsp;
								<label><input type="password" id="password" name="password"/></label>
								<input type="submit" value="'.LOGIN.'" class="confi">
								<p>'.NOT_REGISTERED.' <a href="http://'.$this->siteurl.'/signup">'.HERE.'</a><div id="validateLogin"></div></p>
							</form>
						</div>';
					}
					
				echo '</div>
			</div>
		</div>
		
		<div id="navbar">
			<ul id="navlinks">';
				$section = urlParameters(1);
				if($section == "") echo '<li class="active"><a href="http://'.$_SERVER['SERVER_NAME'].'">'.MAIN.'</a></li>';
				else echo '<li><a href="http://'.$this->siteurl.'">'.MAIN.'</a></li>';
				if($section == "blog") echo '<li class="active"><a href="/blog">'.BLOG.'</a></li>';
				else echo '<li><a href="http://'.$this->siteurl.'/blog">'.BLOG.'</a></li>';
				if($section == "series") echo '<li class="active"><a href="/series">'.SERIES.'</a></li>';
				else echo '<li><a href="http://'.$this->siteurl.'/series">'.SERIES.'</a></li>';
				if($section == "users") echo '<li class="active"><a href="/users">'.USERS.'</a></li>';
				else echo '<li><a href="http://'.$this->siteurl.'/users">'.USERS.'</a></li>';
				if($login->getLevel() > 1) {
					if($section == "admin") echo '<li class="active"><a href="http://'.$this->siteurl.'/admin">Admin</a></li>';
					else echo '<li><a href="http://'.$this->siteurl.'/admin">Admin</a></li>';
				}
			echo '</ul>
		</div>
	
		<div id="contents">
		';

	}
	public function peu(){
		echo '	</div></div>
				<div id="footer">
					<div id="footernav">
						<p class="name">MySeriesList &copy; <small>by <a href="http://franstelecom.com">FransTelecom</a></small></p>
						<ul id="footerbar">
							<li><a href="http://franhp.net">franhp</a> &amp;&amp; <a href="http://hcosta.info">hektor</a></li>
						</ul>
						
					</div>
				</div>
			</body>
		</html>
		';
	}
	
}


class Login {
	
	private $username;
	private $password;
	
	/*Startup */
	public function __construct() {
		session_start();
		//$this->checkUserIP();
		if(!isset($_SESSION['auth'])){
			$_SESSION['auth'] = false;
		}		
	}

	/* Return the userid */
	public function getUserID() {
		return $_SESSION['auth'];
	}
	/* Return the username of a member*/
	public function getUsername() {
		$bd = new Database;
		$bd->conectar();
		$bd->select('username','users','id=\''.$_SESSION['auth'].'\'');
		$name = $bd->dump();
		$this->username = $name[0]['username'];
		return $this->username;
	}

	/* Get a member's IP Address */
	public function getUserIP() {
		return getenv("REMOTE_ADDR");
	}
	
	
	/* Mostra el nivell del usuari (-1 ban, 1 usuari, 2 manager, 3 admin) */
	public function getLevel() {
		return $_SESSION['level'];
	}

	/* Validate an email is in the correct format e.g. someone@somewhere.com */
	public function validateEmail($email) {
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return false;
		}
		return true;
	}
	
	/* Actually does the login part */
	public function doLogin($username,$password){
		$bd = new Database;
		$bd->conectar();
		$bd->select('id,username,password,level','users','username=\''.$this->clean($username).'\' and password=\''.$this->clean($password).'\'');
		
		if(!$bd->num_rows()) return false;
		else {
			$userdata = $bd->obj();
			foreach ($userdata as $user){
				$this->username = $user->username;
				$this->password = $user->password;
				$_SESSION['auth'] = $user->id;
				$_SESSION['level'] = $user->level;
			}
			
			$bd->update('users','ip',$this->getUserIP(),'username=\''.$this->username.'\'');
			$bd->desconectar();
			
			return true;
		}
	}
	
	/* To check if user is logged in */	
	public function logged(){
		if($_SESSION['auth']==true) return true;
		else return false;
	}
	
	/* To log off the user */
	public function logoff(){
		unset($_SESSION['auth']);
		unset($_SESSION['level']);
		$_SESSION = array();
		session_destroy();
		return true;
	}
	

	/* Compare the member's IP with the IPs recorded in the database. 
	If the IP appears more than 10 times, display the ban message 
	*/
	public function checkUserIP() {
		$ip = $this->getUserIP();
		$bd = new Database;
		$bd->conectar();
		$bd->select('*','users','IP=\''.$ip.'\' LIMIT 0,5');
		
			if ($bd->num_rows() >= 10) {
				echo 'Too much connections';
			}
		
		$bd->desconectar();
	}

	
	/* Escape the input */
	public function clean($input) {
		return mysql_real_escape_string($input);
	}
	
	
	/* Other Logins section */
	
	
	public function twitterLogin(){
		require_once('includes/oauth/twitter/twitteroauth/twitteroauth.php');
                /* Build TwitterOAuth object with client credentials. */
                $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);

                 /* Get temporary credentials. */
                 $request_token = $connection->getRequestToken(OAUTH_CALLBACK);

                 /* Save temporary credentials to session. */
                 $_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
                 $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

                  /* If last connection failed don't display authorization link. */
                  switch ($connection->http_code) {
			case 200:
			/* Build authorize URL and redirect user to Twitter. */
			$url = $connection->getAuthorizeURL($token);
			echo '<meta HTTP-EQUIV="REFRESH" content="0; url='.$url.'">';
			break;
			default:
			/* Show notification if something went wrong. */
			echo 'Could not connect to Twitter. Refresh the page or try again later.';
		}

	}
	
	public function twitterCallback(){
		
		/**
		* @file
		* Take the user when they return from Twitter. Get access tokens.
		* Verify credentials and redirect to based on response from Twitter.
		*/
	
		require_once('includes/oauth/twitter/twitteroauth/twitteroauth.php');
		$login = new Login;
	
	
		/* If the oauth_token is old redirect to the connect page. */
		if (isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
		$_SESSION['oauth_status'] = 'oldtoken';
			$login->logoff();
		}
    
		/* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
    
		/* Request access tokens from twitter */
		$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
    
		/* Save the access tokens. Normally these would be saved in a database for future use. */
		$bd = new Database;
		$login = new Login;
		$bd->conectar();
		$bd->update('users','twitter_token',$access_token['oauth_token'],'id='.$login->getUserID());
		$bd->update('users','twitter_token_secret',$access_token['oauth_token_secret'],'id='.$login->getUserID());
		$bd->update('users','twitter_userid',$access_token['user_id'],'id='.$login->getUserID());
		$bd->update('users','twitter_screen_name',$access_token['screen_name'],'id='.$login->getUserID());
		
		$bd->desconectar();
		
		/* Remove no longer needed request tokens */
		unset($_SESSION['oauth_token']);
		unset($_SESSION['oauth_token_secret']);
    
		/* If HTTP response is 200 continue otherwise send to connect page to retry */
		if (200 == $connection->http_code) {
		/* The user has been verified and the access tokens can be saved for future use */
		  $_SESSION['status'] = 'verified';
		    header('Location: ./twitterupdate');
		    } else {
		      /* Save HTTP status for error dialog on connnect page.*/
			echo 'Account not verified';
		}
	}
	
	
	public function twitterConnect(){
	        require_once('includes/oauth/twitter/twitteroauth/twitteroauth.php');
		/* Get user access tokens out of the session. */
		$bd = new Database;
		$login = new Login;
		
		$bd->conectar();
		$bd->select('twitter_token,twitter_token_secret,twitter_userid, twitter_screen_name','users','id='.$login->getUserID());
		$todo = $bd->dump();
		$access_token = array();
		$access_token['oauth_token'] = $todo[0]['twitter_token'];
		$access_token['oauth_token_secret'] = $todo[0]['twitter_token_secret'];
		$access_token['user_id'] = $todo[0]['twitter_userid'];
		$access_token['screen_name'] = $todo[0]['twitter_screen_name'];
		$bd->desconectar();
		
		return $access_token;
	}
	
	public function facebookLogin(){
	    
	}
    	public function chooseLang(){
		$login = new Login;
		$bd = new Database;
		$bd->conectar();
		
		$bd->select('preferred_language','users','id='.$login->getUserID());
		$todo = $bd->dump();
		$lang = $todo[0]['preferred_language'];
		$bd->desconectar();
		
		if($lang == "ca" ) include('lang/catalan.php');
		else if($lang == "es" ) include('lang/spanish.php');
		else if($lang == "en" ) include('lang/english.php');
		else include('lang/english.php');
		
		return $language;
	}

}





/**
 * Clase Database: Crea una conexion a la base de datos, tiene metodos para consultar y volcar resultados
 * Ejemplos: $db->conectar; $db->select("*", "prueba",""); $db->dump(); $db->desconectar();
 **/

class Database{
	
	/*
	 
	 Falta implementar esto
	 

	public function clean($input) {
		return mysql_real_escape_string($input);
	}
	
	
	*/
	
	private $dbhost = DB_HOST;
	private $dbuser = DB_USER;
	private $dbpass = DB_PASS;
	private $dbname= DB_NAME;
	
	
	private $query;
	private $result;
	private $row;
	
	public function conectar()
	{
		@ mysql_connect($this->dbhost,$this->dbuser, $this->dbpass) or die ('Error conectando a mysql');
		@ mysql_selectdb($this->dbname) or die ('Error conectando a mysql');
	}
	
	public function _clean($text){
		return mysql_real_escape_string($text);
	}
	
	public function select($fields, $table_name,  $filters)
	{
		if(empty($table_name)||empty($fields)) die ('Query Incorrecta');
		
		
		$this->query = "Select ".$fields." from ".$table_name;
		
		if (!empty($filters)) $this->query =  $this->query." where ".$filters;
		
		$this->result =  @ mysql_query ($this->query);
	}
	
	public function query($query){
		$this->query = $query;
		
		$this->result =  @ mysql_query ($this->query);
	}
	
	public function _rRow(){
		if(!$this->result) return;
		$this->row = @ mysql_fetch_assoc($this->result);
		return $this->row;
	}
	
	 // Retorna una matriz del estilo $matrix[Numero_registro][Nombre_Columna][Valor_Columna]
	public function dump()
	{
		
		if(!$this->result) return;
		else {
			$i = 0;
			while($i < @ mysql_num_fields($this->result))
			{
				$meta = @ mysql_fetch_field($this->result, $i);
				$fieldnames[]=$meta->name;
				$i++;
			}
			
			$y=0;
			while($this->row = @ mysql_fetch_assoc($this->result))
			{
				for($i=0;$i<count($fieldnames);$i++)
				{
					$matrix[$y][$fieldnames[$i]] = $this->row[$fieldnames[$i]];
				}
				$y++;
			}
			
			return $matrix;
		}
		@ mysql_free_result($this->result);
	}
	
	public function _json($nombre){
		
		header("Content-Type: application/json");
		$arr = array();
		$i=0;
		echo "{\"results\": [";
		while ($row = mysql_fetch_assoc($this->result)) {
		    $str = $row[$nombre];
		    $arr[] = "{\"id\": \"".$i."\", \"value\": \"".$str."\", \"info\": \"\"}";
		    $i++;    
		}
		echo implode(", ", $arr);
		echo "]}";
	}
	
	public function _isset() {
		if(mysql_fetch_row($this->result) > 0) return true;
		else return false;
	}
	
	public function obj() {
		$array = array();  
		while ($fila = @mysql_fetch_object($this->result)) {
			$array[] = $fila;  	
		} 
		return $array;
	}
	
	public function num_rows(){
		if(!$this->result) return;
		else {
			$rows = @mysql_num_rows($this->result);
		}
		return $rows;
	}
	
	public function insert($table_name, $fields, $values){
		if(empty($table_name)||empty($fields)) die ('Query Incorrecta');
		
		$this->query = 'Insert into '.$table_name.' ('.$fields.') values ('.$values.')';
		
		$this->result =  @ mysql_query ($this->query);
	}
	
	public function update($table_name, $column, $value, $filters){
		if(empty($table_name)||empty($column)||empty($filters)) die ('Query Incorrecta');
		
		$this->query = 'update '.$table_name.' set '.$column.'=\''.$value.'\' where '.$filters;
		
		$this->result =  @ mysql_query ($this->query);
	}
	
	public function delete($table_name, $filters){
		if(empty($table_name)||empty($filters)) die ('Query Incorrecta');
		
		$this->query = 'delete from '.$table_name.' where '.$filters;
		
		$this->result =  @ mysql_query ($this->query);
	}
	
	public function debug(){
		echo $this->query;
	}
	
	public function desconectar()
	{
		@ mysql_free_result($this->result);			
		@ mysql_close();
	}
}


?>
