<?php

class wikiextractor 
{
	var $title;
	var $genre;
	var $seasons;
	var $episodes;
	var $duration;
	var $imdbid;
	
	var $director;
	var $producer;
	var $starring;
	var $company;
	
	var $first_air;
	var $last_air;
	
	
	//Funci� que troba les parts necessaries d'un enlla� de la wikipedia per a poder
	// utilitzar wikiextractor->series_info
	
	function series_search($link){
		$link_clean = explode("/", $link);
		$lang_clean = explode(".", $link_clean[2]);
		
		$result['lang'] = $lang_clean[0];
		$result['name'] = $link_clean[4];
		
		return $result;
		
	}
	
	function series_info($serie, $idioma){
		if($idioma=="es") $starting_template = "{{Ficha de serie de";
		else if($idioma=="en") $starting_template = "{{Infobox television";
		
		//Anem a la wikipedia a veure la web i agafar les dades importants
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://'.$idioma.'.wikipedia.org/w/index.php?title='.$serie.'&action=edit');
		// A vegades la wikipedia no dona la informaci� si no creu que ets un navegador normal
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; es-ES; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3");
		
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($ch);
		curl_close($ch);
		
		//Sacar la caja donde esta todo el articulo
		$start = strpos($data, "<textarea");
		$end = strpos($data, "</textarea>");
		
		for($i=$start;$i<$end;$i++)
		{
			$textarea = $textarea.$data[$i];
		}
		
		//Encontrar la secci�n de la Ficha tecnica
		$start = strpos($textarea, $starting_template);
		if(!$start) echo $textarea;
		if(!$start) die('No info');
		
		//Comenzamos desde el primer | que esta a 32 caracteres del principio
		for($i=$start+32;$i<strlen($textarea);$i++)
		{
			$firstclean = $firstclean.$textarea[$i];
		}
		//Parto todas las linias en un array
		$secondclean = explode("\n" , $firstclean);
		
		//Separo las linias de los arrays en m�s arrays con los datos
		for($i=0;$secondclean[$i]!="}}";$i++)
		{
			$matrix[$i] = explode("=", $secondclean[$i]);
			//Limpiar uno
			$trans = array("|"=>"");
			$matrix[$i][0] =  trim(strtr($matrix[$i][0], $trans));
				
			
			//Limpiar dos
			$trans = array("&lt;br />"=>", ","[["=>" ",  "]]"=>" ", "{{"=>"", "}}"=>"");
			$matrix[$i][1] = strtr($matrix[$i][1], $trans);
			
			
				
			if($idioma == "es"){	
				if(strpos($matrix[$i][0], "tulo_original")){
					$this->title = $matrix[$i][1];
				}
				else if(strpos($matrix[$i][0], "nero")){
					$this->genre = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "um_temporadas")){
					$this->seasons = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "um_episodios")){
					$this->episodes = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "uraci")){
					$this->duration = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "mdb_id")){
					$this->imdbid = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "roductor")){
					$this->producer = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "irector")){
					$this->director = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "eparto")){
					$this->starring = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "mpresa")){
					$this->company = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "adena")){
					$this->channel = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "rimera_emisi")){
					
					$cadena = explode("|", $matrix[$i][1]);
					if(empty($cadena[3])) $this->first_air = $matrix[$i][1];
					else $this->first_air = $cadena[1].'-'.$cadena[2].'-'.$cadena[3];
					
				} else if(strpos($matrix[$i][0], "ltima_emisi")){
					$cadena = explode("|", $matrix[$i][1]);
					if(empty($cadena[3])) $this->last_air = $matrix[$i][1];
					else $this->last_air = $cadena[1].'-'.$cadena[2].'-'.$cadena[3];
					
				}
			}
			
				
			if($idioma == "en"){	
				if(empty($matrix[$i][0])){
					$this->title = $matrix[$i][1];
				}
				else if($matrix[$i][0]=="format"){
					$this->genre = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "um_seasons")){
					$this->seasons = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "um_episodes")){
					$this->episodes = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "untime")){
					$this->duration = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "mdb_id")){
					$this->imdbid = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "xecutive_producer")){
					$this->producer = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "irector")){
					$this->director = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "tarring")){
					$this->starring = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "ompany")){
					$this->company = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "etwork")){
					$this->channel = $matrix[$i][1];
					
				} else if(strpos($matrix[$i][0], "irst_aired")){
					
					$cadena = explode("|", $matrix[$i][1]);
					if(empty($cadena[3])) $this->first_air = $matrix[$i][1];
					else $this->first_air = $cadena[1].'-'.$cadena[2].'-'.$cadena[3];
					
				} else if(strpos($matrix[$i][0], "ast_aired")){
					$cadena = explode("|", $matrix[$i][1]);
					if(empty($cadena[3])) $this->last_air = $matrix[$i][1];
					else $this->last_air = $cadena[1].'-'.$cadena[2].'-'.$cadena[3];
					
				}
			}
			
			
		}
	}
		
}

?>