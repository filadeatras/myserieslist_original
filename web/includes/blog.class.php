<?php
class Blog{
    public function showArticles(){
        $bd = new Database();
        $bd->conectar();
        $bd->select('blog.*,users.username','blog,users','users.id=blog.id_usuario limit 0,10');
        $resultat = $bd->obj();
	echo "<div id=\"news\">";
	echo "<ol class=\"newsList\" id=\"divNews\">";
        foreach ($resultat as $article){
	    echo '<li id="new'.$article->id.'">';
            //Model de article de blog
            echo '<div id="articleTitle"><h2>'.$article->title.'</h2></div>';
            echo '<div id="articleData">'.BY.' <a href="http://'.$_SERVER['SERVER_NAME'].'/users/'.$article->username.'">'.$article->username.'</a> '.ON.' <i>'.$article->date.'</i></div>';
	    echo '<div id="articleImg"><img src="/images/noAvatar.png" /></div>';
	    echo '<div id="articleText">'.$article->body.'</div>';
            $bd->select('*','blog_comments','id_articulo='.$article->id);
            $num = $bd->num_rows();
            if(!$num) echo '<div id="articleComment">'.NO_COMMENTS.' <a href="http://'.$_SERVER['SERVER_NAME'].'/blog/'.$article->id.'"><br>'.COMMENT.'!</a></div>';
            else echo '<div id="articleComment">'.HAS.' '.$num.' '.COMMENTS.'<a href="http://'.$_SERVER['SERVER_NAME'].'/blog/'.$article->id.'"><br>'.COMMENT.'!</a></div>';
            echo '</li>';
        }
	echo "</ol>";
        $bd->desconectar();
    }
    
    public function showArticle($id){
        $bd = new Database();
        $bd->conectar();
        $bd->select('blog.*,users.username','blog,users','users.id=blog.id_usuario and blog.id=\''.urlParameters(2).'\'');
        $resultat = $bd->obj();
        echo "<div id=\"news\">";
	echo "<ol class=\"newsList\" id=\"divNews\">";
        foreach ($resultat as $article){
            echo '<li id="new'.$article->id.'">';
            //Model de article de blog
            echo '<div id="articleTitle"><h2>'.$article->title.'</h2></div>';
            echo '<div id="articleData">'.BY.' <a href="http://'.$_SERVER['SERVER_NAME'].'/users/'.$article->username.'">'.$article->username.'</a> '.ON.' <i>'.$article->date.'</i></div>';
	    echo '<div id="articleImg"><img src="/images/noAvatar.png" /></div>';
	    echo '<div id="articleText">'.$article->body.'</div>';
            $bd->select('blog_comments.*,users.username','blog_comments,users','users.id=blog_comments.id_usuario and id_articulo='.$article->id);
	    
            echo '<br><h3>'.COMMENTS.'</h3>';
	    echo "<div id=\"comments\">";
	    echo "<ol class=\"commentList\" id=\"divComments\">";
            if($bd->num_rows()>=1){
                $resultat = $bd->obj();
                foreach ($resultat as $comment){
		    echo '<li id="comment'.$comment->id.'">';
		    echo '<div id="dataComment">
				<div id="imgComment"><img src="/images/noAvatar.png" /></div>
				<div id="commentAuthor"><a href="http://'.$_SERVER['SERVER_NAME'].'/users/'.$comment->username.'">'.$comment->username.'</a></div>
				<div id="commentDate"><i> '.$comment->date .'</i></div>
			    </div>
			<div id="textComment">'.$comment->comment.'</div>
			';
			
		    echo '</li>';
                }
		echo "</ol>";
		echo "</div>";
            }
            else '<br>'.NO_COMMENTS.' <a href="http://'.$_SERVER['SERVER_NAME'].'/blog/'.$article->id.'">'.COMMENT.'</a>';
        }
	echo "</ol>";
        $bd->desconectar();
        
        //Si esta connectat, pot fer comentaris
        $login = new Login;
        if($login->logged()) $this->newComment();
        else echo 'If you want to comment, you must register';
        
    }
    
    public function newComment(){
        $login = new Login;
	
	echo '<br>'.$login->getUsername().' '.SAYS.' <textarea id="commentBox"></textarea>
        <input type="submit" onclick="sendComment()">
        
        <script type="text/javascript">
        
            $( \'#commentBox\' ).ckeditor({
              toolbar : \'Basic\'
            });
	    
            function sendComment(){
		var i = Math.ceil(10000*Math.random());
		var text = $( \'#commentBox\' ).val();
		$("#divComments").append("<div id=\"spinner"+i+"\"><li><img src=\"/images/spinner.gif\" alt=\"Wait\" /></li></div>");
		$.post(
		    "http://'.$_SERVER['SERVER_NAME'].'/ajaxMethods.php",
		    { message: text , article: '.urlParameters(2).' },
		    function(data){
                        if ((data != "exceded") && (data != "error")) {
                            $("#spinner"+i).html(" ");
                            $("#divComments").append("<li>"+data+"</li><br>");
                        }
                        else if (data == "exceded")  {
                            $("#spinner"+i).html(" ");
                            alert("Wait 1 minute to comment one more time");
                        }
                        else if (data == "error")  {
                            $("#spinner"+i).html(" ");
                            alert("Error posting de message, try again later.");
                        }
		    }
		);
	    }
	</script>';
    }
    
    public function exists_post($id){
        
        $bd = new Database;
        $bd->conectar();
        
        $id = $bd->_clean($id);
        $bd->select('id', 'blog', "id =  $id");
        
        if ($bd->num_rows() > 0) {
            return true;
        }
        else { 
            $bd->desconectar();
            return  false;
        }
    }
};
?>