<?php
define('TEST', 'test en castellano');

/* Index */
define('TOP_5', 'Top 10');
define('EDIT_SERIES','Editar series');
define('EDIT_USERS','Editar Users');
define('NOT_ADMIN','No eres admin!');
define('MARQUEE_MESSAGE','�MySeriesList es un proyecto abandonado! Pero prometemos volver');

/* Basic */
define('WATCHING','Viendo');
define('DROPPED','Abandonada');
define('WATCHED','Vista');
define('TO_BE_WATCHED','Quiero verla');
define('STALLED','Parada');

define('PASSWORD_CHANGED','Contrase&ntilde;a cambiada satisfactoriamente!');
define('PASSWORD_INCORRECT','Valores incorrectos :-(');

define('USER_PROFILE','Perfil del usuario');
define('NOT_REGISTERED','�Todav&iacute;a no estas registrado? Hazlo');
define('HERE','aqu&iacute;');

define('MAIN','Principal');
define('BLOG','Blog');
define('SERIES','Series');
define('USERS','Usuarios');
define('LOGIN','Entrar');

/* Users */
define('SIGN_UP','Formulario de registro');
define('ACC_INFO','Informaci&oacute;n de la cuenta');
define('USERNAME','Nombre de usuario');
define('PASSWORD','Contrase&ntilde;a');
define('REPEAT_PASS','Repetir contrase&ntilde;a');
define('EMAIL','Correo electr&oacute;nico');
define('PERSONAL_INFO','Informaci&oacute;n personal (opcional)');
define('SEX','Sexo');
define('MALE','Hombre');
define('FEMALE','Mujer');
define('NAME','Nombre');
define('LASTNAME','Apellido');
define('COUNTRY','Pa&iacute;s');
define('POSTAL_CODE','C&oacute;digo postal');
define('LANGUAGE','Idioma');
define('VERIFICATION','Verificaci&oacute;n');
define('SEND', 'Enviar');

define('WELCOME', 'Bienvenido');

define('CHANGE_VOICE_PASSWORD','Cambiar la contrase&ntilde;a de las salas de chat');
define('CHANGE_USER_LANGUAGE','Cambiar el idioma');

define('USER_DOESNT_EXIST','El usuario no existe! oops?');
define('USER','Usuario');
define('MAINTAINER','Mantenedor');
define('BANNED','Baneado');
define('ADMINISTRATOR','Administrador');

define('REVIEWS', 'Comentarios');
define('CLICK_EDIT','Click para editar');
define('LAST_REVIEWS','&Uacute;ltimos comentarios');
define('ALL_USER_SERIES','Todas las series');
define('TV_SERIES','Series');
define('CURRENT_EPISODE','Episodio actual');
define('STATUS','Estado');
define('LAST_VIEWS','&Uacute;ltimas vistas');
define('MAP_OF_USERS','Mapa de usuarios');
define('TOTAL_REGISTERED','Usuarios registrados');
define('LAST_10','Los �ltimos 10 son');

define('CHANGE_PASSWORD','Cambio de contrase&ntilde;a');
define('OLD_PASSWD','Contrase&ntilde;a anterior');
define('NEW_PASSWD','Nueva contrase&ntilde;a');
define('CONFIRM_PASSWD','Confirme la nueva contrase&ntilde;a');
define('EN','en');
define('MESSAGE','Mensaje');

define('RATING','Puntuaci&oacute;n');
define('VERY_POOR','Muy mala');
define('POOR','Mala');
define('NOT_BAD','No tan mala');
define('FAIR','Casi normal');
define('AVERAGE','Normal');
define('ALMOST_GOOD','Casi buena');
define('GOOD','Buena');
define('VERY_GOOD','Muy buena');
define('EXCELLENT','Excelente');
define('PERFECT','Perfecta');

define('SEASON','Temporada');
define('EPISODE','Episodio');
define('AIRED','Emitido');
define('RUNTIME','Duraci&oacute;n');
define('YEAR','A&ntilde;o');
define('DIRECTORS','Directores');
define('CAST','Reparto');
define('PLOT','Argumento');

define('AVG','Media');
define('LAST_FOLLOWED','&Uacute;ltimas series seguidas');
define('UPDATED','actualiz&oacute;');
define('LAST_BLOG','&Uacute;ltimas noticias');

/* AjaxMethods */
define('USER_CREATED','El usuario se ha creado correctamente');
define('JUST_NOW','Ahora mismo!');
define('CLICK_HERE','Click aqui');
define('NOT_FOUND','No se ha encontrado');
define('REGISTER_TO_VOTE','Reg&iacute;strese para votar :-(');
define('CHANGED','Cambiado');


/* Blog */
define('NO_COMMENTS', 'No hay comentarios, se el primero');
define('COMMENT', 'Comenta'); //Imperativo
define('COMMENTS', 'Comentarios');
define('HAS', 'Tiene');
define('BY', 'por');
define('ON', 'el');
define('SAYS', 'dice');
define('SAID', 'dijo');

/* Admin */
define('ADD_POST','A&ntilde;adir post');
define('EDIT_POSTS','Editar posts');
define('ADD_SERIES','A&ntilde;adir series');
define('LINKS_ALLOWED','Se pueden insertar enlaces de IMDB y Wikipedia');
define('POST','Publica');


?>
