<?php
define('TEST', 'test in english');

/* Index */
define('TOP_5', 'Top 10');
define('EDIT_SERIES','Edit series');
define('EDIT_USERS','Edit users');
define('NOT_ADMIN','You are not admin!');
define('MARQUEE_MESSAGE','MySeriesList is an abandoned project! But we promise that we\'ll come back!');

/* Basic */
define('WATCHING','Watching');
define('DROPPED','Dropped');
define('WATCHED','Watched');
define('TO_BE_WATCHED','Want to watch');
define('STALLED','Stalled');

define('PASSWORD_CHANGED','Password changed successfully!');
define('PASSWORD_INCORRECT','Incorrect values :-(');

define('USER_PROFILE','User profile');
define('NOT_REGISTERED','Not registered yet? Do it');
define('HERE','here');

define('MAIN','Main');
define('BLOG','Blog');
define('SERIES','Series');
define('USERS','Users');
define('LOGIN','Login');

/* Users */
define('SIGN_UP','Sign up form');
define('ACC_INFO','Account information');
define('USERNAME','Username');
define('PASSWORD','Password');
define('REPEAT_PASS','Repeat password');
define('EMAIL','E-mail');
define('PERSONAL_INFO','Personal information (optional)');
define('SEX','Sex');
define('MALE','Male');
define('FEMALE','Female');
define('NAME','Name');
define('LASTNAME','Lastname');
define('COUNTRY','Country');
define('POSTAL_CODE','Postal code');
define('LANGUAGE','Language');
define('VERIFICATION','Verification');
define('SEND', 'Send');

define('WELCOME', 'Welcome');

define('CHANGE_VOICE_PASSWORD','Change password for chat rooms');
define('CHANGE_USER_LANGUAGE','Change language');


define('USER_DOESNT_EXIST','User doesn\'t exist! oops?');
define('USER','User');
define('MAINTAINER','Maintainer');
define('BANNED','Banned');
define('ADMINISTRATOR','Administrator');

define('REVIEWS', 'Reviews');
define('CLICK_EDIT','Click to edit');
define('LAST_REVIEWS','Last comments');
define('ALL_USER_SERIES','All the series');
define('TV_SERIES','Series');
define('CURRENT_EPISODE','Current episode');
define('STATUS','State');
define('LAST_VIEWS','Last watched');
define('MAP_OF_USERS','Map of users');
define('TOTAL_REGISTERED','Total users registered');
define('LAST_10','Last 10 are');

define('CHANGE_PASSWORD','Password change');
define('OLD_PASSWD','Old password');
define('NEW_PASSWD','New password');
define('CONFIRM_PASSWD','Repeat new password');
define('EN','in');
define('MESSAGE','Message');

define('RATING','Rating');
define('VERY_POOR','Very poor');
define('POOR','Poor');
define('NOT_BAD','Not that bad');
define('FAIR','Fair');
define('AVERAGE','Average');
define('ALMOST_GOOD','Almost good');
define('GOOD','Good');
define('VERY_GOOD','Very good');
define('EXCELLENT','Excellent');
define('PERFECT','Perfect');

define('SEASON','Season');
define('EPISODE','Episode');
define('AIRED','Aired');
define('RUNTIME','Runtime');
define('YEAR','Year');
define('DIRECTORS','Directors');
define('CAST','Cast');
define('PLOT','Plot');

define('AVG','Average');
define('LAST_FOLLOWED','Last followed series');
define('UPDATED','updated');
define('LAST_BLOG','Last news');

/* AjaxMethods */
define('USER_CREATED','User created');
define('JUST_NOW','Just now!');
define('CLICK_HERE','Click here');
define('NOT_FOUND','Not found');
define('REGISTER_TO_VOTE','Register to vote :-(');
define('CHANGED','Changed');


/* Blog */
define('NO_COMMENTS', 'No comments, be the first');
define('COMMENT', 'Comment'); //Imperativo
define('COMMENTS', 'Comments');
define('HAS', 'Has');
define('BY', 'by');
define('ON', 'on');
define('SAYS', 'says');
define('SAID', 'said');

/* Admin */
define('ADD_POST','Add post');
define('EDIT_POSTS','Edit posts');
define('ADD_SERIES','Add series');
define('LINKS_ALLOWED','You can add IMDB and Wikipedia links');
define('POST','Post');

?>
