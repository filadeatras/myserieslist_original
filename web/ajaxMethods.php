<?php
include('includes/basic.php');
require_once('includes/recaptcha/recaptchalib.php');
$login=new Login;
$login->chooseLang();

// Si es vol fer login
if(isset($_POST['username'])){
	
	$username = $_POST['username'];
	$password = md5($_POST['password']."f1l4d34tr4s");
	
	if (!$login->doLogin($username, $password)) echo "false";
	else echo "true";
}

//Si es vol inserir un nou usuari username, password, confirm_password, email, 

else if  ((isset($_POST['uname'])) && (isset($_POST['pass1'])) && (isset($_POST['pass2'])) && (isset($_POST['email'])))
{
	$resp = recaptcha_check_answer (RECAP_PRIVATE,
                $_SERVER["REMOTE_ADDR"],
                $_POST["recaptcha_challenge_field"],
                $_POST["recaptcha_response_field"]);

        if (!$resp->is_valid) {
          die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
               "(reCAPTCHA said: " . $resp->error . ")");
        } else{
		$conn = new Database;
		$conn->conectar();
		$country = $conn->_clean($_POST['country']);
		$conn->select("id","paises", "nombre = '$country'");
		
		//obligatoris
		$username = $conn->_clean($_POST['uname']);
		$password = md5($conn->_clean($_POST['pass1'])."f1l4d34tr4s");
		$email = $conn->_clean($_POST['email']);
		
		//obligatoris
		
		//optatius
		$sex = $conn->_clean($_POST['sex']);
		$realname = htmlentities($conn->_clean($_POST['realname'])); 
		$lastname = htmlentities($conn->_clean($_POST['lastname']));
		
		$postalcode = $conn->_clean($_POST['postalCode']);
		$language= $conn->_clean($_POST['language']);
		
	
		$all = $conn->dump();
		
		$id_pais = $all[0]['id'];
		
		$conn->insert
		(
			"users",
			"username, email, password, signup_date, level, preferred_language, sex, realname, lastname, id_pais, postal_code",
			"'$username', '$email', '$password', now(), 1, '$language', '$sex','$realname', '$lastname','$id_pais', '$postalcode'"
		);
		
		$conn->desconectar();
		echo '<h1>'.USER_CREATED.'</h1>';
	}
}

// Si es vol sortir
else if(isset($_POST['logout'])){
	if($login->logoff());
	echo "logoff";
}

else if(isset($_GET['country'])){
	$country =$_GET['country'];
	$conn=new Database;
	$conn->conectar();
	$conn->select("p.nombre","paises p, idiomas i", 'p.nombre LIKE \''.$country.'%\' AND i.id = p.id_idioma AND p.id_idioma =3');
	$conn->_json("nombre");
	$conn->desconectar();
}

else if(isset($_POST['existUser'])){
	$bool = "false";
	$user =$_POST['existUser'];
	$conn=new Database;
	$conn->conectar();
	$conn->select("username","users", "username='$user'");
	if($conn->_isset()) $bool="true";
	$conn->desconectar();
	echo $bool;
}

// Si es vol escriure un missatge al blog
else if(isset($_POST['message'])){
	
	$bd = new Database;
	$bd->conectar();
	if($login->logged()) {
			
		$idusuari = $login->getUserID();
		$idarticulo = $_POST['article'];
		
		$bd = new Database;
		$bd->conectar();
		
		$bd->select("date","blog_comments","date >= DATE_SUB( NOW( ) , INTERVAL 1 MINUTE) and id_usuario = $idusuari and id_articulo = $idarticulo");
		if($bd->num_rows() > 0){
			echo "exceded";
			exit;
		}else {
			$bd->insert('blog_comments','id_usuario,id_articulo,comment',$login->getUserID().','.clean($_POST['article']).',\''.clean($_POST['message']).'\'');
			$bd->desconectar();
			echo '<div id="dataComment">
					<div id="imgComment"><img src="/images/noAvatar.png" /></div>
					<div id="commentAuthor"><a href="http://'.$_SERVER['SERVER_NAME'].'users/'.$login->getUsername().'">'.$login->getUsername().'</a></div>
					<div id="commentDate"><i> '.JUST_NOW.'</i></div>
				</div>
				<div id="textComment">'.$_POST['message'].'</div>';
		}
	}
	
}

// Si es vol afegir una serie
else if(isset($_POST['imdbid'])){
	if($login->getLevel()>1){
		require_once('includes/imdbphp/imdb.class.php');
		
		$bd = new Database;
		$bd->conectar();
		
		//Add series information to table series
		$bd->insert('series','insert_userid,imdbid,duration,year,name',
		            $login->getUserID().','.clean($_POST['imdbid']).','.clean($_POST['duration']).',\''.clean($_POST['year']).'\',\''.clean($_POST['title']).'\'');
		
		
		
		//Add each episode
		$series = new imdb(clean($_POST['imdbid']));
		$series->setid(clean($_POST['imdbid']));
		
		$bd->select('id','series','imdbid=\''.clean($_POST['imdbid']).'\'');
		$seriesid = $bd->dump();
		
		$episodes_array = $series->episodes();
		
		for($i=1;$i<=$series->seasons();$i++){
		    for($y=1;$y<=count($episodes_array[$i]);$y++){
			if(strtotime($episodes_array[$i][$y]['airdate'])<strtotime('today')){
			    if($episodes_array[$i][$y]['airdate']!='????'){
				$bd->insert('series_episodes','id_series,season,episode_num,name
					    ',$seriesid[0]['id'].','.$i.','.$y.',\''.$episodes_array[$i][$y]['title'].'\'');
			    }
			}
		    }
		}
	
	
		$bd->desconectar();
		echo 'Added!';
	}
}

//Si es vol trobar la id d'una serie mitjan�ant la URL
else if(isset($_POST['url'])){
	if($login->getLevel()>1){
		$link_clean = explode("/", $_POST['url']);
		//Si es IMDB
		if(strpos($link_clean[2], "imdb")){
			require_once('includes/imdbphp/imdb.class.php');
			$foundID = substr($link_clean[4], 2);
		}
		
		//Si es Wiki
		else if(strpos($link_clean[2], "wiki")){
			require_once('includes/wikiextractor.php');
			
			$link = new wikiextractor;
			$search = $link->series_search($_POST['url']);
			
			$link->series_info($search['name'], $search['lang']);
			$foundID = trim($link->imdbid);
			
		}
		
		if(!empty($foundID)) echo '<a href="http://'.$_SERVER['SERVER_NAME'].'/admin/'.$foundID.'">'.CLICK_HERE.'</a>';
		else echo NOT_FOUND;
	}
}

//Si es vol enviar un comentari en un shoutbox de serie
else if(isset($_POST['review'])){
	$bd = new Database;
	$bd->conectar();
	if($login->logged()) {
		
		$idusuari = $login->getUserID();
		$idserie = $_POST['idseries'];
		
		$bd->select("date","series_comments","date >= DATE_SUB( NOW( ) , INTERVAL 1 MINUTE) and id_usuario = $idusuari and id_series = $idserie");
		if($bd->num_rows() > 0){
			echo "exceded";
			exit;
		}
		else {
			$bd->insert('series_comments','id_usuario, id_series, IP, comment, date',
				$login->getUserID().','.clean($_POST['idseries']).',\''.$login->getUserIP().'\',\''.clean($_POST['review']).'\', now()');
			echo '<div id="dataComment">
				<div id="imgComment"><img src="/images/noAvatar.png" /></div>
				<div id="commentAuthor"><a href="http://'.$_SERVER['SERVER_NAME'].'users/'.$login->getUsername().'">'.$login->getUsername().'</a></div>
				<div id="commentDate"><i> '.JUST_NOW.'</i></div>
			</div>
			<div id="textComment">'.$_POST['review'].'</div>';
		}
	}
        else echo "error";
}


//Si es vol enviar un comentari en un shoutbox de episodi
else if(isset($_POST['shoutboxEpisode'])){
	$bd = new Database;
	$bd->conectar();
	if($login->logged()) {
		
		$idusuari = $login->getUserID();
		$idepisode = $_POST['idepisode'];
		
		$bd->select("date","series_episodes_comments","date >= DATE_SUB( NOW( ) , INTERVAL 1 MINUTE) and id_usuario = $idusuari and id_episode = $idepisode");
		if($bd->num_rows() > 0){
			echo "exceded";
			exit;
		}else {
		$bd->insert('series_episodes_comments','id_usuario, id_episode, IP, comment',
		    $login->getUserID().','.clean($_POST['idepisode']).',\''.$login->getUserIP().'\',\''.clean($_POST['shoutboxEpisode']).'\'');
	
		$bd->select('series_episodes_comments.*,users.username','series_episodes_comments,users','
                    series_episodes_comments.id_usuario=users.id and id_episode='.$_POST['idepisode'].' order by date desc');
	
		echo '<div id="dataComment">
				<div id="imgComment"><img src="/images/noAvatar.png" /></div>
				<div id="commentAuthor"><a href="http://'.$_SERVER['SERVER_NAME'].'users/'.$login->getUsername().'">'.$login->getUsername().'</a></div>
				<div id="commentDate"><i> '.JUST_NOW.'</i></div>
			</div>
			<div id="textComment">'.$_POST['shoutboxEpisode'].'</div>';
		}
	}
	else echo "error";
}

else if(isset($_POST['borraPost'])){
	if($login->getLevel() > 2){
		
		$id=$_POST['borraPost'];
		
		$bd = new Database;
		$bd->conectar();
		
		$bd->delete("blog", "id = $id");
		
		$bd->desconectar();

		echo "done";
	}
}

else if(isset($_POST['newLanguage'])){
	
	if($login->logged()){
		
		$idusuari = $login->getUserID();
		
		$bd = new Database;
		$bd->conectar();
		
		$_POST['newLanguage'] = clean($_POST['newLanguage']);
		
		$bd->update('users', 'preferred_language', ''.$_POST['newLanguage'].'', "id = $idusuari");
	}

}

//per posar stars en series
else if(isset($_POST['serie_name'])){

	$bd = new Database;
	$bd->conectar();
	
	//hasta aqui pillamos tos los datos
	//ahora miramos si ya se ha votado o no
	
	$name = clean($_POST['serie_name']);
	
	$bd->select("id", "series", "name = \"$name\"");
	
	$new = $bd->dump();
	
	$idseries = $new[0]['id'];
	$userid = $login->getUserID();
	$rate = clean($_POST['serie_rate']);
	
	//hasta aqui pillamos tos los datos
	//ahora miramos si ya se ha votado o no
	
	$bd->select("*", "series_stars", "id_series = $idseries and id_usuario =$userid");
	
	if($bd->num_rows() > 0){
		$bd->update('series_stars', 'num_stars', $rate, "id_series = $idseries and id_usuario =$userid");
	} else{
		$bd->insert('series_stars','id_usuario,	id_series,num_stars', $login->getUserID().','.$idseries.','.clean($_POST['serie_rate']));
	}
	
	//despues de insertar o actualizar, mostramos la nueva media
	
        $bd->select('num_stars','series_stars',"id_series=$idseries");
        $todo = $bd->dump();
        $suma = 0;
        for($i=0;$i<count($todo);$i++){
            $suma = $todo[$i]['num_stars'] + $suma;
        }
        
        @$avgVotes = $suma / count($todo);
	
	$bd->desconectar();
	
	if(!$login->logged()){
		echo REGISTER_TO_VOTE;
	}else {
		echo '<br>General: '.number_format($avgVotes,2).'/10';
	}
}


// Per posar stars en episodis
else if(isset($_POST['serie_episode_rate'])){
	
	$bd = new Database;
	$bd->conectar();
	
	$episode_id = clean($_POST['serie_episode_id']);
	$userid = $login->getUserID();
	$rate = clean($_POST['serie_episode_rate']);
	
	//hasta aqui pillamos tos los datos
	//ahora miramos si ya se ha votado o no
	
	$bd->select("*", "series_episodes_stars", "id_episode = '$episode_id' and id_usuario ='$userid'");
	
	if($bd->num_rows() > 0){
		$bd->update('series_episodes_stars', 'num_stars', $rate, "id_episode = '$episode_id' and id_usuario ='$userid'");
	} else{
		$bd->insert('series_episodes_stars','id_usuario,id_episode,num_stars', $login->getUserID().','.$episode_id.','.clean($_POST['serie_episode_rate']));
	}
	
	//despues de insertar o actualizar, mostramos la nueva media
	
        $bd->select('num_stars','series_episodes_stars',"id_episode='$episode_id'");
        $todo = $bd->dump();
        $suma = 0;
        for($i=0;$i<count($todo);$i++){
            $suma = $todo[$i]['num_stars'] + $suma;
        }
        
        @$avgVotes = $suma / count($todo);
	
	$bd->desconectar();
	
	if(!$login->logged()){
		echo REGISTER_TO_VOTE;
	}else {
		echo '<br>General: '.number_format($avgVotes,2).'/10';
	}	
}

else if (isset($_POST['title']) && (isset($_POST['bodyText']))){
	
	if($login->getLevel() > 2){
		
		$bd = new Database;
		$bd->conectar();
		
		$title = clean($_POST['title']);
		$bodytext = clean($_POST['bodyText']);
		$id_usuari = $login->getUserID();
		
		$bd->insert("blog","id_usuario,title, body","$id_usuari, '$title', '$bodytext'");
		echo "done";
	}
	else "error";	
}


else if (isset($_POST['oldPassword'])){
	
	if ($login->logged()){

		if ( strlen($_POST['newPassword']) <= 6){
			echo "fail";
			exit;
		}
	
		if (empty($_POST['newPassword']) || empty($_POST['newPasswordConfirm']) ){
			echo "fail";
			exit;
		}
		
		else
		
		{
			if ($_POST['newPassword'] != $_POST['newPasswordConfirm']) {
				echo "fail";
				exit;
			}
			
			$_POST['oldPassword'] .= "f1l4d34tr4s";
			$_POST['newPassword'] .= "f1l4d34tr4s";
			$_POST['newPasswordConfirm'] .= "f1l4d34tr4s";

			$bd = new Database;
			$bd->conectar();		
			$userid = $login->getUserID();
			
			$bd->update('users','password', ''.md5($_POST['newPassword']).'', 'id = '.$userid.'');
			
			$bd->desconectar();
			
		}

	}
}

else if (isset($_POST['asteriskPassword'])){
	
	if ($login->logged()){

		$userid = $login->getUserID();

		$bd = new Database;
		$bd->conectar();		
		
		$bd->update('users','asterisk_passwd', ''.clean($_POST['asteriskPassword']).'', 'id = '.$userid.'');
		
		echo "changed";
		$bd->desconectar();
			
	}
	echo "fail";
}

//Returning list of episodes to user

if(isset($_GET['listEpisodes'])){
	$bd = new Database;
	$bd->conectar();
	$bd->select('series_episodes.id, series_episodes.season, series_episodes.episode_num, series_episodes.name
		    ','series, series_episodes','
		    series.id=series_episodes.id_series and series.id=\''.$_GET['id'].'\'');
	$todo = $bd->dump();
	
	for($i=0;$i<count($todo);$i++){
		$id = $todo[$i]['id'];
		$array[$id] = $todo[$i]['season'].'x'.$todo[$i]['episode_num'].': '.substr($todo[$i]['name'],0,50);
	}
	
	print json_encode($array);
	
	$bd->desconectar();
	
	
	
	
}

if(isset($_GET['saveEpisode'])){
	$bd = new Database;
	$bd->conectar();
	$bd->select('id', 'watched' , 'id_series = '.clean($_POST['id']).' and id_usuario='.$login->getUserID());
	$array = $bd->dump();
	$id_watched = $array[0]['id'];
	
	//echo $bd->num_rows();
	if($bd->num_rows() == 0){
		$bd->insert('watched','id_episode,id_series,id_usuario,state',clean($_POST['value']).','.clean($_POST['id']).','.$login->getUserID().',\'watching\'');
	} else{
		$bd->update('watched','id_episode',clean($_POST['value']),'id='.$id_watched);
		$bd->update('watched','state','watching','id_series='.clean($_POST['id']).' and id_usuario='.$login->getUserID().' and id_episode='.clean($_POST['value']));
	}
	
	
	$bd->select('series_episodes.id, series_episodes.season, series_episodes.episode_num, series_episodes.name
		    ','series, series_episodes','
		    series.id=series_episodes.id_series and series_episodes.id=\''.$_POST['value'].'\'');
	
	$todo = $bd->dump();
	echo $todo[0]['season'].'x'.$todo[0]['episode_num'].': '.$todo[0]['name'];
	$bd->desconectar();
	
	
}

if(isset($_GET['saveStatus'])){
	$bd = new Database;
	$bd->conectar();
	
	$bd->update('watched','state',clean($_POST['value']),'id_series='.clean($_POST['id']).' and id_usuario='.$login->getUserID());
	$bd->select('state','watched','id_series='.clean($_POST['id']).' and id_usuario='.$login->getUserID());
	$todo = $bd->dump();
	echo $todo[0]['state'];
	$bd->desconectar();
}

/* Profiles */
if(isset($_GET['listCountries'])){
	$bd = new Database;
	$bd->conectar();
	$bd->select('id,nombre','paises','id_idioma=3 order by nombre');
	$todo = $bd->dump();
	
	for($i=0;$i<count($todo);$i++){
		$id = $todo[$i]['id'];
		$array[$id] = $todo[$i]['nombre'];
	}
	
	print json_encode($array);
	
	$bd->desconectar();
}

if(isset($_GET['selectCountry'])){	
	$bd = new Database;
	$bd->conectar();
	$bd->update('users','id_pais',clean($_POST['value']),'id='.$login->getUserID());
	
	$bd->select('nombre','paises','id_idioma=3 and id='.clean($_POST['value']));
	$todo = $bd->dump();
	echo $todo[0]['nombre'];
	$bd->desconectar();
}


if(isset($_GET['editProfile'])){
	$bd = new Database;
	$bd->conectar();
	
	$bd->update('users', clean($_POST['id']), clean(htmlentities($_POST['value'])), 'id='.$login->getUserID());
	echo $_POST['value'];
	$bd->desconectar();
	
}

if(isset($_GET['editProfileAdmin'])){
	if($login->getLevel() > 2){
		$id_usuari = $_POST['row_id'];
		
		$bd = new Database;
		$bd->conectar();
		$bd->update('users', clean($_POST['id']), clean($_POST['value']), 'id='.clean($id_usuari));
		echo $_POST['value'];
		$bd->desconectar();
	}
}

if(isset($_GET['editSeriesAdmin'])){
	if($login->getLevel() >= 2){
		$id_serie = $_POST['row_id'];
		
		$bd = new Database;
		$bd->conectar();
		$bd->update('series', clean($_POST['id']), clean($_POST['value']), 'id='.clean($id_serie));
		echo $_POST['value'];
		$bd->desconectar();
	}
}

if (isset($_GET['editBlogEntry'])){
	$level = $login->getLevel();
	$id_entrada = $_POST['row_id'];
	
	if($level < 3) 
	{
		echo "fail";
		exit;
	}
	
	else {
		$bd = new Database;
		$bd->conectar();
		$bd->update('blog', clean($_POST['id']), clean($_POST['value']), 'id='.clean($id_entrada));
		$bd->debug();
		echo $_POST['value'];
		$bd->desconectar();
	}
}

// Tweet!
if(isset($_POST['tweet'])){
	include('includes/series.class.php');
	$series = new Series;
	$series->tweetSeries($_POST['tweet']);
}

if(isset($_POST['topic']) && isset($_POST['problem'])){
	$bd = new Database;
	$bd->conectar();
	
	if($login->logged()) {
	
		$topic = clean($_POST['topic']);
		$problem = clean($_POST['problem']);
		$bd->desconectar();
		
		$cuerpo = "Formulario enviado\n"; 
		$cuerpo .= "Nombre: " . $login->getUsername() . "\n"; 
		$cuerpo .= "Topic: " . $topic . "\n"; 
		$cuerpo .= "Comentarios: " . $problem . "\n"; 
	
		//mando el correo... 
		mail("administrators@myserieslist.com","New report on MySeriesList",$cuerpo); 
	
		echo "done";
	
	} else echo "error";
}

?>